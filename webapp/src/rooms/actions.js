import {ACTION} from './constants';

export const setName = name => ({type: ACTION.SET_NAME, payload: name});

export const pushUserId = userId => ({type: ACTION.PUSH_USER_ID, payload: userId});

export const removeUserId = userId => ({type: ACTION.REMOVE_USER_ID, payload: userId});

export const pushMessage = message => ({type: ACTION.PUSH_MESSAGE, payload: message});

export const clear = () => ({type: ACTION.CLEAR});
