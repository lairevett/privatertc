import {checkSubmitErrors as wsCheckSubmitErrors} from '../../../app/utils/ws/redux_form';
import {validateRoomName, validatePin} from '../../../app/utils/validation';
import {history, retrieveUrl} from '../../../app/utils/router';
import {sendRequest as wsSendRequest} from '../../../ws/actions';
import {makeCreateRoomMessage} from '../../../ws/utils/factory';
import {setName} from '../../actions';

export const validate = ({name, pin}) => {
    const errors = {};
    errors.name = validateRoomName(name);
    errors.pin = validatePin(pin);
    return errors;
};

export const submit = async ({name, pin}, dispatch) => {
    const message = makeCreateRoomMessage(name, pin);
    await wsCheckSubmitErrors(() => dispatch(wsSendRequest(message)));

    dispatch(setName(name));
    history.replace(retrieveUrl('rooms:conversation').path);
};
