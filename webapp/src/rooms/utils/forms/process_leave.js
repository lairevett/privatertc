import {history, retrieveUrl} from '../../../app/utils/router';
import {DATA_CHANNEL_LABEL} from '../../../wrtc/constants';
import {wipeUserStorageData as wrtcWipeUserStorageData} from '../../../wrtc/actions';
import {makeLeaveRoomMessage} from '../../../ws/utils/factory';
import {sendRequest as wsSendRequest} from '../../../ws/actions';
import {clear} from '../../actions';

export const submit = async (_, dispatch, {name, userIds}) => {
    const message = makeLeaveRoomMessage(name);
    await dispatch(wsSendRequest(message));

    dispatch(wrtcWipeUserStorageData([DATA_CHANNEL_LABEL.ROOM], userIds));
    dispatch(clear());
    history.replace(retrieveUrl('rooms:join').path);
};
