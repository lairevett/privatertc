import {checkSubmitErrors as wsCheckSubmitErrors} from '../../../app/utils/ws/redux_form';
import {validateBroadcastedName, validateRoomName, validatePin} from '../../../app/utils/validation';
import {history, retrieveUrl} from '../../../app/utils/router';
import {setCurrentUser as authSetCurrentUser} from '../../../auth/actions';
import {sendRequest as wsSendRequest} from '../../../ws/actions';
import {makeJoinRoomMessage} from '../../../ws/utils/factory';
import {setName} from '../../actions';

export const validate = ({broadcastedName, name, pin}) => {
    const errors = {};
    errors.broadcastedName = validateBroadcastedName(broadcastedName);
    errors.name = validateRoomName(name);
    errors.pin = validatePin(pin);
    return errors;
};

export const submit = async ({broadcastedName, name, pin}, dispatch, {currentUser}) => {
    const message = makeJoinRoomMessage(broadcastedName, name, pin);
    const {user_id} = await wsCheckSubmitErrors(() => dispatch(wsSendRequest(message)));

    // Set artificial user id and temporary broadcasted name for unauthenticated users.
    dispatch(authSetCurrentUser({...currentUser, id: user_id, broadcasted_name: broadcastedName}));

    dispatch(setName(name));
    history.replace(retrieveUrl('rooms:conversation').path);
};
