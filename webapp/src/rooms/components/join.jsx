import React from 'react';
import RegularView from '../../app/components/partials/regular_view';
import {getDisplayName} from '../../app/utils/display_name';
import {useCurrentUser} from '../../auth/utils/hooks';
import JoinForm from './partials/join_form';

const Join = () => {
    const currentUser = useCurrentUser();

    return (
        <RegularView>
            <JoinForm
                initialValues={{broadcastedName: getDisplayName(currentUser)}}
                isAuthenticated={!!currentUser.username}
                currentUser={currentUser}
            />
        </RegularView>
    );
};

export default Join;
