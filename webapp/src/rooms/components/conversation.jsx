import React, {useEffect} from 'react';
import {useDispatch, useSelector, shallowEqual} from 'react-redux';
import deepEqual from 'deep-equal';
import {setNavTitle} from '../../app/actions';
import RegularView from '../../app/components/partials/regular_view';
import ConversationBox from '../../conversations/components/partials/conversation_box';

const Conversation = () => {
    const dispatch = useDispatch();
    const name = useSelector(state => state.rooms.name);
    const userIds = useSelector(state => state.rooms.userIds, shallowEqual);
    const messages = useSelector(state => state.rooms.messages, deepEqual);

    useEffect(() => {
        dispatch(setNavTitle(name));
    }, [dispatch, name]);

    return (
        <RegularView>
            <ConversationBox messagesResults={messages} recipients={userIds} />
        </RegularView>
    );
};

export default Conversation;
