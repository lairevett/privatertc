import React from 'react';
import {Field, reduxForm, propTypes} from 'redux-form';
import PropTypes from 'prop-types';
import {Form, FormField} from '../../../app/components/partials/form';
import FormError from '../../../app/components/partials/form_error';
import SubmitButton from '../../../app/components/partials/submit_button';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../../../users/constants';
import {validate, submit} from '../../utils/forms/process_join';
import {MODULE_NAME} from '../../constants';

const JoinForm = ({handleSubmit, error, valid, pristine, submitting, isAuthenticated}) => (
    <Form>
        {error && <FormError message={error} />}
        <form onSubmit={handleSubmit(submit)}>
            <Field
                component={FormField}
                id="broadcastedName"
                name="broadcastedName"
                label="Broadcasted name"
                type="text"
                disabled={isAuthenticated}
                plainText={isAuthenticated}
            />
            <Field component={FormField} id="name" name="name" label="Room name" type="text" />
            <Field component={FormField} id="pin" name="pin" label="PIN" type="password" />
            <div className="text-center mt-5">
                <SubmitButton disabled={!valid || pristine || submitting} value="Join" />
            </div>
        </form>
    </Form>
);

JoinForm.propTypes = {
    ...propTypes,
    isAuthenticated: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    currentUser: USER_DETAILS_PROP_TYPE(PropTypes).isRequired, // Used in submit().
};

export default reduxForm({
    form: `${MODULE_NAME}Join`,
    validate,
})(JoinForm);
