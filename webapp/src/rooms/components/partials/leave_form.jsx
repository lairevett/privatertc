import React from 'react';
import PropTypes from 'prop-types';
import {propTypes, reduxForm} from 'redux-form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {submit} from '../../utils/forms/process_leave';
import {MODULE_NAME} from '../../constants';

const LeaveForm = ({handleSubmit, submitting}) => (
    <form onSubmit={handleSubmit(submit)}>
        <SubmitButton appendClassName="bmd-btn-icon" disabled={submitting} title="Leave room">
            <span className="material-icons text-danger">login</span>
        </SubmitButton>
    </form>
);

LeaveForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    name: PropTypes.string.isRequired, // Used in submit().
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    userIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired, // Used in submit().
};

export default reduxForm({
    form: `${MODULE_NAME}Leave`,
})(LeaveForm);
