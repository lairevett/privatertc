import React from 'react';
import {useSelector, shallowEqual} from 'react-redux';
import LeaveForm from './leave_form';

const ConversationNavHeader = () => {
    const name = useSelector(state => state.rooms.name);
    const userIds = useSelector(state => state.rooms.userIds, shallowEqual);
    return <LeaveForm name={name} userIds={userIds} />;
};

export default ConversationNavHeader;
