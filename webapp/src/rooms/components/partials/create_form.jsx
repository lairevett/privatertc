import React from 'react';
import {Field, reduxForm, propTypes} from 'redux-form';
import {Form, FormField} from '../../../app/components/partials/form';
import FormError from '../../../app/components/partials/form_error';
import SubmitButton from '../../../app/components/partials/submit_button';
import {validate, submit} from '../../utils/forms/process_create';
import {MODULE_NAME} from '../../constants';

const CreateForm = ({handleSubmit, error, valid, pristine, submitting}) => (
    <Form>
        {error && <FormError message={error} />}
        <form onSubmit={handleSubmit(submit)}>
            <Field component={FormField} id="name" name="name" label="Room name" type="text" />
            <Field component={FormField} id="pin" name="pin" label="PIN" type="password" />
            <div className="text-center mt-5">
                <SubmitButton disabled={!valid || pristine || submitting} value="Create" />
            </div>
        </form>
    </Form>
);

CreateForm.propTypes = propTypes;

export default reduxForm({
    form: `${MODULE_NAME}Create`,
    validate,
})(CreateForm);
