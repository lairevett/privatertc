import React from 'react';
import RegularView from '../../app/components/partials/regular_view';
import CreateForm from './partials/create_form';

const Create = () => (
    <RegularView>
        <CreateForm />
    </RegularView>
);

export default Create;
