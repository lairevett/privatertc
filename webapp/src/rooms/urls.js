import {RequireSignedIn, RequireOutOfRoom, RequireInRoom} from '../app/components/partials/route_access';
import {registerUrl} from '../app/utils/router';
import Create from './components/create';
import Join from './components/join';
import Conversation from './components/conversation';
import {MODULE_NAME} from './constants';

registerUrl(`${MODULE_NAME}:create`, `/${MODULE_NAME}/create`, [RequireSignedIn, RequireOutOfRoom, Create], {
    title: `Create room`,
    showBackButton: true,
});

registerUrl(`${MODULE_NAME}:join`, `/${MODULE_NAME}/join`, [RequireOutOfRoom, Join], {
    title: 'Join room',
    showBackButton: true,
});

registerUrl(`${MODULE_NAME}:conversation`, `/${MODULE_NAME}/conversation`, [RequireInRoom, Conversation], {
    showBackButton: true,
    hasDynamicTitle: true,
});
