export const MODULE_NAME = 'rooms';

// Redux action types.
export const ACTION = {
    SET_NAME: `@${MODULE_NAME}/SET_NAME`,
    PUSH_USER_ID: `@${MODULE_NAME}/PUSH_USER_ID`,
    REMOVE_USER_ID: `@${MODULE_NAME}/REMOVE_USER_ID`,
    PUSH_MESSAGE: `@${MODULE_NAME}/PUSH_MESSAGE`,
    CLEAR: `@${MODULE_NAME}/CLEAR`,
};

// Reducer initial states.
export const INITIAL_STATE = {
    name: '',
    userIds: [],
    messages: [],
};

// Side effect event types.
export const EVENT = {
    ROOM_LEAVE: 'ROOM_LEAVE',
};
