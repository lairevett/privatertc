import {INITIAL_STATE, ACTION} from './constants';

const roomsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.SET_NAME:
            return {
                ...state,
                name: action.payload,
            };
        case ACTION.PUSH_USER_ID: {
            const userIdsSet = new Set(state.userIds);
            userIdsSet.add(action.payload);

            return {
                ...state,
                userIds: [...userIdsSet],
            };
        }
        case ACTION.REMOVE_USER_ID:
            return {
                ...state,
                userIds: state.userIds.filter(id => id !== action.payload),
            };
        case ACTION.PUSH_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.payload],
            };
        case ACTION.CLEAR:
            return INITIAL_STATE;
        default:
            return state;
    }
};

export default roomsReducer;
