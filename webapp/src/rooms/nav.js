import {NAV_HEADER} from '../app/constants';
import {registerNavHeaderComponent} from '../app/utils/nav';
import ConversationNavHeader from './components/partials/conversation_nav_header';

registerNavHeaderComponent('rooms:conversation', ConversationNavHeader, NAV_HEADER.LEFT);
