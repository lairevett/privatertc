import {RequireSignedIn} from '../app/components/partials/route_access';
import {registerUrl} from '../app/utils/router';
import {createCallbackGetDispatchEvents, updateCallbackGetDispatchEvents} from './utils/dispatch_events';
import Create from './components/create';
import Update from './components/update';
import {MODULE_NAME} from './constants';

registerUrl(`${MODULE_NAME}:create`, `/${MODULE_NAME}/create`, [RequireSignedIn, Create], {
    title: 'Create group',
    showBackButton: true,
    callbackGetDispatchEvents: createCallbackGetDispatchEvents,
});

registerUrl(`${MODULE_NAME}:update`, `/${MODULE_NAME}/update/:groupId`, [RequireSignedIn, Update], {
    title: 'Group settings',
    showBackButton: true,
    callbackGetDispatchEvents: updateCallbackGetDispatchEvents,
});
