import {apiGetRequest, apiPostRequest, apiPatchRequest, apiDeleteRequest} from '../app/utils/api/requests';
import {ENDPOINT, ACTION, DETAILS_PROP_TYPE, DETAILS_INITIAL_STATE, USER_RANK} from './constants';

export const triggerUserInvite = (userId, currentUserRank = USER_RANK.ADMINISTRATOR) => ({
    type: ACTION.TRIGGER_USER_INVITE,
    payload: {userId, currentUserRank},
});

export const initCreateUpdateState = currentMembers => ({
    type: ACTION.INIT_CREATE_UPDATE_STATE,
    payload: currentMembers,
});

export const clearCreateUpdateState = () => ({type: ACTION.CLEAR_CREATE_UPDATE_STATE});

export const create = (broadcastedName, invitedUsers) =>
    apiPostRequest(ACTION.CREATE, DETAILS_PROP_TYPE, ENDPOINT.LIST, {
        broadcasted_name: broadcastedName,
        members: invitedUsers,
    });

export const retrieveDetails = groupId =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(groupId));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const queryConversationId = id =>
    apiGetRequest(ACTION.QUERY_CONVERSATION_ID, {conversation_id: ''}, ENDPOINT.DETAILS_CONVERSATION_ID(id));

export const update = (groupId, broadcastedName, invitedUsers) =>
    apiPatchRequest(ACTION.UPDATE, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(groupId), {
        broadcasted_name: broadcastedName,
        members: invitedUsers,
    });

export const destroy = (groupId, uninvitedUsers) =>
    apiDeleteRequest(ACTION.DESTROY, null, ENDPOINT.DETAILS(groupId), {members: uninvitedUsers});
