import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import deepEqual from 'deep-equal';
import {combineStatuses} from '../../app/utils/api/helpers';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import UserList from '../../users/components/partials/user_list';
import {initCreateUpdateState} from '../actions';
import CreateUpdateForm from './partials/create_update_form';
import UserListElement from './partials/user_list_element';

const Update = ({urlParameters}) => {
    const dispatch = useDispatch();

    const currentMembersState = useSelector(state => state.groups.createUpdate.currentMembers, deepEqual);
    const changedKeys = useSelector(state => state.groups.createUpdate.changedKeys, shallowEqual);

    const currentGroupMembers = useSelector(state => state.groups.details.members, deepEqual);
    const currentGroupBroadcastedName = useSelector(state => state.groups.details.broadcasted_name);

    const groupDetailsStatus = useSelector(state => state.groups.details.__meta__.status);
    const usersListStatus = useSelector(state => state.users.list.__meta__.status);
    const combinedStatuses = combineStatuses([groupDetailsStatus, usersListStatus]);

    useEffect(() => {
        const currentMembers = Object.fromEntries(currentGroupMembers.map(({id, user_rank}) => [id, user_rank]));
        dispatch(initCreateUpdateState(currentMembers));
    }, [dispatch, currentGroupMembers]);

    return (
        <RegularView>
            <Suspense status={combinedStatuses} fallback={<Loading />}>
                <CreateUpdateForm
                    groupId={urlParameters.groupId}
                    currentMembers={currentMembersState}
                    changedKeys={changedKeys}
                    initialValues={{broadcastedName: currentGroupBroadcastedName}}
                />
                <UserList JSXElement={UserListElement} />
            </Suspense>
        </RegularView>
    );
};

Update.propTypes = {
    urlParameters: PropTypes.shape({groupId: PropTypes.string.isRequired}).isRequired,
};

export default Update;
