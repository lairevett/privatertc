import React from 'react';
import {useSelector, shallowEqual} from 'react-redux';
import deepEqual from 'deep-equal';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import UserList from '../../users/components/partials/user_list';
import CreateUpdateForm from './partials/create_update_form';
import UserListElement from './partials/user_list_element';

const Create = () => {
    const currentMembers = useSelector(state => state.groups.createUpdate.currentMembers, deepEqual);
    const changedKeys = useSelector(state => state.groups.createUpdate.changedKeys, shallowEqual);
    const status = useSelector(state => state.users.list.__meta__.status);

    return (
        <RegularView>
            <CreateUpdateForm currentMembers={currentMembers} changedKeys={changedKeys} />
            <Suspense status={status} fallback={<Loading />}>
                <UserList JSXElement={UserListElement} />
            </Suspense>
        </RegularView>
    );
};

export default Create;
