import React from 'react';
import PropTypes from 'prop-types';
import {Field, propTypes, reduxForm} from 'redux-form';
import {FormField} from '../../../app/components/partials/form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {validate, submit} from '../../utils/forms/process_create_update';
import {MODULE_NAME, USER_RANK} from '../../constants';

const CreateUpdateForm = ({handleSubmit, valid, pristine, submitting, groupId, changedKeys}) => (
    <div className="border-bottom pl-3">
        <form onSubmit={handleSubmit(submit)}>
            <div className="d-flex flex-row">
                <Field
                    component={FormField}
                    id="broadcastedName"
                    name="broadcastedName"
                    type="text"
                    label="Broadcasted name (not required)"
                    formGroupClassNames="w-100 mr-2"
                />
                <SubmitButton
                    value={!!groupId ? 'Update' : 'Create'}
                    disabled={!valid || (!!groupId && pristine && changedKeys.length === 0) || submitting}
                />
            </div>
        </form>
    </div>
);

CreateUpdateForm.propTypes = {
    ...propTypes,
    groupId: PropTypes.string,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    currentMembers: PropTypes.objectOf(PropTypes.oneOf(Object.values(USER_RANK))).isRequired, // Used in submit().
    changedKeys: PropTypes.arrayOf(PropTypes.string.isRequred).isRequired, // Used in submit().
};

CreateUpdateForm.defaultProps = {
    groupId: '',
};

export default reduxForm({form: `${MODULE_NAME}CreateUpdate`, validate})(CreateUpdateForm);
