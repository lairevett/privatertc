import React from 'react';
import PropTypes from 'prop-types';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import TableListRow from '../../../app/components/partials/table_list_row';
import UserListElementActiveStatus from '../../../users/components/partials/user_list_element_active_status';
import UserListElementInfo from '../../../users/components/partials/user_list_element_info';
import {triggerUserInvite} from '../../actions';
import UserListElementRank from './user_list_element_rank';

const UserListElement = ({id, displayName, isOnline, lastSeen, currentUserId}) => {
    const dispatch = useDispatch();
    const currentMembers = useSelector(state => state.groups.createUpdate.currentMembers, shallowEqual);

    const userRank = currentMembers?.[id];
    const currentUserRank = currentMembers?.[currentUserId];
    const isActive = typeof userRank !== 'undefined';
    const isDisabled = userRank >= currentUserRank;

    return (
        <TableListRow
            to="#"
            isActive={isActive}
            onClick={() => dispatch(triggerUserInvite(id, currentUserRank))}
            disabled={isDisabled}
            turnSymbolOnActive
            activeSymbol="check_box"
            inactiveSymbol="check_box_outline_blank"
        >
            <UserListElementActiveStatus isOnline={isOnline} />
            <UserListElementInfo displayName={displayName} isOnline={isOnline} lastSeen={lastSeen} />
            <UserListElementRank userRank={~~userRank} />
        </TableListRow>
    );
};

UserListElement.propTypes = {
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    isOnline: PropTypes.bool.isRequired,
    lastSeen: PropTypes.string,
    currentUserId: PropTypes.string.isRequired,
};

UserListElement.defaultProps = {
    lastSeen: null,
};

export default UserListElement;
