import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import {USER_RANK} from '../../constants';

const UserListElementRank = ({userRank}) => {
    const icon = userRank === USER_RANK.ORDINARY ? 'person' : 'shield';

    return (
        <TableListCell appendClassName={`pr-0${!userRank ? ' invisible' : ''}`}>
            <span className="material-icons vertical-center text-primary">{icon}</span>
        </TableListCell>
    );
};

UserListElementRank.propTypes = {
    userRank: PropTypes.oneOf([0, ...Object.values(USER_RANK)]).isRequired,
};

export default UserListElementRank;
