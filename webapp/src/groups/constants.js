import {DEFAULT_META_INITIAL_STATE, META_PROP_TYPE} from '../app/utils/api/constants';

export const MODULE_NAME = 'groups';

// API endpoints.
export const ENDPOINT = {
    LIST: `/v1/${MODULE_NAME}/`,
    DETAILS: id => `/v1/${MODULE_NAME}/${id}/`,
    DETAILS_CONVERSATION_ID: id => `/v1/${MODULE_NAME}/${id}/conversation_id/`,
};

// Redux action types.
export const ACTION = {
    TRIGGER_USER_INVITE: `@${MODULE_NAME}/TRIGGER_USER_INVITE`,
    INIT_CREATE_UPDATE_STATE: `@${MODULE_NAME}/INIT_CREATE_UPDATE_STATE`,
    CLEAR_CREATE_UPDATE_STATE: `@${MODULE_NAME}/CLEAR_CREATE_UPDATE_STATE`,
    CREATE: `@${MODULE_NAME}/CREATE`,
    RETRIEVE_DETAILS: `@${MODULE_NAME}/RETRIEVE_DETAILS`,
    CLEAR_DETAILS: `@${MODULE_NAME}/CLEAR_DETAILS`,
    QUERY_CONVERSATION_ID: `@${MODULE_NAME}/QUERY_CONVERSATION_ID`,
    UPDATE: `@${MODULE_NAME}/UPDATE`,
    DESTROY: `@${MODULE_NAME}/DESTROY`,
};

// Memoized API queries.
export const MEMO_API_QUERY = {
    CONVERSATION_ID: 'group_conversation_id',
};

// Reducer initial states.
export const CREATE_UPDATE_INITIAL_STATE = {
    initialMembers: {},
    currentMembers: {},
    changedKeys: [],
};

export const DETAILS_INITIAL_STATE = {
    id: '',
    broadcasted_name: '',
    members: [],
    is_online: false,
    ...DEFAULT_META_INITIAL_STATE,
};

export const INITIAL_STATE = {
    createUpdate: CREATE_UPDATE_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Group privileges.
export const USER_RANK = {
    ORDINARY: 1,
    MODERATOR: 2,
    ADMINISTRATOR: 3,
};

// Prop types.
export const MEMBER_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        id: PropertyTypes.string.isRequired,
        broadcasted_name: PropertyTypes.string,
        is_online: PropertyTypes.bool.isRequired,
        user_rank: PropertyTypes.number.isRequired,
    });

export const DETAILS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        id: PropertyTypes.string.isRequired,
        broadcasted_name: PropertyTypes.string.isRequired,
        members: PropertyTypes.arrayOf(MEMBER_PROP_TYPE(PropertyTypes)),
        is_online: PropertyTypes.bool.isRequired,
        __meta__: META_PROP_TYPE(PropertyTypes),
    });
