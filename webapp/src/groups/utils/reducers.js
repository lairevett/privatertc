export const updateMemberIsOnline = (members, memberId, isOnline) =>
    members.map(member => (member.id === memberId ? {...member, is_online: isOnline} : member));

export const getOnlineMemberIds = (group, currentUserId) =>
    group
        ? group.members.filter(member => member.id !== currentUserId && member.is_online).map(member => member.id)
        : [];

export const isGroupOnline = (members, currentUserId) =>
    members.some(member => member.id !== currentUserId && member.is_online);
