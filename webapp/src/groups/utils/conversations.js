import {USER_RANK} from '../constants';

export const getSortedMembersByRanks = group => {
    const ordinary = [];
    const moderators = [];
    let administrator = null;

    for (const member of group.members) {
        switch (member.user_rank) {
            case USER_RANK.ADMINISTRATOR:
                administrator = member;
                break;
            case USER_RANK.MODERATOR:
                moderators.push(member);
                break;
            case USER_RANK.ORDINARY:
                ordinary.push(member);
                break;
            default:
        }
    }

    return [ordinary, moderators, administrator];
};
