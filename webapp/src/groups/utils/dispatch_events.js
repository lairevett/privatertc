import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {retrieveList as usersRetrieveList, clearList as usersClearList} from '../../users/actions';
import {retrieveDetails, clearDetails, clearCreateUpdateState} from '../actions';

export const createCallbackGetDispatchEvents = () => () =>
    makeDispatchEvents([usersRetrieveList(1)], [clearCreateUpdateState, usersClearList]);

export const updateCallbackGetDispatchEvents = () => ({groupId}) =>
    makeDispatchEvents(
        [retrieveDetails(groupId), usersRetrieveList(1)],
        [clearDetails, clearCreateUpdateState, usersClearList],
        [groupId]
    );
