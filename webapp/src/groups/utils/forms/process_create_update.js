import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {validateBroadcastedName} from '../../../app/utils/validation';
import {history, reverseUrl} from '../../../app/utils/router';
import {create, queryConversationId, update, destroy} from '../../actions';
import {USER_RANK} from '../../constants';

// Spoof USER_RANK during DELETE request so backend does less less work.
// Provided value doesn't matter on backend side as long as it's one of the entries from USER_RANK map.
const parseUsersEntries = usersEntries =>
    usersEntries.map(([userId, userRank]) => ({user_id: userId, user_rank: userRank ?? USER_RANK.ORDINARY}));

const getSortedRequestBodies = changedUsersEntries => {
    const sortedEntries = changedUsersEntries.reduce(
        (accumulator, next) => {
            const [, userRank] = next;
            const isUninvited = typeof userRank === 'undefined';
            const objectKey = isUninvited ? 'uninvited' : 'invited';
            // eslint-disable-next-line security/detect-object-injection
            accumulator[objectKey].push(next);
            return accumulator;
        },
        {invited: [], uninvited: []}
    );

    return {
        invitedUsers: parseUsersEntries(sortedEntries.invited),
        uninvitedUsers: parseUsersEntries(sortedEntries.uninvited),
    };
};

export const validate = ({broadcastedName}) => {
    const errors = {};
    errors.broadcastedName = validateBroadcastedName(broadcastedName);
    return errors;
};

export const submit = async ({broadcastedName}, dispatch, {groupId, currentMembers, changedKeys}) => {
    const changedUsersEntries = changedKeys.map(userId => [userId, currentMembers?.[userId]]);
    const {invitedUsers, uninvitedUsers} = getSortedRequestBodies(changedUsersEntries);
    const isUpdate = !!groupId;

    const response = isUpdate
        ? dispatch(await update(groupId, broadcastedName, invitedUsers)())
        : dispatch(await create(broadcastedName, invitedUsers)());

    if (response?.payload) {
        apiCheckSubmitErrors(response.payload, {broadcasted_name: 'broadcastedName'});
    }

    if (uninvitedUsers.length > 0) {
        const {payload: destroyPayload} = dispatch(await destroy(groupId, uninvitedUsers)());
        apiCheckSubmitErrors(destroyPayload);
    }

    const {
        payload: {conversation_id},
    } = dispatch(await queryConversationId(groupId ?? response.payload.id)());

    if (isUpdate) {
        history.goBack();
        return;
    }

    history.replace(reverseUrl('conversations:details', {conversationId: conversation_id}).path);
};
