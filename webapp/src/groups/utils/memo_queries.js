import {memoizedQuery} from '../../app/utils/api/memo';
import {MEMO_API_QUERY} from '../constants';
import {queryConversationId} from '../actions';

export const memoizedQueryGroupConversationId = async (dispatch, groupId) =>
    memoizedQuery(dispatch, groupId, MEMO_API_QUERY.CONVERSATION_ID, queryConversationId(groupId));
