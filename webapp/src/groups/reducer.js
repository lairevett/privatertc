import {INITIAL_STATE, ACTION, DETAILS_INITIAL_STATE, USER_RANK} from './constants';

const groupsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.TRIGGER_USER_INVITE: {
            const {userId, currentUserRank} = action.payload;
            const userRank = state.createUpdate.currentMembers?.[userId];
            const permitAddingModerator = currentUserRank === USER_RANK.ADMINISTRATOR;
            const nextUserRank =
                typeof userRank === 'undefined'
                    ? USER_RANK.ORDINARY
                    : userRank === USER_RANK.ORDINARY && permitAddingModerator
                    ? USER_RANK.MODERATOR
                    : undefined;

            const isRankDifferent = state.createUpdate.initialMembers?.[userId] !== nextUserRank;
            const nextChangedKeys = new Set(state.createUpdate.changedKeys);
            (isRankDifferent ? Set.prototype.add : Set.prototype.delete).bind(nextChangedKeys)(userId);

            return {
                ...state,
                createUpdate: {
                    ...state.createUpdate,
                    currentMembers: {
                        ...state.createUpdate.currentMembers,
                        [userId]: nextUserRank,
                    },
                    changedKeys: [...nextChangedKeys],
                },
            };
        }
        case ACTION.INIT_CREATE_UPDATE_STATE:
            return {
                ...state,
                createUpdate: {
                    ...state.createUpdate,
                    initialMembers: action.payload, // To track changes between the two.
                    currentMembers: action.payload,
                },
            };
        case ACTION.CLEAR_CREATE_UPDATE_STATE:
            return {
                ...state,
                createUpdate: {
                    ...state.createUpdate,
                    initialMembers: {},
                    currentMembers: {},
                    changedKeys: [],
                },
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: DETAILS_INITIAL_STATE,
            };
        default:
            return state;
    }
};

export default groupsReducer;
