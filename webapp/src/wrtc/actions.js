import {ACTION} from './constants';

export const addUserToStorage = (targetUserId, targetUserBroadcastedName) => ({
    type: ACTION.ADD_USER_TO_STORAGE,
    payload: {targetUserId, targetUserBroadcastedName},
});

export const addConnection = (targetUserId, targetUserWsChannelName, polite) => ({
    type: ACTION.ADD_CONNECTION,
    payload: {targetUserId, targetUserWsChannelName, polite},
});

export const addDataChannel = (targetUserId, targetUserWsChannelName, label) => ({
    type: ACTION.ADD_DATA_CHANNEL,
    payload: {targetUserId, targetUserWsChannelName, label},
});

export const addIceCandidate = (targetUserId, targetUserWsChannelName, iceCandidate) => ({
    type: ACTION.ADD_ICE_CANDIDATE,
    payload: {targetUserId, targetUserWsChannelName, iceCandidate},
});

export const sendToDataChannels = (messageContext, conversationContext) => ({
    type: ACTION.SEND_TO_DATA_CHANNELS,
    payload: {messageContext, conversationContext},
});

export const obtainMediaContext = callType => ({type: ACTION.OBTAIN_MEDIA_CONTEXT, payload: callType});

export const setUpCall = (targetUserId, targetUserWsChannelName, isFinished) => ({
    type: ACTION.SET_UP_CALL,
    payload: {targetUserId, targetUserWsChannelName, isFinished},
});

export const finalizeCallSetup = (targetUserId, targetUserWsChannelName) => ({
    type: ACTION.FINALIZE_CALL_SETUP,
    payload: {targetUserId, targetUserWsChannelName},
});

export const triggerAudioTrackMute = () => ({type: ACTION.TRIGGER_AUDIO_TRACK_MUTE});

export const triggerVideoTrackVisibility = () => ({type: ACTION.TRIGGER_VIDEO_TRACK_VISIBILITY});

export const getVideoStreams = participants => ({type: ACTION.GET_VIDEO_STREAMS, payload: participants});

export const tearDownCall = (redirectToDetails, targetUserId = '', targetUserWsChannelName = '') => ({
    type: ACTION.TEAR_DOWN_CALL,
    payload: {redirectToDetails, targetUserId, targetUserWsChannelName},
});

export const cleanUserStorageGarbage = (targetUserId, targetUserWsChannelName, dataChannelLabel) => ({
    type: ACTION.CLEAN_USER_STORAGE_GARBAGE,
    payload: {targetUserId, targetUserWsChannelName, dataChannelLabel},
});

export const wipeUserStorageData = (dataChannelLabels, userIds = undefined) => ({
    type: ACTION.WIPE_USER_STORAGE_DATA,
    payload: {dataChannelLabels, userIds},
});
