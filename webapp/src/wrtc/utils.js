export const makeForward = (type, value) => ({type, value});

export const makeEmptyForward = () => makeForward('', '');

export class WRTCStorageError extends Error {
    constructor(...parameters) {
        super(...parameters);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, WRTCStorageError);
        }

        this.name = 'WRTCStorageError';
    }
}
