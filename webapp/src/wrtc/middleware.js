/* eslint-disable security/detect-object-injection */
import {makeFunctionQueue} from '../app/utils/queues';
import {
    addAudioNode,
    attachAudioNodeSource,
    removeAudioNode,
    removeAudioNodes,
    getTargetUserWsChannelNameFromNode,
    getAudioNodes,
} from '../app/utils/dom';
import {
    TYPE as CALL_TYPE,
    STATUS as CALL_STATUS,
    ACTIVE_INITIAL_STATE as CALL_ACTIVE_INITIAL_STATE,
} from '../calls/constants';
import {
    updateActive as callsUpdateActive,
    addActiveVideoSender as callsAddActiveVideoSender,
    removeActiveVideoSender as callsRemoveActiveVideoSender,
} from '../calls/actions';
import {redirectFromCall} from '../calls/utils/router';
import {memoizedQueryUserChannelsCount, memoizedQueryUserConversationId} from '../users/utils/memo_queries';
import {memoizedQueryGroupConversationId} from '../groups/utils/memo_queries';
import {MESSAGE_QUEUE} from '../conversations/constants';
import {
    queryDetails as conversationsQueryDetails,
    prependResult as conversationsPrependResult,
    pushMessage as conversationsPushMessage,
    setLastMessage as conversationsSetLastMessage,
    querySupplementaryMessageData,
} from '../conversations/actions';
import {addLeaveCallAlertBeforeUnload, removeLeaveCallAlertBeforeUnload} from '../conversations/utils/calls';
import {memoizedQueryConversationUserIds} from '../conversations/utils/memo_queries';
import {
    makeKey,
    makeMessageQueueItem,
    pushItemToMessageQueue,
    queryMessageQueue,
    flushMessageQueue,
} from '../conversations/utils/message_queues';
import {
    makeMessageContext,
    makeConversationMessage,
    stripConversationContext,
    makeStrippedConversationContext,
} from '../conversations/utils/factory';
import {
    pushUserId as roomsPushUserId,
    removeUserId as roomsRemoveUserId,
    pushMessage as roomsPushMessage,
} from '../rooms/actions';
import {sendMessage as wsSendMessage, sendRequest as wsSendRequest} from '../ws/actions';
import {makeJoinMessage, makeRelayIceCandidateMessage, makeHangUpCallMessage} from '../ws/utils/factory';
import {relayDescription} from '../ws/utils/helpers';
import {ACTION, DATA_CHANNEL_LABEL, FORWARD} from './constants';
import {makeForward, makeEmptyForward, WRTCStorageError} from './utils';

const wrtcMiddleware = () => {
    const ICE_SERVERS = [{urls: ['stun:stun.l.google.com:19302']}];
    const WRTC_CONFIG = {
        iceServers: ICE_SERVERS,
    };

    /* Private API */

    /* Events */
    const _onConnectionIceCandidate = (evnt, dispatch, targetUserWsChannelName) => {
        if (evnt.candidate) {
            const relayIceCandidateMessage = makeRelayIceCandidateMessage(targetUserWsChannelName, evnt.candidate);
            dispatch(wsSendMessage(relayIceCandidateMessage));
        }
    };

    const _onReceivedConversationDataChannelOpen = (store, targetUserId, listenerFn) => {
        (async () => {
            // First N messages to new, not acknowledged conversation are getting
            // queued while RTC connection gets created. This event is dispatched when
            // all data channels are usable to dequeue the messages to the conversation.
            const user = _getUserFromStorage(targetUserId);
            const links = Object.values(user.wsToWrtcLinks);

            // Memoize, so that client won't send request every on data channel open.
            const {deleteMemo, channels_count} = await memoizedQueryUserChannelsCount(store.dispatch, targetUserId);

            const areAllOpened = label =>
                links.every(
                    ({dataChannels}) => dataChannels[label] != null && dataChannels[label].readyState === 'open'
                );

            const onAllOpened = () => {
                // API message requests aren't deferred and client could have already conversation fetched from
                // there, duplicates are handled in the reducer.
                _onAllReceivedConversationDataChannelsOpened(store, targetUserId);
                deleteMemo();

                // eslint-disable-next-line no-shadow
                links.forEach(({dataChannels}) => {
                    // eslint-disable-next-line no-shadow
                    const dataChannel = dataChannels[DATA_CHANNEL_LABEL.CONVERSATION];
                    if (dataChannel) {
                        dataChannel.removeEventListener('open', listenerFn);
                    }
                });
            };

            // Force flush if links still not opened after 3 seconds (probably stale cache).
            const timeout = setTimeout(onAllOpened, 3000);

            // Memoized channels count could become stale, don't compare strictly.
            if (links.length >= channels_count && areAllOpened(DATA_CHANNEL_LABEL.CONVERSATION)) {
                onAllOpened();
                clearTimeout(timeout);
            }
        })();
    };

    const _onAllReceivedConversationDataChannelsOpened = (store, targetUserId) => {
        (async () => {
            const {currentQueueState, keysWithMessages} = queryMessageQueue(MESSAGE_QUEUE.WEBRTC, targetUserId);

            flushMessageQueue(
                MESSAGE_QUEUE.WEBRTC,
                currentQueueState,
                keysWithMessages,
                (conversationId, zippedContent) => {
                    const messageContext = makeMessageContext(zippedContent, true);
                    const strippedConversationContext = makeStrippedConversationContext([targetUserId]);
                    _sendToReceiversDataChannel(store, messageContext, strippedConversationContext);
                }
            );
        })();
    };

    const _onReceivedDataChannel = (evnt, store, targetUserId, targetUserWsChannelName) => {
        const {channel} = evnt;

        if (channel.label === DATA_CHANNEL_LABEL.CONVERSATION) {
            (() => {
                const listenerFn = () => _onReceivedConversationDataChannelOpen(store, targetUserId, listenerFn);
                channel.addEventListener('open', listenerFn);
            })();
        }

        addDataChannel(store, targetUserId, targetUserWsChannelName, channel.label, channel);
    };

    const _onDataChannelMessage = (evnt, dispatch, senderUserId, pushMessageAction) => {
        const {content, useActualMessage, forward: forwardArguments} = _unzipDataChannelMessage(evnt.data);
        const forward = makeForward(...forwardArguments);

        const message = makeConversationMessage(content, senderUserId);
        dispatch(pushMessageAction(forward, message, useActualMessage));

        (async () => {
            // Make remote clients on other devices join conversation, too if some device joins one.
            if (forward.type === FORWARD.CONVERSATION_ID) {
                const conversationId = forward.value;
                const {user_ids} = await memoizedQueryConversationUserIds(dispatch, conversationId);

                // Check which users aren't in storage yet and send them join message.
                const disconnectedUsers = user_ids.filter(userId => _userStorage[userId]);
                if (disconnectedUsers.length > 0) {
                    const joinMessage = makeJoinMessage(disconnectedUsers);
                    dispatch(wsSendMessage(joinMessage));
                }
            }
        })();
    };

    const _onAddTrack = (dispatch, stream, kind, targetUserId, targetUserWsChannelName) => {
        try {
            const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);
            link.streams[kind] = stream;

            switch (kind) {
                case 'audio': {
                    const attachAudio = () => attachAudioNodeSource(targetUserWsChannelName, stream);

                    try {
                        attachAudio();
                    } catch (error) {
                        if (error instanceof TypeError) {
                            // Audio node wasn't added on time.
                            _onNewAudioNode.schedule(targetUserWsChannelName, attachAudio);
                            return;
                        }

                        console.trace(error);
                    }
                    break;
                }
                case 'video':
                    dispatch(callsAddActiveVideoSender(targetUserId, targetUserWsChannelName));
                    break;
                default:
            }
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                console.error(
                    `Tried to add track to not acknowledged user \
                    "${targetUserId}", channel "${targetUserWsChannelName}".`
                );
            }

            console.trace(error);
        }
    };

    const _onRemoveTrack = (dispatch, kind, targetUserId, targetUserWsChannelName) => {
        try {
            const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);
            link.streams[kind] = null;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                // Already deallocated.
                return;
            }

            console.trace(error);
        }

        if (kind === 'video') {
            dispatch(callsRemoveActiveVideoSender(targetUserId, targetUserWsChannelName));
        }
    };

    /* Users */
    const _userStorage = {};

    const _makeDataForUser = (id, broadcastedName) => ({
        id,
        broadcastedName,
        wsToWrtcLinks: {},
    });

    const _makeWsToWrtcLink = polite => ({
        connection: null,
        polite,
        makingOffer: false,
        dataChannels: {
            [DATA_CHANNEL_LABEL.CONVERSATION]: null,
            [DATA_CHANNEL_LABEL.ROOM]: null,
        },
        rtpSenders: {
            audio: null,
            video: null,
        },
        streams: {
            audio: null,
            video: null,
        },
    });

    const _getUserFromStorage = targetUserId => {
        const user = _userStorage[targetUserId];

        if (!user) {
            throw new WRTCStorageError(`User "${targetUserId}" was not added to storage.`);
        }

        return user;
    };

    const _getWsToWrtcLink = (targetUserId, targetUserWsChannelName, needsPeerConnection = false) => {
        const user = _getUserFromStorage(targetUserId);
        const link = user.wsToWrtcLinks[targetUserWsChannelName];

        if (!link) {
            console.dir(user);
            throw new WRTCStorageError(
                `WebSocket link "${targetUserWsChannelName}" to user "${targetUserId}" does not exist.`
            );
        }

        if (needsPeerConnection && !link.connection) {
            throw new WRTCStorageError(
                `Connection "${targetUserWsChannelName}" to user "${targetUserId}" does not exist.`
            );
        }

        return link;
    };

    const _getDataChannel = (targetUserId, targetUserWsChannelName, label) => {
        const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);
        const dataChannel = link.dataChannels[label];

        if (!dataChannel) {
            console.dir(link);
            throw new WRTCStorageError(
                `Data channel "${label}" to WebSocket link "${targetUserWsChannelName}" of user "${targetUserId}" does not exist.`
            );
        }

        return dataChannel;
    };

    const _hasNonLocalSelfConnections = currentUserId => !!_userStorage[currentUserId];

    const _removeUserFromStorage = targetUserId => {
        if (!_userStorage[targetUserId]) {
            return;
        }

        delete _userStorage[targetUserId];
    };

    /* WebRTC P2P setup */
    const _onPeerConnect = makeFunctionQueue();

    const _removeConnection = (targetUserId, targetUserWsChannelName) => {
        try {
            const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);

            // Don't remove connection yet if some data channel is still open.
            const allDataChannelsRemoved = Object.values(link.dataChannels).every(dataChannel => dataChannel == null);
            if (allDataChannelsRemoved) {
                link.connection.close();
                link.connection = null;
            }

            return allDataChannelsRemoved;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                // Already removed.
                return;
            }

            console.trace(error);
        }

        return true;
    };

    /* WebRTC data channels */
    const _getConversationIdFromForward = async (store, forward, targetUserId) => {
        const {type, value} = forward;

        switch (type) {
            case FORWARD.CONVERSATION_ID: {
                // Got the message from _sendToNonLocalSelfDataChannels() user conversation.
                const {id: currentUserId} = store.getState().auth.currentUser;

                if (targetUserId !== currentUserId) {
                    // Ignore message if it's not coming from _sendToNonLocalSelfDataChannels().
                    return null;
                }

                return value;
            }
            case FORWARD.GROUP_ID: {
                const {conversation_id} = await memoizedQueryGroupConversationId(store.dispatch, value);
                return conversation_id;
            }
            default: {
                // Empty forward, got user id from _userStorage.
                const {conversation_id} = await memoizedQueryUserConversationId(store.dispatch, targetUserId);
                return conversation_id;
            }
        }
    };

    const _getSelfPushMessageAction = (dispatch, conversationContext) => {
        const isConversation = !!conversationContext.conversation;

        if (isConversation) {
            return message => () => {
                (async () => {
                    const {conversation, isConversationEmpty} = conversationContext;

                    // Empty user conversations aren't visible.
                    // Group conversation are visible even when there are no messages.
                    if (isConversationEmpty && !!conversation.to_user) {
                        dispatch(conversationsPrependResult(conversation));
                    }

                    dispatch(conversationsPushMessage(message));
                    dispatch(conversationsSetLastMessage(conversation.id, message.content));
                })();
            };
        }

        return roomsPushMessage;
    };

    const _getPushMessageAction = (store, targetUserId, label) => {
        if (label === DATA_CHANNEL_LABEL.CONVERSATION) {
            return (forward, message, useActualMessage) => dispatch => {
                (async () => {
                    const {
                        list: {resultsIds: conversationsListIds},
                        details: currentlyOpenedConversation,
                    } = store.getState().conversations;

                    try {
                        const conversationId = await _getConversationIdFromForward(store, forward, targetUserId);
                        const hasConversationOpened = currentlyOpenedConversation.id === conversationId;
                        let conversationAcknowledged = conversationsListIds.includes(conversationId);

                        if (hasConversationOpened) {
                            const messageCopy = {...message};

                            if (useActualMessage) {
                                // API POST request to message endpoint is awaited, the message is then sent down to
                                // WebRTC DataChannel, which is ordered, so this state shouldn't get stale?
                                // TODO: altough this works, the api should just send a message down to sockets on post_save
                                const nextMessageIndex = store.getState().conversations.messages.count;

                                const {
                                    payload: {id, has_attachments},
                                } = store.dispatch(
                                    await querySupplementaryMessageData(conversationId, nextMessageIndex)({})
                                );

                                if (id) {
                                    messageCopy.id = id;
                                    messageCopy.has_attachments = has_attachments;
                                }
                            }

                            dispatch(conversationsPushMessage(messageCopy));

                            if (!conversationAcknowledged) {
                                // Details are serialized a bit differently in api,
                                // e.g. conversationSetLastMessage() dispatch is needed.
                                // Other than this, details return even more information than list result needs.
                                store.dispatch(conversationsPrependResult(currentlyOpenedConversation));
                                conversationAcknowledged = true;
                            }
                        }

                        if (!conversationAcknowledged) {
                            try {
                                const {payload: conversation} = store.dispatch(
                                    await conversationsQueryDetails(conversationId)({})
                                );
                                store.dispatch(conversationsPrependResult(conversation));
                            } catch (error) {
                                console.trace(error);
                            }
                        }

                        dispatch(conversationsSetLastMessage(conversationId, message.content));
                    } catch (error) {
                        console.trace(error);
                    }
                })();
            };
        }

        return (_, message) => roomsPushMessage(message);
    };

    const _unzipDataChannelMessage = dataChannelMessage => {
        const regex = /(.*);(.*);(.*);/;
        // eslint-disable-next-line no-unused-vars
        const [_, content, useActualMessage, ...forward] = dataChannelMessage.split(regex);
        return {content, useActualMessage: !!+useActualMessage, forward};
    };

    const _zipDataChannelMessage = (content, useActualMessage = '', forward = makeEmptyForward()) =>
        `${content};${useActualMessage ? 1 : 0};${Object.values(forward).join(';')}`;

    const _sendToWsWrtcLinks = (links, dataChannelLabel, zippedContent) => {
        if (typeof links === 'object' && links !== null) {
            Object.values(links).forEach(link => {
                const dataChannel = link.dataChannels[dataChannelLabel];

                if (dataChannel && dataChannel.readyState === 'open') {
                    dataChannel.send(zippedContent);
                }
                // TODO: throw if not open.
            });
        }
    };

    const _sendToSelf = (store, messageContext, conversationContext) => {
        const {currentUser} = store.getState().auth;

        const message =
            messageContext.useActualMessage && !messageContext.isError
                ? messageContext.actualMessage
                : makeConversationMessage(messageContext.content, currentUser.id);

        // Push hasError property into the redux store to display some feedback.
        message.hasError = messageContext.isError;

        const selfPushMessageAction = _getSelfPushMessageAction(store.dispatch, conversationContext);
        store.dispatch(selfPushMessageAction(message));
    };

    const _sendToNonLocalSelfDataChannels = (store, messageContext, {conversation}) => {
        const {currentUser} = store.getState().auth;
        const {content, isZipped, useActualMessage} = messageContext;

        // User could have same conversation opened on another device.
        // Push message to that non-local device, too.
        const nonLocalOwnLinks = _getUserFromStorage(currentUser.id).wsToWrtcLinks;
        const forward = makeForward(FORWARD.CONVERSATION_ID, conversation.id);
        const zippedContent = isZipped ? content : _zipDataChannelMessage(content, useActualMessage, forward);

        // Add conversation id context for other devices, otherwise
        // they've no clue where the message belongs.
        _sendToWsWrtcLinks(nonLocalOwnLinks, DATA_CHANNEL_LABEL.CONVERSATION, zippedContent);
    };

    const _sendToReceiversDataChannel = (store, messageContext, strippedConversationContext) => {
        const {content, isZipped, useActualMessage} = messageContext;
        const {recipients, conversationId, groupId} = strippedConversationContext;
        const isConversation = !!conversationId || isZipped;
        const label = DATA_CHANNEL_LABEL[isConversation ? 'CONVERSATION' : 'ROOM'];

        let zippedContent = content;
        if (!isZipped) {
            // Make empty forward if sending to other user conversation, the recipient
            // queries conversation id from user id in storage.
            const forward = groupId ? makeForward(FORWARD.GROUP_ID, groupId) : makeEmptyForward();
            zippedContent = _zipDataChannelMessage(content, useActualMessage, forward);
        }

        const disconnectedUsers = [];

        recipients.forEach(id => {
            const user = _userStorage[id];
            if (user) {
                // TODO: reducer conversation id push duplicates (if user spams with messages, the queue flushes too
                // quickly which results in multiple same conversations on the remote side).
                _sendToWsWrtcLinks(user.wsToWrtcLinks, label, zippedContent);
                return;
            }

            // Enqueue message to conversation if user isn't in storage yet.
            // Don't queue if content is zipped (coming from flushMessageQueue).
            if (isConversation && !isZipped) {
                const item = makeMessageQueueItem(MESSAGE_QUEUE.WEBRTC, makeKey(conversationId, id), zippedContent);
                pushItemToMessageQueue(item);
            }

            disconnectedUsers.push(id);
        });

        if (disconnectedUsers.length > 0) {
            const joinMessage = makeJoinMessage(disconnectedUsers);
            store.dispatch(wsSendRequest(joinMessage));
        }
    };

    const _removeDataChannel = (dispatch, targetUserId, targetUserWsChannelName, label) => {
        if (label === DATA_CHANNEL_LABEL.ROOM) {
            dispatch(roomsRemoveUserId(targetUserId));
        }

        try {
            const dataChannel = _getDataChannel(targetUserId, targetUserWsChannelName, label);
            dataChannel.close();

            const user = _getUserFromStorage(targetUserId);
            user.wsToWrtcLinks[targetUserWsChannelName].dataChannels[label] = null;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                // Already removed.
                return;
            }

            console.trace(error);
        }
    };

    const _removeLink = (targetUserId, targetUserWsChannelName) => {
        try {
            const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);

            if (link.connection == null) {
                delete _userStorage[targetUserId].wsToWrtcLinks[targetUserWsChannelName];
            }
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                // Already removed.
                return;
            }

            console.trace(error);
        }

        try {
            const user = _getUserFromStorage(targetUserId);

            // Return true if this was the last link to user.
            return Object.keys(user.wsToWrtcLinks).length === 0;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                // Already removed.
                return;
            }

            console.trace(error);
        }

        return true;
    };

    /* WebRTC media streams */
    const _onNewAudioNode = makeFunctionQueue();
    let _audioNodesMutationObserver = null;

    const _mediaStreams = {
        main: null,
        videoOnly: null,
        local: {
            audio: {
                instance: null,
                wsToWrtcLinks: [],
            },
            video: {
                instance: null,
                wsToWrtcLinks: [],
            },
        },
    };

    const _getMainMediaStream = async () => {
        if (_mediaStreams.main == null) {
            _mediaStreams.main = await navigator.mediaDevices.getUserMedia({audio: true});
        }

        return _mediaStreams.main;
    };

    const _getVideoOnlyMediaStream = async () => {
        if (_mediaStreams.videoOnly == null) {
            _mediaStreams.videoOnly = await navigator.mediaDevices.getUserMedia({video: true});
        }

        return _mediaStreams.videoOnly;
    };

    const _obtainAudioTrack = async store => {
        try {
            if (_mediaStreams.local.audio.instance == null) {
                const [audioTrack] = (await _getMainMediaStream()).getAudioTracks();
                _mediaStreams.local.audio.instance = audioTrack;
            }
        } catch (error) {
            if (error instanceof DOMException) {
                // Audio track not permitted or no microphone detected.
                tearDownCall(store, true);
            } else {
                console.trace(error);
            }
        }

        const audioSucceeded = _mediaStreams.main != null && _mediaStreams.local.audio.instance != null;
        return {audioSucceeded};
    };

    const _obtainAudioAndVideoTrack = async store => {
        const {audioSucceeded} = await _obtainAudioTrack(store);

        if (audioSucceeded) {
            try {
                if (_mediaStreams.local.video.instance == null) {
                    const [videoTrack] = (await _getVideoOnlyMediaStream()).getVideoTracks();
                    _mediaStreams.local.video.instance = videoTrack;
                }
            } catch (error) {
                if (error instanceof DOMException) {
                    // TODO:
                    // Camera not permitted or not found, do nothing for now.
                    console.trace(error);
                } else {
                    console.trace(error);
                }
            }
        }

        const videoSucceeded = _mediaStreams.videoOnly != null && _mediaStreams.local.video.instance != null;
        return {audioSucceeded, videoSucceeded};
    };

    const _addTracksToLink = (store, kinds, targetUserId, targetUserWsChannelName) => {
        try {
            const {connection, rtpSenders} = _getWsToWrtcLink(targetUserId, targetUserWsChannelName, true);

            kinds.forEach(kind => {
                if (!rtpSenders[kind] && _mediaStreams.local[kind].instance != null) {
                    rtpSenders[kind] = connection.addTrack(_mediaStreams.local[kind].instance, _mediaStreams.main);
                    _mediaStreams.local[kind].wsToWrtcLinks.push([targetUserId, targetUserWsChannelName]);

                    if (kind === 'audio') {
                        const {enabled} = _mediaStreams.local[kind].instance;
                        store.dispatch(callsUpdateActive({isAudioTrackEnabled: enabled}));
                    }
                }
            });

            return true;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                _onPeerConnect.schedule(targetUserWsChannelName, () =>
                    _addTracksToLink(store, kinds, targetUserId, targetUserWsChannelName)
                );

                return true;
            }

            // TODO: DOMException: InvalidAccessError, InvalidStateError
            console.trace(error);
        }

        return false;
    };

    const _removeTrackFromLink = (kind, targetUserId, targetUserWsChannelName) => {
        try {
            const {connection, rtpSenders} = _getWsToWrtcLink(targetUserId, targetUserWsChannelName, true);
            connection.removeTrack(rtpSenders[kind]);
            rtpSenders[kind] = null;
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                console.warn(
                    `The ${kind} rtp sender to user "${targetUserId}", \
                    channel "${targetUserWsChannelName}" was removed before _removeLink() was called.`
                );

                return;
            }

            if (error instanceof DOMException) {
                if (error.name === 'InvalidStateError') {
                    console.warn(
                        `The connection to user "${targetUserId}", channel "${targetUserWsChannelName}" is not open.`
                    );

                    return;
                }
            }

            console.trace(error);
        } finally {
            _mediaStreams.local[kind].wsToWrtcLinks = _mediaStreams.local[kind].wsToWrtcLinks.filter(
                ([userId, wsChannelName]) => userId !== targetUserId && wsChannelName !== targetUserWsChannelName
            );
        }
    };

    const _startAudioNodesMutationObserver = () => {
        if (_audioNodesMutationObserver == null) {
            _audioNodesMutationObserver = new MutationObserver(mutations => {
                mutations.forEach(mutation => {
                    if (mutation.type === 'childList') {
                        mutation.addedNodes.forEach(node => {
                            const targetUserWsChannelName = getTargetUserWsChannelNameFromNode(node);
                            _onNewAudioNode.flush(targetUserWsChannelName);
                        });
                    }
                });
            });

            const target = getAudioNodes();
            _audioNodesMutationObserver.observe(target, {childList: true});
        }
    };

    const _stopAudioNodesMutationObserver = () => {
        if (_audioNodesMutationObserver != null) {
            _audioNodesMutationObserver.takeRecords();
            _audioNodesMutationObserver.disconnect();
            _audioNodesMutationObserver = null;
        }
    };

    const _stopVideoOnlyStream = () => {
        if (_mediaStreams.main != null) {
            const mainVideoTracks = _mediaStreams.main.getVideoTracks();

            // Optional, so could as well be 0.
            console.assert(mainVideoTracks.length <= 1, {
                mainMediaStream: _mediaStreams.main,
                mainVideoTracks,
                error: 'There was more than one video track attached to the main media stream!',
            });

            mainVideoTracks.forEach(videoTrack => {
                videoTrack.stop();
                _mediaStreams.main.removeTrack(videoTrack);
            });
        }

        if (_mediaStreams.videoOnly != null) {
            _mediaStreams.local.video.wsToWrtcLinks.forEach(link => _removeTrackFromLink('video', ...link));

            const videoOnlyTracks = _mediaStreams.videoOnly.getTracks();
            videoOnlyTracks.forEach(videoTrack => videoTrack.stop());

            console.assert(videoOnlyTracks.length === 1, {
                videoOnlyMediaStream: _mediaStreams.videoOnly,
                videoOnlyTracks,
                error: 'There was more than one track attached to the video only media stream!',
            });

            _mediaStreams.videoOnly = null;
            _mediaStreams.local.video.instance = null;
        }
    };

    const _stopWholeMediaStream = () => {
        _stopVideoOnlyStream();

        if (_mediaStreams.main != null) {
            _mediaStreams.local.audio.wsToWrtcLinks.forEach(link => _removeTrackFromLink('audio', ...link));

            const mainTracks = _mediaStreams.main.getTracks();
            mainTracks.forEach(track => track.stop());
            _mediaStreams.main = null;
            _mediaStreams.local.audio.instance = null;
        }
    };

    /* Public API */

    /* Users */
    const addUserToStorage = (targetUserId, targetUserBroadcastedName) => {
        if (_userStorage[targetUserId]) {
            return;
        }

        _userStorage[targetUserId] = _makeDataForUser(targetUserId, targetUserBroadcastedName);
    };

    const cleanUserStorageGarbage = (dispatch, targetUserId, targetUserWsChannelName, dataChannelLabel) => {
        _removeDataChannel(dispatch, targetUserId, targetUserWsChannelName, dataChannelLabel);
        const allDataChannelsRemoved = _removeConnection(targetUserId, targetUserWsChannelName);

        if (allDataChannelsRemoved) {
            const allLinksRemoved = _removeLink(targetUserId, targetUserWsChannelName);

            if (allLinksRemoved) {
                _removeUserFromStorage(targetUserId, targetUserWsChannelName);
            }
        }
    };

    const wipeUserStorageData = (dispatch, dataChannelLabels, userIds = Object.keys(_userStorage)) => {
        // Wipes all data from user storage if userIds is undefined.

        userIds.forEach(id => {
            try {
                const user = _getUserFromStorage(id);

                Object.keys(user.wsToWrtcLinks).forEach(targetUserWsChannelName => {
                    dataChannelLabels.forEach(label =>
                        cleanUserStorageGarbage(dispatch, id, targetUserWsChannelName, label)
                    );
                });
            } catch (error) {
                if (error instanceof WRTCStorageError) {
                    // Already removed.
                    return;
                }

                console.trace(error);
            }
        });
    };

    /* WebRTC P2P setup */
    const addConnection = (store, targetUserId, targetUserWsChannelName, polite) => {
        const user = _getUserFromStorage(targetUserId);

        if (user.wsToWrtcLinks?.[targetUserWsChannelName]?.connection) {
            // Reuse the connection.
            // TODO: also check connection state.
            return user.wsToWrtcLinks[targetUserWsChannelName];
        }

        const connection = new RTCPeerConnection(WRTC_CONFIG);
        const link = (user.wsToWrtcLinks[targetUserWsChannelName] = _makeWsToWrtcLink(polite));
        link.connection = connection;

        connection.addEventListener('icecandidate', evnt =>
            _onConnectionIceCandidate(evnt, store.dispatch, targetUserWsChannelName)
        );

        connection.addEventListener('datachannel', evnt =>
            // Calls addDataChannel().
            _onReceivedDataChannel(evnt, store, targetUserId, targetUserWsChannelName)
        );

        connection.addEventListener('negotiationneeded', async () => {
            try {
                link.makingOffer = true;
                // Await so the peers won't race and setRemoteDescription in have-remote-answer state doesn't happen.
                await relayDescription(store.dispatch, link, targetUserWsChannelName);
            } catch (error) {
                console.error(error);
            } finally {
                link.makingOffer = false;
            }
        });

        connection.addEventListener('icegatheringstatechange', () => {
            // onconnectionstatechange is not implemented in firefox yet.
            if (['gathering', 'complete'].includes(connection.iceGatheringState)) {
                _onPeerConnect.flush(targetUserWsChannelName);
            }
        });

        connection.addEventListener('iceconnectionstatechange', () => {
            if (connection.iceConnectionState === 'failed') {
                connection.restartIce();
            }
        });

        connection.addEventListener('track', rtcEvnt => {
            const [stream] = rtcEvnt.streams;
            _onAddTrack(store.dispatch, stream, rtcEvnt.track.kind, targetUserId, targetUserWsChannelName);

            if (!stream.onremovetrack) {
                stream.onremovetrack = streamEvnt =>
                    _onRemoveTrack(store.dispatch, streamEvnt.track.kind, targetUserId, targetUserWsChannelName);
            }
        });

        return link;
    };

    const addIceCandidate = (targetUserId, targetUserWsChannelName, iceCandidate) => {
        try {
            const {connection} = _getWsToWrtcLink(targetUserId, targetUserWsChannelName, true);
            connection.addIceCandidate(iceCandidate);
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                _onPeerConnect.schedule(targetUserWsChannelName, () =>
                    addIceCandidate(targetUserId, targetUserWsChannelName, iceCandidate)
                );

                return;
            }

            // TODO: TypeError, DOMException: InvalidStateError, OperationError

            console.trace(error);
        }
    };

    /* WebRTC data channels */
    const addDataChannel = (store, targetUserId, targetUserWsChannelName, label, receivedDataChannel = null) => {
        const user = _getUserFromStorage(targetUserId);
        const link = _getWsToWrtcLink(targetUserId, targetUserWsChannelName);

        if (link.dataChannels[label] != null) {
            return false;
        }

        // TODO: InvalidStateError, TypeError, SyntaxError, ResourceInUse, OperationError
        const dataChannel = receivedDataChannel || link.connection.createDataChannel(label, {ordered: true});
        // TODO: check if this is garbage collected properly after removing the data channel
        const pushMessageAction = _getPushMessageAction(store, targetUserId, label);

        dataChannel.addEventListener('message', evnt =>
            _onDataChannelMessage(evnt, store.dispatch, user.id, pushMessageAction)
        );

        link.dataChannels[label] = dataChannel;

        if (label === DATA_CHANNEL_LABEL.ROOM) {
            // Every unauthenticated user gets his own user id.
            store.dispatch(roomsPushUserId(targetUserId));
        }

        return true;
    };

    const sendToDataChannels = (store, messageContext, conversationContext) => {
        _sendToSelf(store, messageContext, conversationContext);

        if (!messageContext.isError) {
            const {currentUser} = store.getState().auth;
            const isConversation = !!conversationContext.conversation;

            if (isConversation && _hasNonLocalSelfConnections(currentUser.id)) {
                // More than one same user id cannot join room.
                _sendToNonLocalSelfDataChannels(store, messageContext, conversationContext);
            }

            const strippedConversationContext = stripConversationContext(conversationContext);
            _sendToReceiversDataChannel(store, messageContext, strippedConversationContext);
        }
    };

    /* WebRTC media streams */
    const obtainMediaContext = async (store, callType) => {
        const getDesiredTracks = callType === CALL_TYPE.AUDIO ? _obtainAudioTrack : _obtainAudioAndVideoTrack;
        const {audioSucceeded, videoSucceeded} = await getDesiredTracks(store);

        if (!audioSucceeded) {
            return false;
        }

        let nextCallType = callType;
        if (typeof videoSucceeded !== 'undefined' && !videoSucceeded) {
            // TODO: display modal or something.
            nextCallType = CALL_TYPE.AUDIO;
        }

        store.dispatch(callsUpdateActive({type: nextCallType}));
        return true;
    };

    const setUpCall = async (store, targetUserId, targetUserWsChannelName, isFinished) => {
        addLeaveCallAlertBeforeUnload();
        _startAudioNodesMutationObserver(); // Remote audio stream is sometimes received before the node is added.
        addAudioNode(targetUserWsChannelName);
        // Video isn't added if wasn't requested by user.
        const result = _addTracksToLink(store, ['audio', 'video'], targetUserId, targetUserWsChannelName);

        if (isFinished) {
            store.dispatch(
                callsUpdateActive({
                    status: CALL_STATUS.CONNECTED,
                    startedAt: new Date().toJSON(),
                })
            );
        }

        return result;
    };

    const finalizeCallSetup = (store, targetUserId, targetUserWsChannelName) => {
        try {
            _getWsToWrtcLink(targetUserId, targetUserWsChannelName);
            setUpCall(store, targetUserId, targetUserWsChannelName, true);
        } catch (error) {
            if (error instanceof WRTCStorageError) {
                _onPeerConnect.schedule(targetUserWsChannelName, () =>
                    finalizeCallSetup(store, targetUserId, targetUserWsChannelName)
                );

                const joinMessage = makeJoinMessage([targetUserId]);
                store.dispatch(wsSendMessage(joinMessage));

                return;
            }

            console.trace(error);
        }
    };

    const triggerAudioTrackMute = async store => {
        const {instance: audioTrack} = _mediaStreams.local.audio;
        if (audioTrack == null) {
            console.error('Audio track instance not found.');
            return;
        }

        audioTrack.enabled = !audioTrack.enabled;
        store.dispatch(callsUpdateActive({isAudioTrackEnabled: audioTrack.enabled}));
    };

    const triggerVideoTrackVisibility = (() => {
        let lock = false;

        return async store => {
            if (lock) {
                // When user spams trigger camera button getUserMedia() is getting called more than once.
                // _mediaStreams object stores reference to the last MediaStream, which causes memory leak.
                // User's camera doesn't retract until the garbage collector is ran manually from devtools or
                // automatically some time later.
                return;
            }

            lock = true;
            const {videoSucceeded} = await _obtainAudioAndVideoTrack(store);

            if (!videoSucceeded) {
                lock = false;
                return;
            }

            let nextCallType;
            const {type: callType} = store.getState().calls.active;

            if (callType === CALL_TYPE.AUDIO) {
                // Add video track to every audio receiver => every participant in call.
                _mediaStreams.local.audio.wsToWrtcLinks.forEach(link => _addTracksToLink(store, ['video'], ...link));
                nextCallType = CALL_TYPE.AUDIO_AND_VIDEO;
            } else {
                _stopVideoOnlyStream();
                nextCallType = CALL_TYPE.AUDIO;
            }

            store.dispatch(callsUpdateActive({type: nextCallType}));
            lock = false;
        };
    })();

    const getVideoStreams = videoSenders =>
        Object.fromEntries(
            videoSenders
                .map(([userId, wsChannelName]) => {
                    try {
                        const link = _getWsToWrtcLink(userId, wsChannelName);
                        return [wsChannelName, link.streams.video];
                    } catch (error) {
                        if (error instanceof WRTCStorageError) {
                            return null;
                        }

                        console.trace(error);
                        return null;
                    }
                })
                .filter(value => value != null)
        );

    const tearDownCall = (store, redirectToDetails, targetUserId, targetUserWsChannelName) => {
        console.assert((targetUserId && targetUserWsChannelName) || (!targetUserId && !targetUserWsChannelName), {
            targetUserId,
            targetUserWsChannelName,
            error: 'Tried to call tearDownCall() with only one of these arguments.',
        });

        const {conversationId, type: callType} = store.getState().calls.active;

        if (targetUserId && targetUserWsChannelName) {
            // TODO: check that
            // When hang up ws event is received from the call participant.
            removeAudioNode(targetUserWsChannelName);

            if (callType === CALL_TYPE.AUDIO_AND_VIDEO) {
                _removeTrackFromLink('video', targetUserId, targetUserWsChannelName);
            }

            _removeTrackFromLink('audio', targetUserId, targetUserWsChannelName);

            const isLastTrack = getAudioNodes().childElementCount === 0;
            if (!isLastTrack) {
                // Don't hang up the call altogether if there are remaining users in the call.
                return;
            }

            // Also makes all other incoming calls hang up since this is the call sender hanging up.
            const hangUpCallMessage = makeHangUpCallMessage();
            store.dispatch(wsSendMessage(hangUpCallMessage));
        }

        // When hang up ws event is sent from this socket or received from the last call participant.
        store.dispatch(callsUpdateActive({status: CALL_STATUS.DISCONNECTING}));
        _stopWholeMediaStream();
        _stopAudioNodesMutationObserver();
        removeAudioNodes();
        removeLeaveCallAlertBeforeUnload();
        redirectFromCall(conversationId, redirectToDetails);
        store.dispatch(callsUpdateActive({...CALL_ACTIVE_INITIAL_STATE}));
    };

    return store => next => action => {
        switch (action.type) {
            case ACTION.ADD_USER_TO_STORAGE: {
                const {targetUserId, targetUserBroadcastedName} = action.payload;
                addUserToStorage(targetUserId, targetUserBroadcastedName);
                break;
            }
            case ACTION.ADD_CONNECTION: {
                const {targetUserId, targetUserWsChannelName, polite} = action.payload;
                return addConnection(store, targetUserId, targetUserWsChannelName, polite);
            }
            case ACTION.ADD_DATA_CHANNEL: {
                const {targetUserId, targetUserWsChannelName, label} = action.payload;
                return addDataChannel(store, targetUserId, targetUserWsChannelName, label);
            }
            case ACTION.ADD_ICE_CANDIDATE: {
                const {targetUserId, targetUserWsChannelName, iceCandidate} = action.payload;
                addIceCandidate(targetUserId, targetUserWsChannelName, iceCandidate);
                break;
            }
            case ACTION.SEND_TO_DATA_CHANNELS: {
                const {messageContext, conversationContext} = action.payload;
                sendToDataChannels(store, messageContext, conversationContext);
                break;
            }
            case ACTION.OBTAIN_MEDIA_CONTEXT:
                return obtainMediaContext(store, action.payload);
            case ACTION.SET_UP_CALL: {
                const {targetUserId, targetUserWsChannelName, isFinished} = action.payload;
                return setUpCall(store, targetUserId, targetUserWsChannelName, isFinished);
            }
            case ACTION.FINALIZE_CALL_SETUP: {
                const {targetUserId, targetUserWsChannelName} = action.payload;
                finalizeCallSetup(store, targetUserId, targetUserWsChannelName);
                break;
            }
            case ACTION.TRIGGER_AUDIO_TRACK_MUTE:
                triggerAudioTrackMute(store);
                break;
            case ACTION.TRIGGER_VIDEO_TRACK_VISIBILITY:
                triggerVideoTrackVisibility(store);
                break;
            case ACTION.GET_VIDEO_STREAMS:
                return getVideoStreams(action.payload);
            case ACTION.TEAR_DOWN_CALL: {
                const {redirectToDetails, targetUserId, targetUserWsChannelName} = action.payload;
                // TODO: actions
                tearDownCall(store, redirectToDetails, targetUserId, targetUserWsChannelName);
                break;
            }
            case ACTION.CLEAN_USER_STORAGE_GARBAGE: {
                const {targetUserId, targetUserWsChannelName, dataChannelLabel} = action.payload;
                cleanUserStorageGarbage(store.dispatch, targetUserId, targetUserWsChannelName, dataChannelLabel);
                break;
            }
            case ACTION.WIPE_USER_STORAGE_DATA: {
                const {dataChannelLabels, userIds} = action.payload;
                wipeUserStorageData(store.dispatch, dataChannelLabels, userIds);
                break;
            }
            default:
                return next(action);
        }
    };
};

export default wrtcMiddleware();
