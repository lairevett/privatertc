import {useEffect} from 'react';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import {getDisplayName} from '../../app/utils/display_name';
import {retrieveCurrentUser, setCurrentUser, setIsAuthenticated} from '../actions';

export const useAuthAttempt = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            const {payload: user} = dispatch(await retrieveCurrentUser()());
            dispatch(setIsAuthenticated(!!user.username));
            dispatch(setCurrentUser(user));
        })();
    }, [dispatch]);
};

export const useAuthStatus = () => useSelector(state => state.auth.currentUser.__meta__.status);

export const useAuthCheck = () => useSelector(state => state.auth.isAuthenticated);

export const useAdminCheck = () => useSelector(state => state.auth.currentUser.is_staff);

export const useCurrentUser = () => useSelector(state => state.auth.currentUser, shallowEqual);

export const useCurrentUserDisplayName = () => {
    const currentUser = useCurrentUser();
    return getDisplayName(currentUser);
};
