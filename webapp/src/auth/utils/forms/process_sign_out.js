import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {history, retrieveUrl} from '../../../app/utils/router';
import {SELF_DETAILS_INITIAL_STATE} from '../../../users/constants';
import {DATA_CHANNEL_LABEL} from '../../../wrtc/constants';
import {wipeUserStorageData as wrtcWipeUserStorageData} from '../../../wrtc/actions';
import {reconnect as wsReconnect} from '../../../ws/actions';
import {signOut, setCurrentUser, setIsAuthenticated} from '../../actions';

export const submit = async (_, dispatch) => {
    const {payload} = dispatch(await signOut()());
    apiCheckSubmitErrors(payload);

    dispatch(wsReconnect());
    dispatch(setCurrentUser({...SELF_DETAILS_INITIAL_STATE, ...payload}));
    dispatch(setIsAuthenticated(false));
    dispatch(wrtcWipeUserStorageData(Object.values(DATA_CHANNEL_LABEL), undefined));

    history.push(retrieveUrl('auth:sign_in').path);
};
