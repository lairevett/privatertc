import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {history, retrieveUrl} from '../../../app/utils/router';
import {reconnect as wsReconnect} from '../../../ws/actions';
import {validateUsername, validatePassword} from '../../../app/utils/validation';
import {signIn, retrieveCurrentUser, setCurrentUser, setIsAuthenticated} from '../../actions';

export const validate = ({username, password}) => {
    const errors = {};
    errors.username = validateUsername(username);
    errors.password = validatePassword(password);
    return errors;
};

export const submit = async ({username, password}, dispatch) => {
    const {payload} = dispatch(await signIn(username, password)());
    apiCheckSubmitErrors(payload);

    const {payload: user} = dispatch(await retrieveCurrentUser()());
    dispatch(setCurrentUser(user));
    dispatch(setIsAuthenticated(!!user.username));
    dispatch(wsReconnect());
    history.push(retrieveUrl('conversations:list').path);
};
