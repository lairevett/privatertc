import {DEFAULT_META_INITIAL_STATE} from '../app/utils/api/constants';
import {SELF_DETAILS_INITIAL_STATE as USER_SELF_DETAILS_INITIAL_STATE} from '../users/constants';

export const MODULE_NAME = 'auth';

// API endpoints.
export const ENDPOINT = {
    AUTH: `/${MODULE_NAME}/`,
    UPDATE_BROADCASTED_NAME: `/${MODULE_NAME}/update_broadcasted_name/`,
    UPDATE_PASSWORD: `/${MODULE_NAME}/update_password/`,
    DELETE_ACCOUNT: `/${MODULE_NAME}/delete_account/`,
};

// Redux action types.
export const ACTION = {
    SET_IS_AUTHENTICATED: `@${MODULE_NAME}/SET_IS_AUTHENTICATED`,
    SET_CURRENT_USER: `@${MODULE_NAME}/SET_CURRENT_USER`,
    RETRIEVE_CURRENT_USER: `@${MODULE_NAME}/RETRIEVE_CURRENT_USER`,
    UPDATE_BROADCASTED_NAME: `@${MODULE_NAME}/UPDATE_BROADCASTED_NAME`,
    UPDATE_PASSWORD: `@${MODULE_NAME}/UPDATE_PASSWORD`,
    DELETE_ACCOUNT: `@${MODULE_NAME}/DELETE_ACCOUNT`,
    SIGN_IN: `@${MODULE_NAME}/SIGN_IN`,
    SIGN_OUT: `@${MODULE_NAME}/SIGN_OUT`,
};

// Reducer initial states.
export const INITIAL_STATE = {
    isAuthenticated: false,
    currentUser: USER_SELF_DETAILS_INITIAL_STATE,
    signIn: DEFAULT_META_INITIAL_STATE,
    signOut: DEFAULT_META_INITIAL_STATE,
};
