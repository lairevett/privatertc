import {RequireSignedOut} from '../app/components/partials/route_access';
import {registerUrl} from '../app/utils/router';
import SignIn from './components/sign_in';
import {MODULE_NAME} from './constants';

registerUrl(`${MODULE_NAME}:sign_in`, `/${MODULE_NAME}/sign-in`, [RequireSignedOut, SignIn]);
