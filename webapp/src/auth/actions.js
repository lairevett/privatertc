import {apiPostRequest, apiDeleteRequest, apiGetRequest} from '../app/utils/api/requests';
import {ACTION, ENDPOINT} from './constants';
import {SELF_DETAILS_INITIAL_STATE} from '../users/constants';
import {DEFAULT_META_INITIAL_STATE} from '../app/utils/api/constants';

export const setIsAuthenticated = isAuthenticated => ({type: ACTION.SET_IS_AUTHENTICATED, payload: isAuthenticated});

export const setCurrentUser = userObject => ({type: ACTION.SET_CURRENT_USER, payload: userObject});

export const retrieveCurrentUser = () =>
    apiGetRequest(ACTION.RETRIEVE_CURRENT_USER, SELF_DETAILS_INITIAL_STATE, ENDPOINT.AUTH);

export const signIn = (username, password) =>
    apiPostRequest(ACTION.SIGN_IN, DEFAULT_META_INITIAL_STATE, ENDPOINT.AUTH, {username, password});

export const signOut = () => apiDeleteRequest(ACTION.SIGN_OUT, DEFAULT_META_INITIAL_STATE, ENDPOINT.AUTH);

export const updateBroadcastedName = broadcastedName =>
    apiPostRequest(ACTION.UPDATE_BROADCASTED_NAME, {broadcasted_name: ''}, ENDPOINT.UPDATE_BROADCASTED_NAME, {
        broadcasted_name: broadcastedName,
    });

export const updatePassword = (currentPassword, password) =>
    apiPostRequest(ACTION.UPDATE_PASSWORD, null, ENDPOINT.UPDATE_PASSWORD, {
        current_password: currentPassword,
        password,
    });

export const deleteAccount = password =>
    apiPostRequest(ACTION.DELETE_ACCOUNT, null, ENDPOINT.DELETE_ACCOUNT, {password});
