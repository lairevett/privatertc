import React from 'react';
import {Field, reduxForm, propTypes} from 'redux-form';
import {Form, FormField} from '../../../app/components/partials/form';
import FormError from '../../../app/components/partials/form_error';
import SubmitButton from '../../../app/components/partials/submit_button';
import {validate, submit} from '../../utils/forms/process_sign_in';

const SignInForm = ({handleSubmit, valid, pristine, submitting, error}) => (
    <Form>
        {error && <FormError message={error} />}
        <form onSubmit={handleSubmit(submit)}>
            <Field component={FormField} id="username" name="username" label="Username" type="text" />
            <Field component={FormField} id="password" name="password" label="Password" type="password" />
            <div className="text-center mt-5">
                <SubmitButton disabled={!valid || pristine || submitting} value="Sign in" />
            </div>
        </form>
    </Form>
);

SignInForm.propTypes = propTypes;

export default reduxForm({
    form: 'authSignIn',
    validate,
})(SignInForm);
