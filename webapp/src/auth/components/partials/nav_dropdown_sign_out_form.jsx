import React from 'react';
import NavDropdownForm from '../../../app/components/partials/nav_dropdown_form';
import {submit} from '../../utils/forms/process_sign_out';

const NavDropdownSignOutForm = () => (
    <NavDropdownForm form="navDropdownSignOut" processSubmit={submit} value="Sign out" />
);

export default NavDropdownSignOutForm;
