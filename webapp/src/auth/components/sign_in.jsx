import React from 'react';
import RegularView from '../../app/components/partials/regular_view';
import SignInForm from './partials/sign_in_form';

const SignIn = () => (
    <RegularView>
        <SignInForm />
    </RegularView>
);

export default SignIn;
