import {getDisplayName} from '../app/utils/display_name';
import {INITIAL_STATE, ACTION} from './constants';

const authReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.SET_IS_AUTHENTICATED:
            return {
                ...state,
                isAuthenticated: action.payload,
            };
        case ACTION.SET_CURRENT_USER:
            return {
                ...state,
                currentUser: {
                    ...action.payload,
                    displayName: getDisplayName(action.payload),
                },
            };
        case ACTION.SIGN_IN:
            return {
                ...state,
                signIn: action.payload,
            };
        case ACTION.SIGN_OUT:
            return {
                ...state,
                signOut: action.payload,
            };
        case ACTION.UPDATE_BROADCASTED_NAME: {
            const currentUser = {...state.currentUser, broadcasted_name: action.payload.broadcasted_name};
            currentUser.displayName = getDisplayName(currentUser);

            return {
                ...state,
                currentUser,
            };
        }
        default:
            return state;
    }
};

export default authReducer;
