import {INITIAL_STATE, ACTION} from '../constants';

const appReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_CONFIG:
            return {
                ...state,
                config: action.payload,
            };
        case ACTION.SET_NAV_TITLE:
            return {
                ...state,
                navTitle: action.payload,
            };
        case ACTION.SET_SHOW_BACK_BUTTON:
            return {
                ...state,
                showBackButton: action.payload,
            };
        case ACTION.SET_CURRENT_VIEW_NAME:
            return {
                ...state,
                currentViewName: action.payload,
            };
        default:
            return state;
    }
};

export default appReducer;
