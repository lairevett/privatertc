import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import appReducer from './app';
import authReducer from '../../auth/reducer';
import callsReducer from '../../calls/reducer';
import conversationsReducer from '../../conversations/reducer';
import groupsReducer from '../../groups/reducer';
import roomsReducer from '../../rooms/reducer';
import usersReducer from '../../users/reducer';
import wsReducer from '../../ws/reducer';

const reducers = {
    form: formReducer,
    app: appReducer,
    auth: authReducer,
    calls: callsReducer,
    conversations: conversationsReducer,
    groups: groupsReducer,
    rooms: roomsReducer,
    users: usersReducer,
    ws: wsReducer,
};

const makeReducers = () => combineReducers(reducers);

export default makeReducers;
