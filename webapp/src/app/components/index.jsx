import React from 'react';
import {Redirect} from 'react-router-dom';
import {useAuthCheck} from '../../auth/utils/hooks';
import {retrieveUrl} from '../utils/router';

const Index = () => {
    const isAuthenticated = useAuthCheck();
    const {path: redirectPath} = retrieveUrl(isAuthenticated ? 'conversations:list' : 'auth:sign_in');

    return <Redirect to={redirectPath} />;
};

export default Index;
