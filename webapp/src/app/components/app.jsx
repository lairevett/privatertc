import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {Router} from 'react-router-dom';
import 'file-icon-vectors/dist/file-icon-vivid.css';
import {useAuthAttempt, useAuthStatus} from '../../auth/utils/hooks';
import {connect as wsConnect, disconnect as wsDisconnect} from '../../ws/actions';
import {history} from '../utils/router';
import {useLocationHashModals} from '../utils/modals';
import {retrieveConfig} from '../actions';
import '../urls';
import '../nav';
import Layout from './partials/layout';
import Suspense from './partials/suspense';
import Routes from './partials/routes';

const App = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            dispatch(await retrieveConfig()());
        })();
    }, [dispatch]);

    useAuthAttempt();
    const status = useAuthStatus();

    useEffect(() => {
        dispatch(wsConnect());

        return () => {
            dispatch(wsDisconnect());
        };
    }, [dispatch]);

    useLocationHashModals();

    return (
        <Router history={history}>
            <Layout>
                <Suspense status={status} fallback={<></>}>
                    <Routes />
                </Suspense>
            </Layout>
        </Router>
    );
};

export default App;
