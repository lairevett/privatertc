import React from 'react';
import {useCurrentUser} from '../../auth/utils/hooks';
import ScrollView from './partials/scroll_view';
import ViewSection from './partials/view_section';
import SettingsUpdateBroadcastedNameForm from './partials/settings_update_broadcasted_name_form';
import SettingsUpdatePasswordForm from './partials/settings_update_password_form';
import SettingsDeleteAccountForm from './partials/settings_delete_account_form';

const Settings = () => {
    const currentUser = useCurrentUser();

    return (
        <ScrollView>
            <ViewSection title="Broadcasted name">
                <SettingsUpdateBroadcastedNameForm
                    initialValues={{broadcastedName: currentUser.broadcasted_name}}
                    currentUserId={currentUser.id}
                />
            </ViewSection>
            <ViewSection title="Password">
                <SettingsUpdatePasswordForm />
            </ViewSection>
            <ViewSection title="Delete account">
                <SettingsDeleteAccountForm />
            </ViewSection>
        </ScrollView>
    );
};

export default Settings;
