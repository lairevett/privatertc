import React from 'react';
import PropTypes from 'prop-types';

const FormError = ({message, children}) => (
    <div className="alert alert-danger" role="alert">
        {message ?? children ?? 'Check if all fields are filled correctly and try again.'}
    </div>
);

FormError.propTypes = {
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

FormError.defaultProps = {
    message: null,
    children: null,
};

export default FormError;
