import React from 'react';
import PropTypes from 'prop-types';
import RegularView from './regular_view';

const ScrollView = ({children}) => <RegularView appendClassName="overflow-y-auto">{children}</RegularView>;

ScrollView.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
};

ScrollView.defaultProps = {
    children: null,
};

export default ScrollView;
