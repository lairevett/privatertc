import React from 'react';
import {retrieveUrl} from '../../utils/router';
import NavLink from './nav_link';

const NavLinks = () => (
    <>
        <NavLink to={retrieveUrl('conversations:list').path} value="Conversations" />
        <NavLink to={retrieveUrl('users:list').path} value="Users" />
        <NavLink to={retrieveUrl('calls:list').path} value="Calls" />
    </>
);

export default NavLinks;
