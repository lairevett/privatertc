import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import TableListCell from './table_list_cell';

const TableListRow = ({
    children,
    appendClassName,
    to,
    onClick,
    disabled,
    isActive,
    turnSymbolOnActive,
    activeSymbol,
    inactiveSymbol,
    afterLink,
    ...properties
}) => {
    const handleClick = evnt => {
        if (to === '#') {
            // Prevent component from re-rendering when an empty link is clicked at /users/list.
            evnt.preventDefault();
        }

        if (!disabled && onClick) {
            onClick(evnt);
        }
    };

    const linkBackgroundClass = isActive ? ' bg-light' : '';
    const symbolColorClass = disabled ? 'text-muted' : 'text-primary';

    return (
        <li className={`table-list-row ${appendClassName}`} {...properties}>
            {to ? (
                <>
                    <Link to={to} onClick={handleClick} className={`btn${linkBackgroundClass} no-focus-color`}>
                        {children}
                        {turnSymbolOnActive && (
                            <TableListCell>
                                <span className={`material-icons vertical-center ${symbolColorClass}`}>
                                    {isActive ? activeSymbol : inactiveSymbol}
                                </span>
                            </TableListCell>
                        )}
                    </Link>
                    {afterLink}
                </>
            ) : (
                children
            )}
        </li>
    );
};

TableListRow.propTypes = {
    children: PropTypes.node.isRequired,
    appendClassName: PropTypes.string,
    to: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    isActive: PropTypes.bool,
    turnSymbolOnActive: PropTypes.bool,
    activeSymbol: PropTypes.string,
    inactiveSymbol: PropTypes.string,
    afterLink: PropTypes.node,
};

TableListRow.defaultProps = {
    appendClassName: '',
    to: null,
    onClick: null,
    disabled: false,
    isActive: false,
    turnSymbolOnActive: false,
    activeSymbol: 'keyboard_arrow_left',
    inactiveSymbol: 'keyboard_arrow_right',
    afterLink: null,
};

export default TableListRow;
