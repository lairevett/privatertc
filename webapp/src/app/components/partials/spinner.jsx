import React from 'react';
import PropTypes from 'prop-types';

const Spinner = ({size}) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 100 100"
        preserveAspectRatio="xMidYMid"
        style={{width: size, height: size}}
    >
        <circle cx="50" cy="50" fill="none" stroke="rgb(0, 0, 0, 0.50)" strokeWidth="4" r="32" strokeDasharray="96 96">
            <animateTransform
                attributeName="transform"
                type="rotate"
                repeatCount="indefinite"
                dur="1.25s"
                values="0 50 50;360 50 50"
                keyTimes="0;1"
            />
        </circle>
    </svg>
);

Spinner.propTypes = {
    size: PropTypes.string.isRequired,
};

export default Spinner;
