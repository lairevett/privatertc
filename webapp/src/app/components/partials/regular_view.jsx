import React from 'react';
import PropTypes from 'prop-types';

const RegularView = ({children, appendClassName}) => <div className={`card h-100 ${appendClassName}`}>{children}</div>;

RegularView.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    appendClassName: PropTypes.string,
};

RegularView.defaultProps = {
    children: null,
    appendClassName: '',
};

export default RegularView;
