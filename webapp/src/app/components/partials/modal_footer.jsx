import React from 'react';
import PropTypes from 'prop-types';

const ModalFooter = ({children}) => <div className="modal-footer">{children}</div>;

ModalFooter.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]).isRequired,
};

export default ModalFooter;
