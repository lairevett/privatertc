import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import RegularView from './regular_view';

const MasterDetailView = ({master, detail, preferDetail}) => {
    const masterReference = useRef();
    const detailReference = useRef();
    const hideClasses = 'd-none d-md-block';

    useEffect(() => {
        // Hides master/detail on mobile.
        const {current} = preferDetail ? masterReference : detailReference;
        current.className += ` ${hideClasses}`;
    }, [preferDetail]);

    return (
        <div className="row m-0 h-100">
            <div ref={masterReference} className="col-md-4 col-lg-3 p-0 h-100">
                {master}
            </div>
            <div ref={detailReference} className="col-md-8 col-lg-9 p-0 h-100">
                {detail}
            </div>
        </div>
    );
};

MasterDetailView.propTypes = {
    master: PropTypes.node.isRequired,
    detail: PropTypes.node,
    preferDetail: PropTypes.bool,
};

MasterDetailView.defaultProps = {
    detail: <RegularView />,
    preferDetail: false,
};

export default MasterDetailView;
