import React from 'react';
import {Field, propTypes, reduxForm} from 'redux-form';
import {validate, submit} from '../../utils/forms/process_settings_delete_account';
import {MODULE_NAME} from '../../constants';
import {Form, FormField} from './form';
import FormError from './form_error';
import SubmitButton from './submit_button';

const SettingsDeleteAccountForm = ({handleSubmit, error, valid, pristine, submitting}) => (
    <div className="mb-4">
        <Form>
            {error && <FormError message={error} />}
            <form onSubmit={handleSubmit(submit)}>
                <Field component={FormField} id="password" name="password" type="password" label="Password" />
                <div className="text-center mt-3">
                    <SubmitButton disabled={!valid || pristine || submitting} value="Delete" />
                </div>
            </form>
        </Form>
    </div>
);

SettingsDeleteAccountForm.propTypes = propTypes;

export default reduxForm({form: `${MODULE_NAME}SettingsDeleteAccount`, validate})(SettingsDeleteAccountForm);
