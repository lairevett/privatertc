/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import {handleFileChange} from '../../utils/form';

export const Form = ({children}) => (
    <div className="my-auto">
        <div className="col-9 col-md-6 mx-auto">{children}</div>
    </div>
);

export const FormCard = ({children}) => (
    <Form>
        <div className="card">
            <div className="card-body">{children}</div>
        </div>
    </Form>
);

FormCard.propTypes = {
    children: PropTypes.node.isRequired,
};

export const FormField = ({
    id,
    input,
    label,
    type,
    doNotValidate,
    meta: {touched, error},
    small,
    children,
    plainText,
    formGroupClassNames,
    inputClassNames,
    value,
    ...restOfProperties
}) =>
    type === 'checkbox' ? (
        <div className="form-check">
            <input
                className={`form-check-input${inputClassNames ? ` ${inputClassNames}` : ''}
                ${touched ? (error ? ' is-invalid' : '') : ''}`}
                {...input}
                type="checkbox"
                {...restOfProperties}
            />
            <label htmlFor={id} className="form-check-label">
                {label}
            </label>
            <div className="invalid-feedback">{error}</div>
        </div>
    ) : type === 'hidden' ? (
        <input {...input} className={inputClassNames || ''} type="hidden" required readOnly {...restOfProperties} />
    ) : (
        <div className={`form-group bmd-form-group${formGroupClassNames ? ` ${formGroupClassNames}` : ''}`}>
            {label ? (
                <label htmlFor={id} className="bmd-label-floating">
                    {label}
                </label>
            ) : (
                ''
            )}
            {type === 'textarea' ? (
                <textarea
                    className={`form-control${inputClassNames ? ` ${inputClassNames}` : ''}
                    ${doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''}`}
                    {...input}
                    {...restOfProperties}
                >
                    {children}
                </textarea>
            ) : type === 'select' ? (
                <select
                    className={`custom-select${inputClassNames ? ` ${inputClassNames}` : ''}
                    ${doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''}`}
                    {...input}
                    {...restOfProperties}
                >
                    {children}
                </select>
            ) : type !== 'select' && type !== 'textarea' ? (
                <input
                    type={type}
                    {...input}
                    className={`${plainText ? 'form-control-plaintext' : 'form-control'}${
                        inputClassNames ? ` ${inputClassNames}` : ''
                    }
                    ${doNotValidate ? '' : touched ? (error ? ' is-invalid' : ' is-valid') : ''}${
                        small ? ' form-control-sm' : ''
                    }`}
                    {...restOfProperties}
                />
            ) : (
                'Wrong type...'
            )}
            <div className="invalid-feedback">{error}</div>
        </div>
    );

export const FormHorizontalField = ({id, input, label, type, min, max, maxLength, meta: {touched, error}}) => (
    <div className="form-group bmd-form-group row">
        <label htmlFor={id} className="col col-form-label bmd-label-floating">
            {label}
        </label>
        <div className="col">
            <input
                id={id}
                {...input}
                type={type}
                className={`form-control${touched ? (error ? ' is-invalid' : '') : ''}`}
                min={min}
                max={max}
                maxLength={maxLength}
            />
        </div>
    </div>
);

export const FormFileField = ({
    input: {onChange, onBlur, value: omitValue, ...inputProperties},
    meta: {touched, error},
    id,
    label,
    placeholder,
    formGroupClassNames,
    labelClassNames,
    ...properties
}) => (
    <div className={`form-group bmd-form-group${formGroupClassNames ? ` ${formGroupClassNames}` : ''}`}>
        {label ? (
            <label htmlFor={id} className={labelClassNames}>
                {label}
            </label>
        ) : (
            ''
        )}
        <input
            id={id}
            className={`${touched ? (error ? ' is-invalid' : ' is-valid') : ''}`}
            {...inputProperties}
            {...properties}
            type="file"
            onChange={handleFileChange(onChange)}
            onBlur={handleFileChange(onBlur)}
        />
        <div className="invalid-feedback">{error}</div>
    </div>
);
