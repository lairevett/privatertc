import React from 'react';
import {Field, propTypes, reduxForm} from 'redux-form';
import PropTypes from 'prop-types';
import {validate, submit} from '../../utils/forms/process_settings_update_broadcasted_name';
import {MODULE_NAME} from '../../constants';
import {Form, FormField} from './form';
import FormError from './form_error';
import SubmitButton from './submit_button';

const SettingsUpdateBroadcastedNameForm = ({handleSubmit, error, valid, pristine, submitting, currentUserId}) => (
    <div className="mb-4">
        <Form>
            {error && <FormError message={error} />}
            <form onSubmit={handleSubmit(submit)}>
                <Field
                    component={FormField}
                    id="broadcastedName"
                    name="broadcastedName"
                    type="text"
                    placeholder={currentUserId}
                />
                <div className="text-center mt-3">
                    <SubmitButton disabled={!valid || pristine || submitting} value="Update" />
                </div>
            </form>
        </Form>
    </div>
);

SettingsUpdateBroadcastedNameForm.propTypes = {
    ...propTypes,
    currentUserId: PropTypes.string.isRequired,
};

export default reduxForm({form: `${MODULE_NAME}SettingsUpdateBroadcastedName`, validate})(
    SettingsUpdateBroadcastedNameForm
);
