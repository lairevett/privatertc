import React from 'react';
import PropTypes from 'prop-types';

const CarouselIndicator = ({id, position, isActive}) => (
    <li data-target={`#${id}`} data-slide-to={position} className={isActive ? 'active' : ''} />
);

CarouselIndicator.propTypes = {
    id: PropTypes.string.isRequired,
    position: PropTypes.number.isRequired,
    isActive: PropTypes.bool,
};

CarouselIndicator.defaultProps = {
    isActive: false,
};

export default CarouselIndicator;
