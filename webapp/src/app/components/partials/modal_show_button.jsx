import React from 'react';
import PropTypes from 'prop-types';
import {openModal} from '../../utils/modals';
import Button from './button';

const ModalShowButton = ({children, onClick, modalId, ...properties}) => (
    <Button
        onClick={() => {
            onClick();
            openModal(modalId);
        }}
        {...properties}
    >
        {children}
    </Button>
);

ModalShowButton.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node.isRequired, PropTypes.string.isRequired]).isRequired,
    onClick: PropTypes.func,
    modalId: PropTypes.string.isRequired,
};

ModalShowButton.defaultProps = {
    onClick: () => {},
};

export default ModalShowButton;
