import React from 'react';
import {useSelector} from 'react-redux';
import {NAV_HEADER} from '../../constants';
import {useAuthCheck} from '../../../auth/utils/hooks';
import {useNavDropdownComponent, useNavSearchFunction} from '../../utils/nav';
import NavBackButton from './nav_back_button';
import NavHeaderComponents from './nav_header_components';
import NavLinks from './nav_links';
import NavSearch from './nav_search';
import NavDropdown from './nav_dropdown';
import NavDropdownDefaultContent from './nav_dropdown_default_content';

const Nav = () => {
    const appName = useSelector(state => state.app.config.name);
    const navTitle = useSelector(state => state.app.navTitle);
    const showBackButton = useSelector(state => state.app.showBackButton);

    const activeCallId = useSelector(state => state.calls.active.id);
    const isInCall = !!activeCallId;

    const NavDropdownComponent = useNavDropdownComponent() || NavDropdownDefaultContent;
    const searchFunction = useNavSearchFunction();

    const isAuthenticated = useAuthCheck();
    const showNavLinks = isAuthenticated && !showBackButton; // Show nav links on root module pages only.

    // min-width: 0; in both flex containers is needed for text-truncate to work.
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="d-flex justify-content-between w-100">
                    <div className="d-flex mnw-0">
                        <div className="navbar-brand d-flex mnw-0">
                            <div className="text-truncate">
                                {showBackButton && <NavBackButton />}
                                <span className="align-middle">{navTitle ?? appName}</span>
                            </div>
                            <NavHeaderComponents side={NAV_HEADER.LEFT} />
                        </div>
                        {showNavLinks && (
                            <ul className="d-none d-md-flex flex-row align-items-center navbar-nav">
                                <NavLinks />
                            </ul>
                        )}
                    </div>
                    <div className="d-flex align-items-center justify-content-end mxw-50">
                        <NavHeaderComponents side={NAV_HEADER.RIGHT} />
                        {searchFunction && <NavSearch searchActionCallback={searchFunction} />}
                        <NavDropdown content={<NavDropdownComponent />} showPhoneBadge={isInCall} />
                    </div>
                </div>
            </nav>
            {showNavLinks && (
                <ul className="d-md-none nav nav-tabs nav-fill bg-dark justify-content-center">
                    <NavLinks />
                </ul>
            )}
        </>
    );
};

export default Nav;
