import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

const RawNavLink = ({value, children, ...properties}) => {
    const finalValue = children ?? value ?? 'Link';

    return (
        <NavLink activeClassName="active" {...properties}>
            {finalValue}
        </NavLink>
    );
};

RawNavLink.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]), // eslint-disable-line react/require-default-props
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]), // eslint-disable-line react/require-default-props
};

export default RawNavLink;
