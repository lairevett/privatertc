import React from 'react';
import PropTypes from 'prop-types';
import ModalCloseButton from './modal_close_button';

const ModalHeader = ({children, title}) => (
    <div className="modal-header">
        <h5 className="modal-title w-50 text-truncate">{title}</h5>
        {children}
        <ModalCloseButton className="btn btn-link p-0 modal-button">
            <span className="material-icons">close</span>
        </ModalCloseButton>
    </div>
);

ModalHeader.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    title: PropTypes.string,
};

ModalHeader.defaultProps = {
    children: null,
    title: '',
};

export default ModalHeader;
