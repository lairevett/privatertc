import React from 'react';
import {Field, propTypes, reduxForm} from 'redux-form';
import {validate, submit} from '../../utils/forms/process_settings_update_password';
import {MODULE_NAME} from '../../constants';
import {Form, FormField} from './form';
import FormError from './form_error';
import SubmitButton from './submit_button';

const SettingsUpdatePasswordForm = ({handleSubmit, error, valid, pristine, submitting}) => (
    <div className="mb-4">
        <Form>
            {error && <FormError message={error} />}
            <form onSubmit={handleSubmit(submit)}>
                <Field
                    component={FormField}
                    id="currentPassword"
                    name="currentPassword"
                    type="password"
                    label="Current password"
                />
                <Field component={FormField} id="password" name="password" type="password" label="Password" />
                <Field
                    component={FormField}
                    id="confirmPassword"
                    name="confirmPassword"
                    type="password"
                    label="Confirm password"
                />
                <div className="text-center mt-3">
                    <SubmitButton disabled={!valid || pristine || submitting} value="Update" />
                </div>
            </form>
        </Form>
    </div>
);

SettingsUpdatePasswordForm.propTypes = propTypes;

export default reduxForm({form: `${MODULE_NAME}SettingsUpdatePassword`, validate})(SettingsUpdatePasswordForm);
