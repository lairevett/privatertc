import React from 'react';
import PropTypes from 'prop-types';
import RawNavLink from './raw_nav_link';

const NavLink = ({value, children, ...properties}) => {
    const finalValue = children ?? value ?? 'Link';

    return (
        <li className="nav-item">
            <RawNavLink className="nav-link" {...properties}>
                {finalValue}
            </RawNavLink>
        </li>
    );
};

NavLink.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]), // eslint-disable-line react/require-default-props
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]), // eslint-disable-line react/require-default-props
};

export default NavLink;
