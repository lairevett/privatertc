import React from 'react';
import {useSelector} from 'react-redux';
import NavDropdownSignOutForm from '../../../auth/components/partials/nav_dropdown_sign_out_form';
import {useCurrentUser} from '../../../auth/utils/hooks';
import {retrieveUrl, reverseUrl} from '../../utils/router';
import NavDropdownLink from './nav_dropdown_link';

const NavDropdownDefaultContent = () => {
    const {username} = useCurrentUser();
    const activeCallConversationId = useSelector(state => state.calls.active.conversationId);
    const roomName = useSelector(state => state.rooms.name);

    const {path: detailsCallConversationPath} = reverseUrl('conversations:details_call', {
        conversationId: activeCallConversationId,
    });

    const {path: createGroupPath, wrappedViewProperties: createGroupProperties} = retrieveUrl('groups:create');
    const conversationRoomPath = retrieveUrl('rooms:conversation').path;
    const {path: createRoomPath, wrappedViewProperties: createRoomProperties} = retrieveUrl('rooms:create');
    const {path: joinRoomPath, wrappedViewProperties: joinRoomProperties} = retrieveUrl('rooms:join');
    const {path: settingsPath, wrappedViewProperties: settingsProperties} = retrieveUrl('app:settings');

    return username ? (
        <>
            <span className="dropdown-header">{username}</span>
            <div className="dropdown-divider" />
            {activeCallConversationId && <NavDropdownLink to={detailsCallConversationPath} value="Back to call" />}
            <NavDropdownLink to={createGroupPath} value={createGroupProperties.title} />
            {roomName ? (
                <NavDropdownLink to={conversationRoomPath} value="Back to room" />
            ) : (
                <>
                    <NavDropdownLink to={createRoomPath} value={createRoomProperties.title} />
                    <NavDropdownLink to={joinRoomPath} value={joinRoomProperties.title} />
                </>
            )}
            <NavDropdownLink to={settingsPath} value={settingsProperties.title} />
            <div className="dropdown-divider" />
            <NavDropdownSignOutForm />
        </>
    ) : roomName ? (
        <NavDropdownLink to={conversationRoomPath} value="Back to room" />
    ) : (
        <NavDropdownLink to={joinRoomPath} value={joinRoomProperties.title} />
    );
};

export default NavDropdownDefaultContent;
