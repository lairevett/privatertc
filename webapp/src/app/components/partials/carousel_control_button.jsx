import React from 'react';
import PropTypes from 'prop-types';
import {CAROUSEL_CONTROL_TYPE} from '../../constants';

const CarouselControlButton = ({id, type}) => (
    <a className={`carousel-control-${type}`} role="button" href={`#${id}`} data-slide={type}>
        <span className={`carousel-control-${type}-icon`} aria-hidden="true" />
        <span className="sr-only">{type === CAROUSEL_CONTROL_TYPE.PREVIOUS ? 'Previous' : 'Next'}</span>
    </a>
);

CarouselControlButton.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.oneOf(Object.values(CAROUSEL_CONTROL_TYPE)).isRequired,
};

export default CarouselControlButton;
