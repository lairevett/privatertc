import React, {forwardRef} from 'react';
import PropTypes from 'prop-types';

const TableListCell = forwardRef(({children, appendClassName, ...properties}, reference) => (
    <span ref={reference} className={`table-list-cell ${appendClassName}`} {...properties}>
        {children}
    </span>
));

TableListCell.propTypes = {
    children: PropTypes.node.isRequired,
    appendClassName: PropTypes.string,
};

TableListCell.defaultProps = {
    appendClassName: '',
};

export default TableListCell;
