import React from 'react';
import PropTypes from 'prop-types';

const NoResultsText = ({value}) => <p className="text-center text-muted mt-3">{value}</p>;

NoResultsText.propTypes = {
    value: PropTypes.string,
};

NoResultsText.defaultProps = {
    value: 'No results',
};

export default NoResultsText;
