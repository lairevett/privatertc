import React from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import Button from './button';
import {debounce} from '../../utils/events';

const NavSearch = ({searchActionCallback}) => {
    const dispatch = useDispatch();
    const handleChange = debounce(async evnt => dispatch(await searchActionCallback(evnt.target.value)()), 500);

    return (
        <div className="bmd-form-group bmd-collapse-inline">
            <span id="collapse-search" className="collapse">
                <input className="form-control" type="text" id="search" onChange={handleChange} />
            </span>
            <Button
                colorClassName=""
                appendClassName="bmd-btn-icon text-light"
                htmlFor="search"
                data-toggle="collapse"
                data-target="#collapse-search"
                aria-expanded="false"
                aria-controls="collapse-search"
            >
                <span className="material-icons">search</span>
            </Button>
        </div>
    );
};

NavSearch.propTypes = {
    searchActionCallback: PropTypes.func.isRequired,
};

export default NavSearch;
