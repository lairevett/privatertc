import React from 'react';
import PropTypes from 'prop-types';
import ModalShowButton from './modal_show_button';

const NavDropdownModalShowButton = ({value, ...properties}) => (
    <ModalShowButton className="dropdown-item" style={{cursor: 'pointer'}} {...properties}>
        {value}
    </ModalShowButton>
);

NavDropdownModalShowButton.propTypes = {
    value: PropTypes.string.isRequired,
};

export default NavDropdownModalShowButton;
