import React from 'react';
import PropTypes from 'prop-types';
import Nav from './nav';

const Layout = ({children}) => (
    <div id="app" className="d-flex flex-column">
        <Nav />
        <main className="d-flex justify-content-center h-100">
            <div className="col-12 col-lg-10 p-0 h-100">{children}</div>
        </main>
    </div>
);

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
