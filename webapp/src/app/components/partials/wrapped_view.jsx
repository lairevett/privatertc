/* eslint-disable security/detect-object-injection */
import React, {useMemo} from 'react';
import queryString from 'query-string';
import {setNavTitle, setShowBackButton, setCurrentViewName} from '../../actions';
import {makeContext as makeAxiosContext} from '../../utils/api/requests';
import {useDispatchEvents, makeDispatchEvents} from '../../utils/dispatch_events';

export const buildView = (() => {
    const memo = {};

    return (viewName, accessChain) => {
        if (memo[viewName]) {
            return memo[viewName];
        }

        const builtView = accessChain.reduce(
            (Previous, Current) => {
                return properties => (
                    <Previous {...properties}>
                        <Current {...properties} />
                    </Previous>
                );
            },
            ({children}) => children
        );

        memo[viewName] = builtView;
        return builtView;
    };
})();

const WrappedView = properties => {
    const {
        match: {params: urlParameters},
        location: {search},
    } = properties;

    const queryParameters = useMemo(() => queryString.parse(search), [search]);

    const {
        name,
        title,
        hasDynamicTitle,
        showBackButton,
        callbackGetDispatchEvents,
        view: View,
        ...restOfProperties
    } = properties;

    const renderDispatchEvents = makeDispatchEvents(
        [() => setShowBackButton(showBackButton), () => setCurrentViewName(name)],
        [],
        [showBackButton, name]
    );

    if (!hasDynamicTitle) {
        renderDispatchEvents.onInit.push(() => setNavTitle(title ?? null));
        renderDispatchEvents.dependencies.push(title);
    }

    useDispatchEvents(renderDispatchEvents);
    useDispatchEvents(callbackGetDispatchEvents(urlParameters, queryParameters), makeAxiosContext());

    return <View urlParameters={urlParameters} queryParameters={queryParameters} {...restOfProperties} />;
};

export default React.memo(
    WrappedView,
    // WrappedView properties don't change unless the actual route (its name) changes.
    (previousProperties, nextProperties) => previousProperties.name === nextProperties.name
);
