import React from 'react';
import PropTypes from 'prop-types';

const ViewSection = ({title, children}) => (
    <section className="border-bottom">
        <h6 className="pl-3 pt-3 pr-3 mb-0 text-muted">{title}</h6>
        <div>{children}</div>
    </section>
);

ViewSection.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]).isRequired,
};

export default ViewSection;
