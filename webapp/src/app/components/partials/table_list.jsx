import React, {forwardRef} from 'react';
import PropTypes from 'prop-types';

const TableList = forwardRef(({appendClassName, children, ...properties}, reference) => (
    <ul ref={reference} className={`table-list ${appendClassName}`} {...properties}>
        {children}
    </ul>
));

TableList.propTypes = {
    appendClassName: PropTypes.string,
    children: PropTypes.node.isRequired,
};

TableList.defaultProps = {
    appendClassName: '',
};

export default TableList;
