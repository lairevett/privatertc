import React from 'react';
import PropTypes from 'prop-types';

const CarouselItem = ({child, isActive}) => <div className={`carousel-item ${isActive ? 'active' : ''}`}>{child}</div>;

CarouselItem.propTypes = {
    child: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]).isRequired,
    isActive: PropTypes.bool,
};

CarouselItem.defaultProps = {
    isActive: false,
};

export default CarouselItem;
