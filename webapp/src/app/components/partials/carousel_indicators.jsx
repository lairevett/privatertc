import React from 'react';
import PropTypes from 'prop-types';
import CarouselIndicator from './carousel_indicator';

const CarouselIndicators = ({id, childrenAndTheirKeys}) => (
    <ol className="carousel-indicators">
        {childrenAndTheirKeys.map(([key], position) => (
            <CarouselIndicator
                key={`carousel_indicator_${id}_${key}`}
                id={id}
                position={position}
                isActive={position === 0}
            />
        ))}
    </ol>
);

CarouselIndicators.propTypes = {
    id: PropTypes.string.isRequired,
    childrenAndTheirKeys: PropTypes.arrayOf(PropTypes.array).isRequired,
};

export default CarouselIndicators;
