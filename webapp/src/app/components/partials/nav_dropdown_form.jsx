import React from 'react';
import {reduxForm, propTypes} from 'redux-form';
import PropTypes from 'prop-types';
import NavDropdownSubmitButton from './nav_dropdown_submit_button';

const NavDropdownForm = ({form, processSubmit, handleSubmit, submitting, value}) => (
    <>
        <form id={form} className="d-none" onSubmit={handleSubmit(processSubmit)} />
        <NavDropdownSubmitButton form={form} value={value} disabled={submitting} />
    </>
);

NavDropdownForm.propTypes = {
    ...propTypes,
    value: PropTypes.string.isRequired,
};

// Form name assigned dynamically.
export default reduxForm()(NavDropdownForm);
