/* eslint-disable react/no-array-index-key, no-shadow */
import React, {useMemo} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import parse, {NodeType} from 'slack-message-parser';

// TODO: add rest of the node types.
const SlackMessageFormatter = ({node}) => {
    switch (node.type) {
        case NodeType.Root:
            return node.children.map((node, i) => <SlackMessageFormatter key={i} node={node} />);
        case NodeType.Text:
            return <span>{node.text}</span>;
        case NodeType.Bold:
            return (
                <span className="font-weight-bold">
                    {node.children.map((node, i) => (
                        <SlackMessageFormatter key={i} node={node} />
                    ))}
                </span>
            );
        case NodeType.Italic:
            return (
                <span className="font-italic">
                    {node.children.map((node, i) => (
                        <SlackMessageFormatter key={i} node={node} />
                    ))}
                </span>
            );
        case NodeType.Strike:
            return (
                <del>
                    {node.children.map((node, i) => (
                        <SlackMessageFormatter key={i} node={node} />
                    ))}
                </del>
            );
        case NodeType.URL:
            return (
                <Link to={{pathname: node.url}} target="_blank" rel="noopener noreferrer">
                    {node.label
                        ? node.label.map((child, i) => <SlackMessageFormatter key={i} node={child} />)
                        : node.url}
                </Link>
            );
        default:
            return null;
    }
};

SlackMessageFormatter.propTypes = {
    // TODO: add proptypes
    // eslint-disable-next-line react/forbid-prop-types
    node: PropTypes.any.isRequired,
};

const SlackMessage = ({content}) => {
    const result = useMemo(() => parse(content), [content]);
    return <SlackMessageFormatter node={result} />;
};

SlackMessage.propTypes = {
    content: PropTypes.string.isRequired,
};

export default SlackMessage;
