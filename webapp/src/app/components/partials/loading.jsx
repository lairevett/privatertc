import React from 'react';
import PropTypes from 'prop-types';
import Spinner from './spinner';

const Loading = ({isPlacedOnBottom}) => (
    <div className={`text-center m${isPlacedOnBottom ? 'b' : 't'}-3`}>
        <Spinner size="2.5rem" />
    </div>
);

Loading.propTypes = {
    isPlacedOnBottom: PropTypes.bool,
};

Loading.defaultProps = {
    isPlacedOnBottom: false,
};

export default Loading;
