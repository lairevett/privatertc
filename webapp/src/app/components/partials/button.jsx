import PropTypes from 'prop-types';
import React from 'react';
import {Link} from 'react-router-dom';

const Button = ({to, colorClassName, appendClassName, value, children, ...properties}) => {
    const className = `btn ${colorClassName} ${appendClassName}`;

    return to ? (
        <Link to={to} className={className} {...properties}>
            {value ?? children}
        </Link>
    ) : (
        <button type="button" className={className} {...properties}>
            {value ?? children}
        </button>
    );
};

Button.propTypes = {
    to: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.object.isRequired]),
    colorClassName: PropTypes.string,
    appendClassName: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
};

Button.defaultProps = {
    to: null,
    colorClassName: 'btn-primary',
    appendClassName: '',
    value: null,
    children: 'Button',
};

export default Button;
