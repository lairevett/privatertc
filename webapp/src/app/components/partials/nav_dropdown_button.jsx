import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';

const NavDropdownButton = ({value, ...properties}) => (
    <Button className="dropdown-item" style={{cursor: 'pointer'}} value={value} {...properties} />
);

NavDropdownButton.propTypes = {
    value: PropTypes.string.isRequired,
};

export default NavDropdownButton;
