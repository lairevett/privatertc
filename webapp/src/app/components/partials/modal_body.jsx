import React from 'react';
import PropTypes from 'prop-types';

const ModalBody = ({children}) => <div className="modal-body">{children}</div>;

ModalBody.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node.isRequired, PropTypes.string.isRequired]).isRequired,
};

export default ModalBody;
