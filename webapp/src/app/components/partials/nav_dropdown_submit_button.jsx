import PropTypes from 'prop-types';
import React from 'react';
import SubmitButton from './submit_button';

const NavDropdownSubmitButton = ({value, ...properties}) => (
    <SubmitButton className="dropdown-item" style={{cursor: 'pointer'}} value={value} {...properties} />
);

NavDropdownSubmitButton.propTypes = {
    value: PropTypes.string.isRequired,
};

export default NavDropdownSubmitButton;
