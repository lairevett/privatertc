import React, {forwardRef, useRef} from 'react';
import PropTypes from 'prop-types';
import {SCROLL_DIRECTION} from '../../utils/api/constants';
import {usePaginationActionDispatchOnScroll} from '../../utils/api/hooks';
import TableList from './table_list';
import Loading from './loading';
import TableListRow from './table_list_row';
import TableListCell from './table_list_cell';

const LoadingRow = properties => (
    <TableListRow appendClassName="justify-content-center">
        <TableListCell>
            <Loading {...properties} />
        </TableListCell>
    </TableListRow>
);

const PaginatedTableList = forwardRef(
    ({children, scrollDirection, nextPageLink, actionCallback, appendClassName, ...properties}, reference) => {
        const freshTableListReference = useRef();
        const tableListReference = reference ?? freshTableListReference;
        const hasNextPage = usePaginationActionDispatchOnScroll(
            tableListReference,
            scrollDirection,
            nextPageLink,
            actionCallback
        );

        return (
            <TableList ref={tableListReference} appendClassName={`paginated ${appendClassName}`} {...properties}>
                {hasNextPage && scrollDirection === SCROLL_DIRECTION.TOP && <LoadingRow />}
                {children}
                {hasNextPage && scrollDirection === SCROLL_DIRECTION.BOTTOM && <LoadingRow isPlacedOnBottom />}
            </TableList>
        );
    }
);

PaginatedTableList.propTypes = {
    children: PropTypes.node,
    scrollDirection: PropTypes.oneOf(Object.values(SCROLL_DIRECTION)).isRequired,
    nextPageLink: PropTypes.string,
    actionCallback: PropTypes.func.isRequired,
    appendClassName: PropTypes.string,
};

PaginatedTableList.defaultProps = {
    children: null,
    nextPageLink: null,
    appendClassName: '',
};

export default PaginatedTableList;
