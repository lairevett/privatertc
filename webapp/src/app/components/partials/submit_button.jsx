import PropTypes from 'prop-types';
import React from 'react';

const SubmitButton = ({colorClassName, appendClassName, value, children, ...properties}) => (
    <button type="submit" className={`btn ${colorClassName} ${appendClassName}`} {...properties}>
        {value ?? children}
    </button>
);

SubmitButton.propTypes = {
    colorClassName: PropTypes.string,
    appendClassName: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
};

SubmitButton.defaultProps = {
    colorClassName: 'btn-primary',
    appendClassName: '',
    value: null,
    children: 'Submit',
};

export default SubmitButton;
