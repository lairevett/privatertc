import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {NAV_HEADER} from '../../constants';
import {useNavHeaderComponents} from '../../utils/nav';

const NavHeaderComponents = ({side}) => {
    const navHeaderComponents = useNavHeaderComponents();

    return (
        navHeaderComponents?.[side]?.map(Component => (
            <Fragment key={Component.name}>
                <Component />
                <div className="mr-2" />
            </Fragment>
        )) ?? null
    );
};

NavHeaderComponents.propTypes = {
    side: PropTypes.oneOf(Object.values(NAV_HEADER)).isRequired,
};

export default NavHeaderComponents;
