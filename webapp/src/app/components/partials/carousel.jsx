import React from 'react';
import PropTypes from 'prop-types';
import {CAROUSEL_CONTROL_TYPE} from '../../constants';
import CarouselIndicators from './carousel_indicators';
import CarouselControlButton from './carousel_control_button';
import CarouselItem from './carousel_item';

const Carousel = ({children, id, cycleInterval, appendClassName, withIndicators, withControls}) => {
    const childrenAndTheirKeys = children.map(child => [child.key, child]);

    return (
        <div id={id} className={`carousel slide ${appendClassName}`} data-ride="carousel" data-interval={cycleInterval}>
            <div className="carousel-inner">
                {childrenAndTheirKeys.map(([key, child], position) => (
                    <CarouselItem key={`carousel_item_${id}_${key}`} id={id} child={child} isActive={position === 0} />
                ))}
            </div>
            {withIndicators && <CarouselIndicators id={id} childrenAndTheirKeys={childrenAndTheirKeys} />}
            {withControls && (
                <>
                    <CarouselControlButton id={id} type={CAROUSEL_CONTROL_TYPE.PREVIOUS} />
                    <CarouselControlButton id={id} type={CAROUSEL_CONTROL_TYPE.NEXT} />
                </>
            )}
        </div>
    );
};

Carousel.propTypes = {
    children: PropTypes.node.isRequired,
    id: PropTypes.string.isRequired,
    cycleInterval: PropTypes.number,
    appendClassName: PropTypes.string,
    withIndicators: PropTypes.bool,
    withControls: PropTypes.bool,
};

Carousel.defaultProps = {
    cycleInterval: 0,
    appendClassName: '',
    withIndicators: false,
    withControls: false,
};

export default Carousel;
