import React from 'react';
import {Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {useAuthCheck, useAdminCheck} from '../../../auth/utils/hooks';
import {STATUS as CALL_STATUS} from '../../../calls/constants';
import {retrieveUrl} from '../../utils/router';

const RedirectOrRender = ({redirectCondition, urlName, render}) =>
    redirectCondition ? <Redirect to={retrieveUrl(urlName).path} /> : render;

RedirectOrRender.propTypes = {
    redirectCondition: PropTypes.bool.isRequired,
    urlName: PropTypes.string,
    render: PropTypes.node.isRequired,
};

RedirectOrRender.defaultProps = {
    urlName: 'app:index',
};

// eslint-disable-next-line react/prop-types
export const RequireSignedOut = ({children}) => {
    const isAuthenticated = useAuthCheck();
    return <RedirectOrRender redirectCondition={isAuthenticated} render={children} />;
};

// eslint-disable-next-line react/prop-types
export const RequireSignedIn = ({children}) => {
    const isAuthenticated = useAuthCheck();
    return <RedirectOrRender redirectCondition={!isAuthenticated} render={children} />;
};

// eslint-disable-next-line react/prop-types
export const RequireAdmin = ({children}) => {
    const isAdmin = useAdminCheck();
    return (
        <RequireSignedIn>
            <RedirectOrRender redirectCondition={!isAdmin} render={children} />
        </RequireSignedIn>
    );
};

// eslint-disable-next-line react/prop-types
export const RequireOutOfRoom = ({children}) => {
    const roomName = useSelector(state => state.rooms.name);
    return <RedirectOrRender redirectCondition={!!roomName} urlName="rooms:conversation" render={children} />;
};

// eslint-disable-next-line react/prop-types
export const RequireInRoom = ({children}) => {
    const roomName = useSelector(state => state.rooms.name);
    return <RedirectOrRender redirectCondition={!roomName} urlName="rooms:join" render={children} />;
};

// eslint-disable-next-line react/prop-types
export const RequireInCall = ({children}) => {
    const activeCallStatus = useSelector(state => state.calls.active.status);
    return <RedirectOrRender redirectCondition={activeCallStatus === CALL_STATUS.DISCONNECTED} render={children} />;
};
