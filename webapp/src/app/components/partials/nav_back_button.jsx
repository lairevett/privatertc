import React from 'react';
import {history} from '../../utils/router';
import Button from './button';

const NavBackButton = () => (
    <Button colorClassName="" appendClassName="bmd-btn-icon" onClick={history.goBack}>
        <span className="material-icons">arrow_back</span>
    </Button>
);

export default NavBackButton;
