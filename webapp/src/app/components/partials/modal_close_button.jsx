import React from 'react';
import {closeModal} from '../../utils/modals';
import Button from './button';

const ModalCloseButton = ({...properties}) => <Button onClick={closeModal} {...properties} />;

export default ModalCloseButton;
