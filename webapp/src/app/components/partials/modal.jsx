import React from 'react';
import PropTypes from 'prop-types';
import {useClearModalIdFromHashIfNeeded} from '../../utils/modals';
import ModalHeader from './modal_header';

const Modal = ({id, title, children, isFullScreen, appendContentClassName, hideHeader}) => {
    useClearModalIdFromHashIfNeeded(id);

    return (
        <div
            className={`modal fade ${isFullScreen ? 'modal-fullscreen' : ''}`}
            id={id}
            tabIndex="-1"
            role="dialog"
            aria-labelledby={title}
            aria-hidden="true"
        >
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className={`modal-content ${appendContentClassName}`}>
                    {!hideHeader && <ModalHeader title={title} />}
                    {children}
                </div>
            </div>
        </div>
    );
};

Modal.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]).isRequired,
    isFullScreen: PropTypes.bool,
    appendContentClassName: PropTypes.string,
    hideHeader: PropTypes.bool,
};

Modal.defaultProps = {
    title: '',
    isFullScreen: false,
    appendContentClassName: '',
    hideHeader: false,
};

export default Modal;
