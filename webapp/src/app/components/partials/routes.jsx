import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {retrieveUrls} from '../../utils/router';
import WrappedView, {buildView} from './wrapped_view';

const Routes = () => {
    const urls = Object.values(retrieveUrls());

    return (
        <Switch>
            {urls.map(url => (
                <Route
                    key={url.wrappedViewProperties.name}
                    path={url.path}
                    exact={url.exact}
                    render={routeProperties => (
                        <WrappedView
                            {...routeProperties}
                            {...url.wrappedViewProperties}
                            view={buildView(url.wrappedViewProperties.name, url.accessChain)}
                        />
                    )}
                />
            ))}
            <Route render={() => <div>Error 404</div>} />
        </Switch>
    );
};

export default Routes;
