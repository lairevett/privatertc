import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {history} from '../../utils/router';

const NavDropdownLink = ({to, value, disabled, ...properties}) => (
    <Link
        to={to}
        className={`dropdown-item${disabled ? ' disabled' : ''}`}
        onClick={disabled ? evnt => evnt.preventDefault() : null}
        replace={to === history.location.pathname || to === '#'}
        {...properties}
    >
        {value}
    </Link>
);

NavDropdownLink.propTypes = {
    to: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.shape({pathname: PropTypes.string.isRequired}).isRequired,
    ]).isRequired,
    value: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
};

NavDropdownLink.defaultProps = {
    disabled: false,
};

export default NavDropdownLink;
