import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';

const NavDropdown = ({content, showPhoneBadge}) => (
    <div className="dropdown">
        <Button
            appendClassName="bmd-btn-icon dropdown-toggle text-light"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
        >
            <span className="material-icons">more_vert</span>
            {showPhoneBadge && <span className="material-icon-badge material-icons">phone</span>}
        </Button>
        <div className="dropdown-menu">{content}</div>
    </div>
);

NavDropdown.propTypes = {
    content: PropTypes.node.isRequired,
    showPhoneBadge: PropTypes.bool.isRequired,
};

export default NavDropdown;
