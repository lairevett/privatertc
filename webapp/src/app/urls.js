import '../auth/urls';
import '../calls/urls';
import '../conversations/urls';
import '../groups/urls';
import '../rooms/urls';
import '../users/urls';
import {registerUrl} from './utils/router';
import {RequireSignedIn} from './components/partials/route_access';
import Index from './components/index'; // eslint-disable-line unicorn/import-index
import Settings from './components/settings';

registerUrl('app:index', '/', [Index]);

registerUrl('app:settings', '/settings', [RequireSignedIn, Settings], {
    title: 'Settings',
    showBackButton: true,
});
