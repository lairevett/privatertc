export const DOMAIN = 'domain.com';
export const API_PROTOCOL = 'http';
export const API_PORT = '8000';
export const WS_PROTOCOL = 'ws';
