/* eslint-disable security/detect-object-injection */
import {useSelector} from 'react-redux';
import {NAV_HEADER} from '../constants';

const _navViewData = {};

const _makeEmptyHeaderComponents = () => ({
    [NAV_HEADER.LEFT]: [],
    [NAV_HEADER.RIGHT]: [],
});

const _emptyHeaderComponents = _makeEmptyHeaderComponents();

const _addView = viewName => {
    if (typeof _navViewData[viewName] === 'undefined') {
        _navViewData[viewName] = {
            headerComponents: _makeEmptyHeaderComponents(),
            searchFunction: null,
            dropdownComponent: null,
        };
    }
};

export const registerNavHeaderComponent = (viewName, component, side) => {
    _addView(viewName);
    _navViewData[viewName].headerComponents[side].push(component);
};

export const registerNavSearchFunction = (viewName, fn) => {
    _addView(viewName);
    _navViewData[viewName].searchFunction = fn;
};

export const registerNavDropdownComponent = (viewName, component) => {
    _addView(viewName);
    _navViewData[viewName].dropdownComponent = component;
};

export const useNavHeaderComponents = () => {
    const currentViewName = useSelector(state => state.app.currentViewName);
    const data = _navViewData[currentViewName];
    return typeof data === 'undefined' ? _emptyHeaderComponents : data.headerComponents;
};

export const useNavSearchFunction = () => {
    const currentViewName = useSelector(state => state.app.currentViewName);
    const data = _navViewData[currentViewName];
    return typeof data === 'undefined' ? null : data.searchFunction;
};

export const useNavDropdownComponent = () => {
    const currentViewName = useSelector(state => state.app.currentViewName);
    const data = _navViewData[currentViewName];
    return typeof data === 'undefined' ? null : data.dropdownComponent;
};
