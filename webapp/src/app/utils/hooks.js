import {useState, useEffect} from 'react';
import {SCROLL_DIRECTION} from './api/constants';
import {isScrolledToTop, isScrolledToBottom} from './dom';
import {debounce} from './events';

const executeWhen = {
    [SCROLL_DIRECTION.TOP]: isScrolledToTop,
    [SCROLL_DIRECTION.BOTTOM]: isScrolledToBottom,
};

export const useBlockingScrollEvent = (nodeReference, scrollDirection, callback) => {
    const [isBlocked, setIsBlocked] = useState(false);

    useEffect(() => {
        const {current} = nodeReference;

        const handleScrollReference = (() => {
            if (current) {
                const handleScroll = debounce(async () => {
                    const execute = (executeWhen?.[scrollDirection] ?? (() => {}))(current);

                    if (!isBlocked && execute) {
                        setIsBlocked(true);
                        await callback(current);
                        setIsBlocked(false);
                    }
                }, 250);

                current.addEventListener('scroll', handleScroll);

                return handleScroll;
            }

            return null;
        })();

        return () => {
            if (current && handleScrollReference) {
                current.removeEventListener('scroll', handleScrollReference);
            }
        };
    }, [isBlocked, nodeReference, scrollDirection, callback]);
};
