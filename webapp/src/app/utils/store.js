import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import wsMiddleware from '../../ws/middleware';
import wrtcMiddleware from '../../wrtc/middleware';
import makeReducers from '../reducers';

const middleware = [thunk, wsMiddleware, wrtcMiddleware];

const makeStore = initialState =>
    createStore(makeReducers(), initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default makeStore;
