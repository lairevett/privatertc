export const logError = (message, time = null) =>
    console.error(`${new Date(time || Date.now()).toLocaleString('pl-PL')} ${message}`);

export const logDispatchAPIErrors = error => {
    const time = Date.now();

    if (error.config) {
        if (error.response) {
            if (process.env.NODE_ENV === 'development') {
                logError(
                    `Request to ${error.config.method} ${error.config.url} received response with non-2xx status (${error.response.status}).`,
                    time
                );

                logError('-- Response contents --', time);
                try {
                    logError(JSON.stringify(error.response.data), time);
                } catch (error_) {
                    logError('JSON.stringify(error.response.data) failed.', time);
                }
            }
        } else if (error.request) {
            logError(`Request to ${error.config.method} ${error.config.url} didn't receive any response.`, time);
        } else {
            logError(`Unknown error occurred while sending request: ${error.message}`, time);
        }
    }

    logError('-- Stack trace --', time);
    console.trace(error);
};
