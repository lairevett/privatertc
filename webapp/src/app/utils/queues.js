/* eslint-disable security/detect-object-injection */

export const makeFunctionQueue = () => {
    const _queue = {};

    const schedule = (key, fn) => {
        if (!_queue[key]) {
            _queue[key] = {flushed: false, toExecute: []};
        }

        _queue[key].toExecute.push(fn);
    };

    const flush = key => {
        if (!_queue[key] || _queue[key].flushed) {
            return;
        }

        _queue[key].flushed = true;
        _queue[key].toExecute = _queue[key].toExecute.filter(fn => {
            fn();
            return false;
        });
        delete _queue[key];
    };

    return {schedule, flush};
};
