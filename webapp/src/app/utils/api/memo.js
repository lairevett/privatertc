/* eslint-disable security/detect-object-injection */

export const memoizedQuery = (() => {
    const memo = {};

    return async (dispatch, targetEntityId, queryKey, dispatchFunc = null) => {
        if (!memo[targetEntityId]) {
            memo[targetEntityId] = {};
        }

        if (!memo[targetEntityId][queryKey]) {
            const {
                payload: {__meta__, ...response},
            } = dispatch(await dispatchFunc());

            memo[targetEntityId][queryKey] = response;
        }

        return {
            deleteMemo: () => {
                delete memo[targetEntityId][queryKey];
            },
            ...memo[targetEntityId][queryKey],
        };
    };
})();
