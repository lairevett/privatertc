import {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {useBlockingScrollEvent} from '../hooks';
import {SCROLL_DIRECTION} from './constants';
import {getPage, makePageIterator} from './helpers';

export const usePaginationActionDispatchOnScroll = (nodeReference, scrollDirection, nextPageLink, actionCallback) => {
    const dispatch = useDispatch();
    const nextPage = getPage(nextPageLink);
    const hasNextPage = nextPageLink != null;

    let lastScrollHeight;
    const wrappedCallback = async current => {
        if (hasNextPage) {
            lastScrollHeight = current.scrollHeight;
            dispatch(await actionCallback(nextPage)());
            if (scrollDirection === SCROLL_DIRECTION.TOP) {
                // Fix weird behavior when prepending elements to container while scrolling quickly
                // (scrolls container all the way up after prepend).
                current.scrollTo(0, current.scrollHeight - lastScrollHeight);
            }
        }
    };

    useBlockingScrollEvent(nodeReference, scrollDirection, wrappedCallback);
    return hasNextPage;
};

export const usePageIterator = nextPageLink => {
    const [pageIterator, setPageIterator] = useState(null);

    useEffect(() => {
        if (pageIterator == null && nextPageLink != null) {
            setPageIterator(makePageIterator(getPage(nextPageLink)));
        }
    }, [pageIterator, nextPageLink]);

    return pageIterator;
};
