import * as secrets from '../../secrets';

// API address.
const DEFAULT_API_URL = `${secrets.API_PROTOCOL}://api.${secrets.DOMAIN}:${secrets.API_PORT}`;
export const API_URL = localStorage.getItem('API_URL') || DEFAULT_API_URL;

// Allowed reducer meta status values.
export const STATUS = {
    INITIALIZED: 'STATUS_INITIALIZED',
    RESPONSE_BAD_REQUEST: 'STATUS_RESPONSE_BAD_REQUEST',
    RESPONSE_FORBIDDEN: 'STATUS_RESPONSE_FORBIDDEN',
    RESPONSE_NOT_FOUND: 'STATUS_RESPONSE_NOT_FOUND',
    RESPONSE_CONFLICT: 'STATUS_RESPONSE_CONFLICT',
    RESPONSE_INTERNAL_SERVER_ERROR: 'STATUS_RESPONSE_INTERNAL_SERVER_ERROR',
    RESPONSE_OTHER: 'STATUS_RESPONSE_OTHER',
    API_CONNECTION_ERROR: 'STATUS_API_CONNECTION_ERROR',
    UNKNOWN_ERROR: 'STATUS_UNKNOWN_ERROR',
    OK: 'STATUS_OK',
    OK_NO_RESULTS: 'STATUS_OK_NO_RESULTS',
};

export const ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS = new Map([
    [400, STATUS.RESPONSE_BAD_REQUEST],
    [401, STATUS.RESPONSE_FORBIDDEN],
    [404, STATUS.RESPONSE_NOT_FOUND],
    [409, STATUS.RESPONSE_CONFLICT],
    [500, STATUS.RESPONSE_INTERNAL_SERVER_ERROR],
]);

export const SCROLL_DIRECTION = {
    TOP: 'SCROLL_DIRECTION_TOP',
    BOTTOM: 'SCROLL_DIRECTION_BOTTOM',
};

// Common reducer initial states.
export const DEFAULT_META_INITIAL_STATE = {
    __meta__: {
        status: STATUS.INITIALIZED,
    },
};

export const DEFAULT_LIST_INITIAL_STATE = {
    count: 0,
    next: null,
    previous: null,
    results: [],
    ...DEFAULT_META_INITIAL_STATE,
};

// Common prop types.
export const META_PROP_TYPE = PropertyTypes => PropertyTypes.exact({status: PropertyTypes.string.isRequired});
