import {STATUS} from './constants';

// Could as well be STATUS_OK_NO_RESULTS, so slice to the STATUS_OK length before comparing.
export const combineStatuses = statuses =>
    statuses.every(status => status.slice(0, STATUS.OK.length) === STATUS.OK) ? STATUS.OK : STATUS.INITIALIZED;

export const getPage = pageLink => pageLink?.match(/[?&]page=(\d+)/)[1];

export const makePageIterator = currentNextPage => {
    let iterate = false;
    let isFetching = false;
    let requestInterval = null;
    let requestIntervalTimeout = null;
    let nextPage = currentNextPage;

    const stop = () => {
        const changed = iterate;
        iterate = false;
        isFetching = false;
        return changed;
    };

    const start = (actionCallback, onResponse, time) => {
        if (requestInterval != null) {
            clearTimeout(requestIntervalTimeout);
            requestIntervalTimeout = setTimeout(() => start(actionCallback, onResponse, time), time);
            return;
        }

        iterate = true;

        return new Promise((resolve, reject) => {
            requestInterval = setInterval(async () => {
                if (isFetching) {
                    // Account for slow connections (interval time > response time).
                    return;
                }

                if (!nextPage || !iterate) {
                    stop();
                    resolve();
                    clearInterval(requestInterval);
                    requestInterval = null;
                    return;
                }

                isFetching = true;
                const response = await actionCallback(nextPage);
                onResponse(response);
                nextPage = getPage(response?.payload?.next);
                isFetching = false;
            }, time);
        });
    };

    return {start, stop};
};
