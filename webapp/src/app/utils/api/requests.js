import axios from 'axios';
import {APP_CSRF_COOKIE_NAME, APP_CSRF_HEADER_NAME, ACTION} from '../../constants';
import {convertToFormData} from '../form';
import {logDispatchAPIErrors} from '../logger';
import {API_URL, ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS, STATUS} from './constants';

const apiCall = axios.create({baseURL: API_URL, headers: {Accept: 'application/json'}});

const makeResponse = (action, state, status, context) => ({
    type: action,
    payload: {
        ...state,
        __meta__: {
            status,
        },
    },
    context,
});

const makeErrorResponses = (action, initialState, context, error) => {
    if (axios.isCancel(error)) {
        return {type: ACTION.CANCEL_REQUEST};
    }

    if (error?.response) {
        const state =
            typeof error.response.data === 'object' && error.response.data !== null
                ? {...initialState, ...error.response.data}
                : initialState;

        return makeResponse(
            action,
            state,
            ERROR_STATUS_CODES_MAPPED_TO_STATUS_STRINGS.get(error.response.status) ?? STATUS.RESPONSE_OTHER,
            context
        );
    }

    if (error?.request) {
        return makeResponse(action, initialState, STATUS.API_CONNECTION_ERROR, context);
    }

    return makeResponse(action, initialState, STATUS.UNKNOWN_ERROR, context);
};

export const makeContext = () => {
    let cancel;
    const cancelToken = new axios.CancelToken(c => {
        cancel = c;
    });

    console.assert(typeof cancel !== 'undefined', 'Cancel callback is undefined.');
    return {axios: {cancelToken, cancel}};
};

export const apiGetRequest = (action, initialState, url) => async (context = null) => {
    try {
        const response = await apiCall({
            method: 'get',
            url,
            withCredentials: true,
            cancelToken: context?.axios?.cancelToken,
        });

        return makeResponse(action, response.data, STATUS.OK, context);
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiBatchGetRequests = (action, singleInitialState, multipleInitialState, urls) => async (
    context = null
) => {
    try {
        const responses = urls.map(url => apiGetRequest(action, singleInitialState, url, context, true));
        const payload = (await Promise.all(responses)).map(response => response.payload);

        return makeResponse(action, {results: payload}, STATUS.OK, context);
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, multipleInitialState, context, error);
    }
};

export const apiGetListRequest = (action, initialState, url) => async (context = null) => {
    try {
        const response = await apiCall({
            method: 'get',
            url,
            withCredentials: true,
            cancelToken: context?.axios?.cancelToken,
        });

        return (typeof response.data.count === 'undefined' && response.data?.results?.length > 0) ||
            response.data.count > 0
            ? makeResponse(action, response.data, STATUS.OK, context)
            : makeResponse(action, initialState, STATUS.OK_NO_RESULTS, context);
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

const apiRequestWithBody = (method, action, initialState, url, requestBody) => async (context = null) => {
    try {
        const isMultiPart = (context?.multiPartFields ?? []).some(field => field);

        const [contentType, data] = isMultiPart
            ? ['multipart/form-data', convertToFormData(requestBody)]
            : ['application/json', requestBody];

        const response = await apiCall({
            method,
            url,
            headers: {'Content-Type': contentType},
            data,
            xsrfHeaderName: APP_CSRF_HEADER_NAME,
            xsrfCookieName: APP_CSRF_COOKIE_NAME,
            withCredentials: true,
            cancelToken: context?.axios?.cancelToken,
        });

        return makeResponse(action, response.data, STATUS.OK, context);
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponses(action, initialState, context, error);
    }
};

export const apiPostRequest = (action, initialState, resource, requestBody) =>
    apiRequestWithBody('post', action, initialState, resource, requestBody);

export const apiPatchRequest = (action, initialState, resource, requestBody) =>
    apiRequestWithBody('patch', action, initialState, resource, requestBody);

export const apiDeleteRequest = (action, initialState, resource, requestBody) =>
    apiRequestWithBody('delete', action, initialState, resource, requestBody);
