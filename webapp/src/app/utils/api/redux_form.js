/* eslint-disable security/detect-object-injection */
import {SubmissionError} from 'redux-form';
import {STATUS} from './constants';

export const checkSubmitErrors = (payload, fieldNameOverrides = {}) => {
    if (payload.errors) {
        const errors = {...payload.errors};
        Object.entries(fieldNameOverrides).forEach(([apiName, formName]) => {
            errors[formName] = errors[apiName];
            delete errors[apiName];
        });

        throw new SubmissionError(errors);
    }

    if (payload.__meta__.status !== STATUS.OK) {
        const error = payload?.detail ?? 'Something went wrong';
        throw new SubmissionError({_error: error});
    }
};
