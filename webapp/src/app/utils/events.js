export const debounce = (callback, time) => {
    let timeout = null;

    return reactSyntheticEvent => {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }

        // SyntheticEvent object will be reused and all properties
        // will be nullified after the event callback has been invoked.
        const evnt = {...reactSyntheticEvent};

        timeout = setTimeout(() => callback(evnt), time);
    };
};

export const throttle = (callback, time) => {
    let canExecute = true;

    return reactSyntheticEvent => {
        if (canExecute) {
            const evnt = {...reactSyntheticEvent};
            callback(evnt);

            canExecute = false;
            setTimeout(() => {
                canExecute = true;
            }, time);
        }
    };
};
