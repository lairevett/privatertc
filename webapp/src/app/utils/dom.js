/* eslint-disable no-param-reassign */

export const scrollToBottom = node => node.scroll(0, node.scrollHeight);

export const isScrolledToTop = node => node.scrollTop <= 25 + (node?.firstChild?.clientHeight || 0);

export const isScrolledToBottom = node =>
    node.scrollTop > node.scrollHeight - node.offsetHeight - 25 - (node?.lastChild?.clientHeight || 0);

export const isOverflown = node => node.scrollWidth > node.clientWidth || node.scrollHeight > node.clientHeight;

const _makeAudioNodeId = targetUserWsChannelName => ['AUDIO', targetUserWsChannelName].join(';');
export const getAudioNodes = () => document.getElementById('audio-nodes');
export const getTargetUserWsChannelNameFromNode = node => node.id.split(';')[1];

export const addAudioNode = targetUserWsChannelName => {
    const audioNode = document.createElement('AUDIO');
    audioNode.id = _makeAudioNodeId(targetUserWsChannelName);
    audioNode.autoplay = true;
    getAudioNodes().append(audioNode);
};

export const attachAudioNodeSource = (targetUserWsChannelName, source) => {
    const id = _makeAudioNodeId(targetUserWsChannelName);
    const audioNode = document.getElementById(id);
    audioNode.srcObject = source;
};

export const removeAudioNode = targetUserWsChannelName => {
    const id = _makeAudioNodeId(targetUserWsChannelName);
    const audioNode = document.getElementById(id);

    if (audioNode == null) {
        console.info(`Audio node "${id}" does not exist.`);
        return;
    }

    audioNode.remove();
};

export const removeAudioNodes = () => {
    getAudioNodes().innerHTML = '';
};

export const setUpVideoNode = (
    node,
    videoNodesContainerSize,
    videoNodeWidthDenominator,
    videoNodeHeightDenominator,
    stream = null
) => {
    if (node) {
        const [width, height] = videoNodesContainerSize;

        if (stream) {
            node.srcObject = stream;
        }

        node.style.width = `${width / videoNodeWidthDenominator}px`;
        node.style.height = `${height / videoNodeHeightDenominator}px`;
    }
};
