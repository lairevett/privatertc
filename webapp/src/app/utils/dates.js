export const isOnSameDay = (firstDate, secondDate) =>
    firstDate.getFullYear() === secondDate.getFullYear() &&
    firstDate.getMonth() === secondDate.getMonth() &&
    firstDate.getDate() === secondDate.getDate();

export const isOnSameDayAsToday = date => isOnSameDay(date, new Date());

const getSortedUnits = milliseconds => {
    const seconds = Math.floor(milliseconds / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);

    return {seconds, minutes, hours};
};

export const convertMillisecondsToElapsedTime = milliseconds => {
    const {seconds, minutes, hours} = getSortedUnits(milliseconds);

    const paddedSeconds = `${seconds % 60}`.padStart(2, '0');
    const paddedMinutes = `${minutes % 60}`.padStart(2, '0');
    const output = `${paddedMinutes}:${paddedSeconds}`;

    if (hours >= 1) {
        const paddedHours = `${hours}`.padStart(2, '0');
        return `${paddedHours}:${output}`;
    }

    return output;
};

export const convertMillisecondsToElapsedTimeUnits = milliseconds => {
    const {seconds, minutes, hours} = getSortedUnits(milliseconds);
    const output = `${minutes}m ${seconds}s`;

    if (hours >= 1) {
        return `${hours}h ${output}`;
    }

    return output;
};
