import {useDispatch} from 'react-redux';
import {useEffect} from 'react';

export const makeDispatchEvents = (onInit = [], onDestroy = [], dependencies = []) => ({
    onInit,
    onDestroy,
    dependencies,
});

export const useDispatchEvents = (dispatchEvents, context = null) => {
    const dispatch = useDispatch();
    const {onInit, onDestroy} = dispatchEvents;

    useEffect(() => {
        const unwrappedOnInitEvents = onInit.map(onInitEvent => onInitEvent(context));

        // noinspection JSValidateTypes
        Promise.allSettled(unwrappedOnInitEvents)
            .then(promises => promises.forEach(({value}) => dispatch(value)))
            .catch(console.error);

        return () => {
            if (context?.axios) {
                context.axios.cancel();
            }

            const unwrappedOnDestroyEvents = onDestroy.map(onDestroyEvent => onDestroyEvent(context));
            // noinspection JSValidateTypes
            Promise.allSettled(unwrappedOnDestroyEvents)
                .then(promises => promises.forEach(({value}) => dispatch(value)))
                .catch(console.error);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, ...dispatchEvents.dependencies]);
};
