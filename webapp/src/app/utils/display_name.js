export const getDisplayName = entity => {
    if (entity) {
        const {broadcasted_name, id} = entity;
        return broadcasted_name || id;
    }

    return '';
};

export const getMessageSender = (senderId, currentUser, {conversation, room}) => {
    console.assert(!(conversation && room), 'Ambiguous arguments provided to the function.', {conversation, room});
    console.assert(conversation || room, 'Conversation or room to search for user display name were not provided.');

    if (senderId === currentUser.id) {
        return currentUser;
    }

    if (conversation) {
        const toUser = conversation?.to_user;
        if (toUser) {
            return toUser;
        }

        const toGroup = conversation?.to_group;
        if (toGroup) {
            const member = toGroup.members.find(({id}) => senderId === id);
            return member;
        }
    }

    if (room) {
        // TODO:
        return null;
    }

    return null;
};
