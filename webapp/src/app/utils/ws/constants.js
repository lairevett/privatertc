import * as secrets from '../../secrets';
import {API_URL} from '../api/constants';

export const WS_URL = `${secrets.WS_PROTOCOL}${API_URL.replace(/http[s]?/, '')}/ws/`;
