import {SubmissionError} from 'redux-form';

export const checkSubmitErrors = async callback => {
    let response = null;

    try {
        response = await callback();
    } catch (error) {
        throw new SubmissionError({_error: error.message});
    }

    if (typeof response === 'undefined' || response == null) {
        throw new SubmissionError({_error: 'Something went wrong.'});
    } else if (typeof response.is_success === 'boolean' && !response.is_success) {
        throw new SubmissionError({_error: response.reason});
    }

    return response;
};
