import allSettled from 'promise.allsettled';

Promise.allSettled = Promise.allSettled || allSettled;
