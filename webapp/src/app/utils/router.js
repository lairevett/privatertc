import {createBrowserHistory} from 'history';
import {makeDispatchEvents} from './dispatch_events';

export const history = createBrowserHistory();

const _urls = {};

export const makeUrl = (name, path, accessChain, optionalProperties = {}) => {
    const {
        exact,
        title,
        showBackButton,
        hasDynamicTitle,
        callbackGetDispatchEvents,
        ...restOfOptionalProperties
    } = optionalProperties;

    return {
        path,
        exact: exact ?? true,
        accessChain,
        wrappedViewProperties: {
            name,
            title: hasDynamicTitle ? '' : title,
            showBackButton: showBackButton ?? false,
            hasDynamicTitle: hasDynamicTitle ?? false,
            callbackGetDispatchEvents:
                typeof callbackGetDispatchEvents === 'function'
                    ? callbackGetDispatchEvents(restOfOptionalProperties)
                    : () => makeDispatchEvents(),
            ...restOfOptionalProperties,
        },
    };
};

export const registerUrl = (name, path, accessChain, optionalProperties = {}) => {
    if (_urls?.[name] === undefined) {
        // eslint-disable-next-line security/detect-object-injection
        _urls[name] = makeUrl(name, path, accessChain, optionalProperties);
    } else if (process?.env?.NODE_ENV === 'development') {
        console.warn(`Route ${name} at path ${path} is already registered.`);
    }
};

export const retrieveUrl = name => {
    if (_urls?.[name] === undefined) {
        throw new Error(`URL with name ${name} wasn't registered.`);
    }

    // eslint-disable-next-line security/detect-object-injection
    return _urls[name];
};

export const reverseUrl = (name, _arguments) => {
    const url = retrieveUrl(name);
    let {path} = url;

    if (!(typeof _arguments === 'object' && _arguments !== null)) {
        throw new Error('Arguments have to be instance of an object.');
    }

    Object.entries(_arguments).forEach(([key, value]) => {
        const argumentName = `:${key}`;

        if (!path.includes(argumentName)) {
            console.warn(`Argument ${key} was not used in path ${name} (${path}).`);
        }

        // eslint-disable-next-line security/detect-non-literal-regexp
        const regex = new RegExp(argumentName, 'g');
        path = path.replace(regex, value);
    });

    return {...url, path};
};

export const retrieveUrls = () => _urls;
