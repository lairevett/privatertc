import {initialize} from 'redux-form';
import {updateBroadcastedName as authUpdateBroadcastedName} from '../../../auth/actions';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../api/redux_form';
import {validateBroadcastedName} from '../validation';

export const validate = ({broadcastedName}) => {
    const errors = {};
    errors.broadcastedName = validateBroadcastedName(broadcastedName);
    return errors;
};

export const submit = async ({broadcastedName}, dispatch) => {
    const {payload} = dispatch(await authUpdateBroadcastedName(broadcastedName)({}));
    apiCheckSubmitErrors(payload, {broadcasted_name: 'broadcastedName'});
    dispatch(initialize('appSettingsUpdateBroadcastedName', {broadcastedName: payload.broadcasted_name}));
    // TODO: display some toast
};
