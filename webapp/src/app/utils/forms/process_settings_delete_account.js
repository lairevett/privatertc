import {submit as submitAuthSignOut} from '../../../auth/utils/forms/process_sign_out';
import {deleteAccount as authDeleteAccount} from '../../../auth/actions';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../api/redux_form';
import {validatePassword} from '../validation';

export const validate = ({password}) => {
    const errors = {};
    errors.password = validatePassword(password);
    return errors;
};

export const submit = async ({password}, dispatch) => {
    const {payload} = dispatch(await authDeleteAccount(password)({}));
    apiCheckSubmitErrors(payload);
    await submitAuthSignOut(null, dispatch);
};
