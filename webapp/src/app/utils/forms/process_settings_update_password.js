import {submit as submitAuthSignOut} from '../../../auth/utils/forms/process_sign_out';
import {updatePassword as authUpdatePassword} from '../../../auth/actions';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../api/redux_form';
import {validateFieldEquals, validateFieldNotEquals, validatePassword} from '../validation';

export const validate = ({currentPassword, password, confirmPassword}) => {
    const errors = {};
    errors.currentPassword = validatePassword(currentPassword);
    errors.password =
        validatePassword(password) ||
        validateFieldNotEquals(password, currentPassword, "New password can't be the same as the current password.") ||
        validateFieldEquals(password, confirmPassword, "Passwords don't match.");
    errors.confirmPassword = validatePassword(confirmPassword);
    return errors;
};

export const submit = async ({currentPassword, password}, dispatch) => {
    const {payload} = dispatch(await authUpdatePassword(currentPassword, password)({}));
    apiCheckSubmitErrors(payload, {current_password: 'currentPassword'});
    await submitAuthSignOut(null, dispatch);
};
