export const URL_IMAGE_REGEXP_OBJECT = /(\.apng)|(\.avif)|(\.gif)|(\.jpg)|(\.jpeg)|(\.jfef)|(\.jfif)|(\.pjpeg)|(\.pjp)|(\.png)|(\.svg)|(\.webp)/i;
export const URL_VIDEO_REGEXP_OBJECT = /(\.mp4)|(\.webm)/i;
export const URL_AUDIO_REGEXP_OBJECT = /(\.mp3)|(\.wav)/i;
export const CATCH_ALL_REGEXP_OBJECT = /.*/;

const ESCAPE_REGEXP_OBJECT = /[-/\\^$*+?.()|[\]{}]/g;

export const escapeRegExp = string => string.replace(ESCAPE_REGEXP_OBJECT, '\\$&');

export const makeKeywordSearchRegExp = keyword => {
    const escapedKeyword = escapeRegExp(keyword);
    // eslint-disable-next-line security/detect-non-literal-regexp
    return new RegExp(
        `(\\S+${escapedKeyword}\\S+)|(\\S+${escapedKeyword})|(${escapedKeyword}\\S+)|(${escapedKeyword})`,
        'gi'
    );
};

export const makeKeywordReplaceAllFunction = (keyword, replacement) => {
    const regexp = makeKeywordSearchRegExp(keyword);
    return string => string.replace(regexp, replacement);
};
