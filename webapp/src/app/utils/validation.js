export const validateFieldRequired = (field, message = 'Field is required.') => {
    if (
        typeof field === 'undefined' ||
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field === 'boolean' && !field)
    ) {
        return message;
    }

    return null;
};

export const validateFieldContainsNoWhitespace = (field, message = 'Field cannot contain any spaces.') => {
    if (typeof field !== 'undefined' && / /g.test(field)) {
        return message;
    }

    return null;
};

export const validateFieldMinLength = (field, min, customMessage = null) => {
    if (typeof field !== 'undefined' && field.length < min) {
        if (customMessage) return customMessage;
        return `Field must contain min ${min} letters.`;
    }
    return null;
};

export const validateFieldMaxLength = (field, max, customMessage = null) => {
    if (typeof field !== 'undefined' && field.length > max) {
        if (customMessage) return customMessage;
        return `Field can contain max ${max} letters.`;
    }
    return null;
};

export const validateFieldMinValue = (field, min, customMessage = null) => {
    if (typeof field !== 'undefined' && +field < min) {
        if (customMessage) return customMessage;
        return `Min value for field is ${min}.`;
    }
    return null;
};

export const validateFieldMaxValue = (field, max, customMessage = null) => {
    if (typeof field !== 'undefined' && +field > max) {
        if (customMessage) return customMessage;
        return `Max value for field is ${max}.`;
    }
    return null;
};

export const validateFieldEquals = (field, value, message = 'Field values are not equal.') => {
    if (typeof field !== 'undefined' && field !== value) {
        return message;
    }

    return null;
};

export const validateFieldNotEquals = (field, value, message = 'Field values are equal.') => {
    if (validateFieldEquals(field, value, message) == null) {
        return message;
    }

    return null;
};

export const validateUUID = (field, message = 'Field is not a valid UUID.') => {
    if (!/^[0-9a-f]{32}$/i.test(field)) {
        return message;
    }

    return null;
};

export const validateBroadcastedName = broadcastedName => validateFieldMaxLength(broadcastedName, 36);

export const validateUsername = username =>
    validateFieldRequired(username) ||
    validateFieldContainsNoWhitespace(username) ||
    validateFieldMinLength(username, 3) ||
    validateFieldMaxLength(username, 24);

export const validatePassword = password =>
    validateFieldRequired(password) ||
    validateFieldContainsNoWhitespace(password) ||
    validateFieldMinLength(password, 8) ||
    validateFieldMaxLength(password, 128);

export const validateRoomName = roomName =>
    validateFieldRequired(roomName) ||
    validateFieldContainsNoWhitespace(roomName) ||
    validateFieldMinLength(roomName, 3) ||
    validateFieldMaxLength(roomName, 20);

export const validatePin = pin =>
    validateFieldRequired(pin) ||
    validateFieldContainsNoWhitespace(pin) ||
    validateFieldMinLength(pin, 4) ||
    validateFieldMaxLength(pin, 20);
