const getFileObjectProperties = file => {
    // File objects don't have any enumerable properties,
    // so JSON.parse to redux store returns empty object.
    // It's readable as expected in process_form submit though.
    const {lastModified, name, size, type} = file;
    return {instance: file, lastModified, name, size, type};
};

// Maps form file metadata to redux store.
export const handleFileChange = handler => ({target: {files}}) =>
    handler(Array.prototype.map.call(files, getFileObjectProperties));

export const getFileExtension = fileName => {
    const dotIndex = fileName.lastIndexOf('.');
    return dotIndex > -1 ? fileName.slice(dotIndex + 1, fileName.length).toLowerCase() : '';
};

export const convertToFormData = (() => {
    // In case of unnecessary array index specification.
    // The indices specify relationships between one or more nested array-in-array-...
    // E.g. files[0][] as val1, files[0][] as val2, files[0][] as val3 -> {files: [[val1, val2, val3], ...]}
    const lastArrayRegex = /(\[\d+\])$/;

    return (data, formData = new FormData(), keyAccumulator = '') => {
        if (data?.constructor === Object) {
            // eslint-disable-next-line no-param-reassign
            keyAccumulator = keyAccumulator.replace(lastArrayRegex, '[]');

            Object.entries(data).forEach(([key, value]) => {
                const isRoot = !keyAccumulator;
                return convertToFormData(value, formData, isRoot ? key : `${keyAccumulator}[${key}]`);
            });
        } else if (Array.isArray(data)) {
            data.forEach((item, index) => convertToFormData(item, formData, `${keyAccumulator}[${index}]`));
        } else {
            const finalData = data instanceof Date ? data.toJSON() : data;
            // eslint-disable-next-line no-param-reassign
            keyAccumulator = keyAccumulator.replace(lastArrayRegex, '[]');
            formData.append(keyAccumulator, finalData);
        }

        return formData;
    };
})();
