import {useRef, useEffect} from 'react';
import {history} from './router';

const isModalIdInHash = () => history.location.hash.startsWith('#modal-');
const makeModalIdHash = id => `modal-${id}`;
const getModalIdFromHash = () => history.location.hash.split('-')[1];

export const openModal = id => {
    const modalIdHash = makeModalIdHash(id);
    history.push({hash: modalIdHash});
};

export const closeModal = history.goBack;

export const useLocationHashModals = () => {
    const openedModal = useRef(null);

    useEffect(() => {
        return history.listen((_, action) => {
            switch (action) {
                case 'PUSH':
                    if (isModalIdInHash()) {
                        const modalId = getModalIdFromHash();
                        const modal = window.$(`#${modalId}`);
                        openedModal.current = modal;
                        openedModal.current.modal('show');
                    }
                    break;
                case 'POP':
                    if (openedModal.current) {
                        const modalBackdrop = document.querySelector('.modal-backdrop');
                        if (modalBackdrop) {
                            modalBackdrop.remove();
                        }

                        openedModal.current.modal('hide');
                        openedModal.current = null;
                    }
                    break;
                default:
            }
        });
    }, []);
};

export const useClearModalIdFromHashIfNeeded = id => {
    useEffect(() => {
        const modal = window.$(`#${id}`);
        modal.on('hidden.bs.modal', () => {
            if (isModalIdInHash()) {
                closeModal();
            }
        });

        return () => {
            modal.off('hidden.bs.modal');
        };
    }, [id]);
};
