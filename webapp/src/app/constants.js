import {DEFAULT_META_INITIAL_STATE} from './utils/api/constants';

export const DEFAULT_APP_NAME = 'PrivateRTC';

export const APP_IS_MOBILE =
    window.orientation || !/Mobi|Android|webOS|iPhone|iPad|iPod|BlackBerry|Opera Mini/i.test(navigator.userAgent);

export const APP_CSRF_COOKIE_NAME = '_ct';
export const APP_CSRF_HEADER_NAME = 'X-CSRFToken';
export const APP_SESSION_COOKIE_NAME = '_sid';

export const MODULE_NAME = 'app';

export const ENDPOINT = {
    CONFIG: `/v1/${MODULE_NAME}/`,
};

export const ACTION = {
    CANCEL_REQUEST: `@${MODULE_NAME}/CANCEL_REQUEST`,
    RETRIEVE_CONFIG: `@${MODULE_NAME}/RETRIEVE_CONFIG`,
    SET_NAV_TITLE: `@${MODULE_NAME}/SET_NAV_TITLE`,
    SET_SHOW_BACK_BUTTON: `@${MODULE_NAME}/SET_SHOW_BACK_BUTTON`,
    SET_CURRENT_VIEW_NAME: `@${MODULE_NAME}/SET_CURRENT_VIEW_NAME`,
};

export const NAV_HEADER = {
    LEFT: 'NAV_HEADER_LEFT',
    RIGHT: 'NAV_HEADER_RIGHT',
};

export const CONFIG_INITIAL_STATE = {
    name: DEFAULT_APP_NAME,
    maintenance_mode: false,
    ...DEFAULT_META_INITIAL_STATE,
};

export const INITIAL_STATE = {
    config: CONFIG_INITIAL_STATE,
    navTitle: null,
    showBackButton: false,
    currentViewName: '',
};

export const CAROUSEL_CONTROL_TYPE = {
    PREVIOUS: 'prev',
    NEXT: 'next',
};
