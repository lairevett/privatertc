import {ACTION, ENDPOINT, CONFIG_INITIAL_STATE} from './constants';
import {apiGetRequest} from './utils/api/requests';

export const retrieveConfig = () => apiGetRequest(ACTION.RETRIEVE_CONFIG, CONFIG_INITIAL_STATE, ENDPOINT.CONFIG);
export const setNavTitle = title => ({type: ACTION.SET_NAV_TITLE, payload: title});
export const setShowBackButton = showBackButton => ({type: ACTION.SET_SHOW_BACK_BUTTON, payload: showBackButton});
export const setCurrentViewName = name => ({type: ACTION.SET_CURRENT_VIEW_NAME, payload: name});
