import {DEFAULT_LIST_INITIAL_STATE, DEFAULT_META_INITIAL_STATE, META_PROP_TYPE} from '../app/utils/api/constants';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../groups/constants';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../users/constants';

export const MODULE_NAME = 'calls';

export const TYPE = {
    AUDIO: 'TYPE_AUDIO',
    AUDIO_AND_VIDEO: 'TYPE_AUDIO_AND_VIDEO',
};

export const STATUS = {
    INCOMING: 'STATUS_INCOMING',
    OUTGOING: 'STATUS_OUTGOING',
    CONNECTED: 'STATUS_CONNECTED',
    DISCONNECTING: 'STATUS_DISCONNECTING',
    DISCONNECTED: 'STATUS_DISCONNECTED',
};

export const HUMAN_READABLE_STATUS = new Map([
    [STATUS.INCOMING, {statusString: 'Incoming call', alertColor: 'info'}],
    [STATUS.OUTGOING, {statusString: 'Calling', alertColor: 'info'}],
    [STATUS.CONNECTED, {statusString: 'Connected', alertColor: 'success'}],
    [STATUS.DISCONNECTING, {statusString: 'Disconnecting', alertColor: 'info'}],
    [STATUS.DISCONNECTED, {statusString: 'Disconnected', alertColor: 'danger'}],
]);

export const ENDPOINT = {
    LIST: page => `/v1/${MODULE_NAME}/?page=${page}`,
};

export const ACTION = {
    CREATE: `@${MODULE_NAME}/CREATE`,
    UPDATE_ACTIVE: `@${MODULE_NAME}/UPDATE_ACTIVE`,
    ADD_ACTIVE_VIDEO_SENDER: `@${MODULE_NAME}/ADD_ACTIVE_VIDEO_SENDER`,
    REMOVE_ACTIVE_VIDEO_SENDER: `@${MODULE_NAME}/REMOVE_ACTIVE_VIDEO_SENDER`,
    RETRIEVE_LIST: `@${MODULE_NAME}/RETRIEVE_LIST`,
    RETRIEVE_LIST_APPEND: `@${MODULE_NAME}/RETRIEVE_LIST_APPEND`,
    CLEAR_LIST: `@${MODULE_NAME}/CLEAR_LIST`,
};

export const ACTIVE_INITIAL_STATE = {
    id: '',
    type: '',
    status: STATUS.DISCONNECTED,
    conversationId: '',
    participants: {},
    isAudioTrackEnabled: false,
    videoSenders: [],
    startedAt: '',
};

export const LIST_INITIAL_STATE = {...DEFAULT_LIST_INITIAL_STATE};

export const DETAILS_INITIAL_STATE = {
    id: '',
    conversation: {
        id: '',
        to_user: null,
        to_group: null,
    },
    participants: {},
    started_at: '',
    ended_at: '',
    ...DEFAULT_META_INITIAL_STATE,
};

export const INITIAL_STATE = {
    active: ACTIVE_INITIAL_STATE,
    list: LIST_INITIAL_STATE,
};

export const PARTICIPANTS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.objectOf(PropertyTypes.arrayOf(PropertyTypes.string.isRequired).isRequired);

export const DETAILS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        id: PropertyTypes.string.isRequired,
        conversation: PropertyTypes.shape({
            id: PropertyTypes.string.isRequired,
            to_user: USER_DETAILS_PROP_TYPE(PropertyTypes),
            to_group: GROUP_DETAILS_PROP_TYPE(PropertyTypes),
        }).isRequired,
        participants: PARTICIPANTS_PROP_TYPE(PropertyTypes).isRequired,
        started_at: PropertyTypes.string.isRequired,
        ended_at: PropertyTypes.string,
        is_outgoing: PropertyTypes.bool,
        __meta__: META_PROP_TYPE(PropertyTypes),
    });

export const LIST_RESULTS_PROP_TYPE = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPE(PropertyTypes));
