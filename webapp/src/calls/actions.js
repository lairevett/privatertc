import {apiGetListRequest, apiPostRequest} from '../app/utils/api/requests';
import {ACTION, LIST_INITIAL_STATE, ENDPOINT} from './constants';
import {DETAILS_INITIAL_STATE} from '../groups/constants';

export const create = conversationId =>
    apiPostRequest(ACTION.CREATE, DETAILS_INITIAL_STATE, ENDPOINT.LIST(1), {sender_conversation: conversationId});

export const updateActive = active => ({type: ACTION.UPDATE_ACTIVE, payload: active});

export const addActiveVideoSender = (userId, wsChannelName) => ({
    type: ACTION.ADD_ACTIVE_VIDEO_SENDER,
    payload: {userId, wsChannelName},
});

export const removeActiveVideoSender = (userId, wsChannelName) => ({
    type: ACTION.REMOVE_ACTIVE_VIDEO_SENDER,
    payload: {userId, wsChannelName},
});

export const retrieveList = (page = 1) =>
    apiGetListRequest(
        page > 1 ? ACTION.RETRIEVE_LIST_APPEND : ACTION.RETRIEVE_LIST,
        LIST_INITIAL_STATE,
        ENDPOINT.LIST(page)
    );

export const clearList = () => ({type: ACTION.CLEAR_LIST});
