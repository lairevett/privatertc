import {INITIAL_STATE, ACTION, LIST_INITIAL_STATE} from './constants';

const callsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.UPDATE_ACTIVE:
            return {
                ...state,
                active: {
                    ...state.active,
                    ...action.payload,
                },
            };
        case ACTION.ADD_ACTIVE_VIDEO_SENDER: {
            const {userId, wsChannelName} = action.payload;

            return {
                ...state,
                active: {
                    ...state.active,
                    videoSenders: [...state.active.videoSenders, [userId, wsChannelName]],
                },
            };
        }
        case ACTION.REMOVE_ACTIVE_VIDEO_SENDER:
            return {
                ...state,
                active: {
                    ...state.active,
                    videoSenders: state.active.videoSenders.filter(
                        ([_, wsChannelName]) => wsChannelName !== action.payload.wsChannelName
                    ),
                },
            };
        case ACTION.RETRIEVE_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.RETRIEVE_LIST_APPEND:
            return {
                ...state,
                list: {
                    ...state.list,
                    results: {
                        ...state.list.results,
                        ...action.payload.results,
                    },
                },
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: LIST_INITIAL_STATE,
            };
        default:
            return state;
    }
};

export default callsReducer;
