import {reverseUrl, history} from '../../app/utils/router';

export const redirectToCall = conversationId => {
    const redirectPath = reverseUrl('conversations:details_call', {conversationId}).path;
    history.push(redirectPath, {referrer: history.location.pathname});
};

export const redirectFromCall = (conversationId, redirectToDetails) => {
    const currentPath = history.location.pathname;
    const callUrlPattern = reverseUrl('conversations:details_call', {conversationId: '.*'}).path;
    // eslint-disable-next-line security/detect-non-literal-regexp
    const callUrlRegex = new RegExp(callUrlPattern);
    const isInCallPage = callUrlRegex.test(currentPath);

    if (isInCallPage) {
        // Don't redirect user anywhere if he's on other page, e.g. chatting in conversation during call.
        const referrer = history?.location?.state?.referrer;

        if (!referrer) {
            // No referrer, user must have clicked back button or
            // some other navigation link before navigating to call view again.
            history.goBack();
            return;
        }

        if (conversationId && redirectToDetails) {
            const detailsPage = reverseUrl('conversations:details', {conversationId}).path;

            if (referrer === detailsPage) {
                // Don't put another details page on the history stack.
                history.goBack();
                return;
            }

            history.replace(detailsPage, {referrer: null});
            return;
        }

        history.replace(referrer, {referrer: null});
    }
};
