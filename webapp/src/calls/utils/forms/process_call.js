import {SubmissionError} from 'redux-form';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {STATUS as API_STATUS} from '../../../app/utils/api/constants';
import {updateActive, create} from '../../actions';
import {createConversation as usersCreateConversation} from '../../../users/actions';
import {obtainMediaContext as wrtcObtainMediaContext} from '../../../wrtc/actions';
import {sendMessage as wsSendMessage} from '../../../ws/actions';
import {makeAnswerCallMessage} from '../../../ws/utils/factory';
import {STATUS} from '../../constants';
import {redirectToCall} from '../router';

export const submit = async (_, dispatch, {conversationId, userId, type}) => {
    const succeeded = await dispatch(wrtcObtainMediaContext(type));
    if (!succeeded) {
        throw new SubmissionError({_error: 'Microphone permissions were not granted.'});
    }

    // Conversation id is passed from conversation details or call bar (userId empty).
    // User id is passed from user list (conversationId empty).
    let finalConversationId = conversationId;

    if (userId) {
        // TODO: create conversation and call in api with one request
        // Create a conversation before calling.
        const {payload: conversationPayload} = dispatch(await usersCreateConversation(userId)());

        if (conversationPayload.__meta__.status !== API_STATUS.RESPONSE_CONFLICT) {
            // Conversation has already been created.
            apiCheckSubmitErrors(conversationPayload);
        }

        finalConversationId = conversationPayload.id;
    }

    dispatch(
        updateActive({
            status: STATUS.OUTGOING,
            conversationId: finalConversationId,
            startedAt: new Date().toJSON(),
        })
    );

    const {payload: callPayload} = dispatch(await create(finalConversationId)());
    apiCheckSubmitErrors(callPayload);
    dispatch(updateActive({id: callPayload.id}));

    const message = makeAnswerCallMessage(callPayload.id);
    dispatch(wsSendMessage(message));

    redirectToCall(finalConversationId);
};
