import {RequireSignedIn} from '../app/components/partials/route_access';
import {registerUrl} from '../app/utils/router';
import List from './components/list';
import {MODULE_NAME} from './constants';
import {callbackGetDispatchEvents} from './utils/dispatch_events';

registerUrl(`${MODULE_NAME}:list`, `/${MODULE_NAME}/list`, [RequireSignedIn, List], {callbackGetDispatchEvents});
