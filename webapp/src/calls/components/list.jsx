import React from 'react';
import {useSelector} from 'react-redux';
import deepEqual from 'deep-equal';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import CallList from './partials/call_list';

const List = () => {
    const results = useSelector(state => state.calls.list.results, deepEqual);
    const listStatus = useSelector(state => state.calls.list.__meta__.status);

    return (
        <RegularView>
            <Suspense status={listStatus} fallback={<Loading />}>
                <CallList results={results} />
            </Suspense>
        </RegularView>
    );
};

export default List;
