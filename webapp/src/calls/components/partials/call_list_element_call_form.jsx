import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import {TYPE} from '../../constants';
import CallForm from './call_form';

const CallListElementCallForm = ({conversationId}) => (
    <TableListCell appendClassName="p-0">
        <CallForm form={`callsAudioCall_${conversationId}`} conversationId={conversationId} type={TYPE.AUDIO} />
    </TableListCell>
);

CallListElementCallForm.propTypes = {
    conversationId: PropTypes.string.isRequired,
};

export default CallListElementCallForm;
