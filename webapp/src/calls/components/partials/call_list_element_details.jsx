import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import {convertMillisecondsToElapsedTimeUnits} from '../../../app/utils/dates';

const CallListElementDetails = ({entityDisplayName, startedAt, endedAt}) => {
    const startedInstance = new Date(startedAt);
    const endedInstance = new Date(endedAt);
    const delta = endedInstance.getTime() - startedInstance.getTime();
    const elapsedTime = convertMillisecondsToElapsedTimeUnits(delta);

    return (
        <TableListCell appendClassName="w-100">
            <span className="vertical-center">
                <span>{entityDisplayName}</span>
                <br />
                <span className="text-muted">
                    {new Date(startedAt).toLocaleString()} ({elapsedTime})
                </span>
            </span>
        </TableListCell>
    );
};

CallListElementDetails.propTypes = {
    entityDisplayName: PropTypes.string.isRequired,
    startedAt: PropTypes.string.isRequired,
    endedAt: PropTypes.string.isRequired,
};

export default CallListElementDetails;
