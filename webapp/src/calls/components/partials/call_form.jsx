import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {propTypes, reduxForm} from 'redux-form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {TYPE} from '../../constants';
import {submit} from '../../utils/forms/process_call';

const CallForm = ({handleSubmit, submitting, type, userId, isNav}) => {
    const activeCallId = useSelector(state => state.calls.active.id);
    const isInCall = !!activeCallId;
    const currentUserId = useSelector(state => state.auth.currentUser.id);

    const [title, icon] = type === TYPE.AUDIO ? ['Call', 'call'] : ['Call with video', 'videocam'];

    return (
        <form onSubmit={handleSubmit(submit)} className={!isNav ? 'h-100' : ''}>
            <SubmitButton
                disabled={submitting || isInCall || userId === currentUserId}
                appendClassName={!isNav ? 'h-100' : undefined}
                colorClassName={isNav ? 'bmd-btn-icon text-light' : undefined}
                title={title}
            >
                <span className="material-icons">{icon}</span>
            </SubmitButton>
        </form>
    );
};

CallForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    conversationId: PropTypes.string,
    userId: PropTypes.string,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    type: PropTypes.oneOf(Object.values(TYPE)).isRequired,
};

CallForm.defaultProps = {
    conversationId: '',
    userId: '',
};

// Name generated automatically during rendering.
export default reduxForm()(CallForm);
