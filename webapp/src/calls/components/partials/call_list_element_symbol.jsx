import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';

const CallListElementSymbol = ({isOutgoing}) => {
    const icon = `call_${isOutgoing ? 'made' : 'received'}`;

    return (
        <TableListCell>
            <span className="material-icons vertical-center">{icon}</span>
        </TableListCell>
    );
};

CallListElementSymbol.propTypes = {
    isOutgoing: PropTypes.bool.isRequired,
};

export default CallListElementSymbol;
