import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import PaginatedTableList from '../../../app/components/partials/paginated_table_list';
import NoResultsText from '../../../app/components/partials/no_results_text';
import {SCROLL_DIRECTION} from '../../../app/utils/api/constants';
import {getDisplayName} from '../../../app/utils/display_name';
import {LIST_RESULTS_PROP_TYPE} from '../../constants';
import {retrieveList} from '../../actions';
import CallListElement from './call_list_element';

const CallList = ({results}) => {
    const nextPageLink = useSelector(state => state.calls.list.next);

    return results.length > 0 ? (
        <PaginatedTableList
            scrollDirection={SCROLL_DIRECTION.BOTTOM}
            nextPageLink={nextPageLink}
            actionCallback={retrieveList}
        >
            {results.map(({id, conversation, started_at, ended_at, is_outgoing}) => {
                const {id: conversationId, to_user, to_group} = conversation;
                const toEntity = to_user ?? to_group;
                const entityDisplayName = getDisplayName(toEntity);

                return (
                    <CallListElement
                        key={id}
                        isOutgoing={is_outgoing}
                        entityDisplayName={entityDisplayName}
                        startedAt={started_at}
                        endedAt={ended_at}
                        conversationId={conversationId}
                    />
                );
            })}
        </PaginatedTableList>
    ) : (
        <NoResultsText />
    );
};

CallList.propTypes = {
    results: LIST_RESULTS_PROP_TYPE(PropTypes).isRequired,
};

export default memo(CallList);
