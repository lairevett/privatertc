import React from 'react';
import PropTypes from 'prop-types';
import TableListRow from '../../../app/components/partials/table_list_row';
import CallListElementSymbol from './call_list_element_symbol';
import CallListElementDetails from './call_list_element_details';
import CallListElementCallForm from './call_list_element_call_form';

const CallListElement = ({isOutgoing, entityDisplayName, startedAt, endedAt, conversationId}) => (
    <TableListRow>
        <CallListElementSymbol isOutgoing={isOutgoing} />
        <CallListElementDetails entityDisplayName={entityDisplayName} startedAt={startedAt} endedAt={endedAt} />
        <CallListElementCallForm conversationId={conversationId} />
    </TableListRow>
);

CallListElement.propTypes = {
    isOutgoing: PropTypes.bool.isRequired,
    entityDisplayName: PropTypes.string.isRequired,
    startedAt: PropTypes.string.isRequired,
    endedAt: PropTypes.string.isRequired,
    conversationId: PropTypes.string.isRequired,
};

export default CallListElement;
