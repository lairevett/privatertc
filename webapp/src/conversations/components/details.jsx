import React, {useEffect} from 'react';
import deepEqual from 'deep-equal';
import {useSelector, useDispatch} from 'react-redux';
import {setNavTitle} from '../../app/actions';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import MasterDetailView from '../../app/components/partials/master_detail_view';
import {combineStatuses} from '../../app/utils/api/helpers';
import {getDisplayName} from '../../app/utils/display_name';
import ConversationList from './partials/conversation_list';
import ConversationBox from './partials/conversation_box';

const Details = () => {
    const dispatch = useDispatch();

    const {online_user_ids, ...details} = useSelector(state => state.conversations.details, deepEqual);
    const selectedEntity = details.to_user || details.to_group;

    // ConversationBox used in rooms, can't put it into component.
    const messagesResults = useSelector(state => state.conversations.messages.results, deepEqual);

    const listStatus = useSelector(state => state.conversations.list.__meta__.status);
    const messagesStatus = useSelector(state => state.conversations.messages.__meta__.status);

    useEffect(() => {
        if (selectedEntity) {
            const navTitle = getDisplayName(selectedEntity);
            dispatch(setNavTitle(navTitle));
        }
    }, [dispatch, selectedEntity]);

    const master = (
        <RegularView>
            <Suspense status={listStatus} fallback={<Loading />}>
                <ConversationList selectedEntity={selectedEntity} />
            </Suspense>
        </RegularView>
    );

    const combinedStatuses = combineStatuses([details.__meta__.status, messagesStatus]);
    const detail = (
        <RegularView>
            <Suspense status={combinedStatuses} fallback={<Loading />}>
                <ConversationBox details={details} messagesResults={messagesResults} recipients={online_user_ids} />
            </Suspense>
        </RegularView>
    );

    return <MasterDetailView master={master} detail={detail} preferDetail />;
};

export default Details;
