import React, {useEffect} from 'react';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {setNavTitle} from '../../app/actions';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import MasterDetailView from '../../app/components/partials/master_detail_view';
import {getDisplayName} from '../../app/utils/display_name';
import ConversationList from './partials/conversation_list';
import ConversationCallBox from './partials/conversation_call_box';

const Call = () => {
    const dispatch = useDispatch();

    const toUser = useSelector(state => state.conversations.details.to_user, shallowEqual);
    const toGroup = useSelector(state => state.conversations.details.to_group, shallowEqual);
    const selectedEntity = toUser || toGroup;

    const listStatus = useSelector(state => state.conversations.list.__meta__.status);

    useEffect(() => {
        if (selectedEntity) {
            const navTitle = getDisplayName(selectedEntity);
            dispatch(setNavTitle(navTitle));
        }
    }, [dispatch, selectedEntity]);

    const master = (
        <RegularView>
            <Suspense status={listStatus} fallback={<Loading />}>
                <ConversationList selectedEntity={selectedEntity} />
            </Suspense>
        </RegularView>
    );

    const detail = (
        <RegularView>
            {selectedEntity && <ConversationCallBox selectedEntity={selectedEntity} isToGroup={!!toGroup} />}
        </RegularView>
    );

    // List status + details status combo not needed, as selectedEntity is not required in the component.
    return <MasterDetailView master={master} detail={detail} preferDetail />;
};

export default Call;
