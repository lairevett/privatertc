import React from 'react';
import {useSelector} from 'react-redux';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import MasterDetailView from '../../app/components/partials/master_detail_view';
import ConversationList from './partials/conversation_list';

const List = () => {
    const status = useSelector(state => state.conversations.list.__meta__.status);

    const master = (
        <RegularView>
            <Suspense status={status} fallback={<Loading />}>
                <ConversationList />
            </Suspense>
        </RegularView>
    );

    return <MasterDetailView master={master} />;
};

export default List;
