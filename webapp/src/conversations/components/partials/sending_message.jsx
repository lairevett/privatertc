import React from 'react';
import PropTypes from 'prop-types';
import {useCurrentUser} from '../../../auth/utils/hooks';
import Message from './message';

const SendingMessage = ({id, content}) => {
    const currentUser = useCurrentUser();

    return (
        <Message tabIndex="-1" id={id} sender={currentUser.id} content={content} has_attachments={false} hasSpinner />
    );
};

SendingMessage.propTypes = {
    id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
};

export default SendingMessage;
