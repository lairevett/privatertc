import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import ModalShowButton from '../../../app/components/partials/modal_show_button';
import {retrieveMessagesAttachments} from '../../actions';
import {MODAL} from '../../constants';

const MessageAttachmentListModalShowButton = ({messageId}) => {
    const dispatch = useDispatch();
    const conversationId = useSelector(state => state.conversations.details.id);

    return (
        <ModalShowButton
            modalId={MODAL.MESSAGE_ATTACHMENT_LIST}
            onClick={async () => dispatch(await retrieveMessagesAttachments(conversationId, messageId)())}
            colorClassName="btn-dark"
            appendClassName="attachment-list-modal-show bmd-btn-icon"
        >
            <span className="material-icons">perm_media</span>
        </ModalShowButton>
    );
};

MessageAttachmentListModalShowButton.propTypes = {
    messageId: PropTypes.string.isRequired,
};

export default MessageAttachmentListModalShowButton;
