import React from 'react';
import PropTypes from 'prop-types';
import {getFileExtension} from '../../../app/utils/form';

const AttachmentRawIcon = ({name, isLarge}) => {
    const extension = getFileExtension(name);
    const fivIconName = extension || 'blank';
    // Fallback to fiv-icon-blank if non-existent extension icon.
    const fivIconClasses = extension !== 'blank' ? `fiv-icon-blank fiv-icon-${fivIconName}` : 'fiv-icon-blank';

    return <span className={`fiv-viv ${fivIconClasses} ${isLarge ? 'fiv-viv-lg' : ''}`} />;
};

AttachmentRawIcon.propTypes = {
    name: PropTypes.string.isRequired,
    isLarge: PropTypes.bool,
};

AttachmentRawIcon.defaultProps = {
    isLarge: false,
};

export default AttachmentRawIcon;
