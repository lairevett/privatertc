import React from 'react';
import {useSelector} from 'react-redux';
import {TYPE as CALL_TYPE} from '../../../calls/constants';
import CallForm from '../../../calls/components/partials/call_form';

const NavAudioCall = () => {
    const conversationId = useSelector(state => state.conversations.details.id);
    return <CallForm form="navAudioCall" conversationId={conversationId} type={CALL_TYPE.AUDIO} isNav />;
};

export default NavAudioCall;
