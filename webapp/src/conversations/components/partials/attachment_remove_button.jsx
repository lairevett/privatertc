import React from 'react';
import {change} from 'redux-form';
import PropTypes from 'prop-types';
import deepEqual from 'deep-equal';
import {useSelector, useDispatch} from 'react-redux';
import TableListCell from '../../../app/components/partials/table_list_cell';
import Button from '../../../app/components/partials/button';

const AttachmentRemoveButton = ({name}) => {
    const attachments = useSelector(state => state.form.conversationsMessage.values.attachments, deepEqual);
    const dispatch = useDispatch();

    const remove = () => {
        // There are some tricks to create mutable FileList with DataTransfer/ClipboardEvent.clipboardData
        // but they are not widely supported by the browsers, just create
        // a new FormData() object using these filtered results.
        const nextAttachments = attachments.filter(({name: comparedFileName}) => comparedFileName !== name);
        dispatch(change('conversationsMessage', 'attachments', nextAttachments));
    };

    return (
        <TableListCell appendClassName="justify-content-center">
            <Button colorClassName="bmd-btn-icon" appendClassName="text-danger" onClick={remove}>
                <span className="material-icons">close</span>
            </Button>
        </TableListCell>
    );
};

AttachmentRemoveButton.propTypes = {
    name: PropTypes.string.isRequired,
};

export default AttachmentRemoveButton;
