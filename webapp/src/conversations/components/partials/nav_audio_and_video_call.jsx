import React from 'react';
import {useSelector} from 'react-redux';
import {TYPE as CALL_TYPE} from '../../../calls/constants';
import CallForm from '../../../calls/components/partials/call_form';

const NavAudioAndVideoCall = () => {
    const conversationId = useSelector(state => state.conversations.details.id);

    return (
        <CallForm form="navAudioAndVideoCall" conversationId={conversationId} type={CALL_TYPE.AUDIO_AND_VIDEO} isNav />
    );
};

export default NavAudioAndVideoCall;
