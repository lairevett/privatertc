import React from 'react';
import {reduxForm, propTypes} from 'redux-form';
import PropTypes from 'prop-types';
import FormError from '../../../app/components/partials/form_error';
import Modal from '../../../app/components/partials/modal';
import ModalBody from '../../../app/components/partials/modal_body';
import ModalFooter from '../../../app/components/partials/modal_footer';
import ModalCloseButton from '../../../app/components/partials/modal_close_button';
import SubmitButton from '../../../app/components/partials/submit_button';
import {MODAL, MODULE_NAME} from '../../constants';
import {submit} from '../../utils/forms/process_leave_group';

const LeaveGroupModalForm = ({form, handleSubmit, error}) => (
    <Modal id={MODAL.LEAVE_GROUP} title="Confirmation">
        <ModalBody>
            {error && <FormError message={error} />}
            Do you really want to leave this group?
        </ModalBody>
        <ModalFooter>
            <form id={form} className="d-none" onSubmit={handleSubmit(submit)} />
            <ModalCloseButton colorClassName="btn-secondary" value="No" />
            <SubmitButton form={form} value="Yes" />
        </ModalFooter>
    </Modal>
);

LeaveGroupModalForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    conversationId: PropTypes.string.isRequired, // Used in submit().
};

export default reduxForm({form: `${MODULE_NAME}LeaveGroupModal`})(LeaveGroupModalForm);
