import React from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import NavDropdownForm from '../../../app/components/partials/nav_dropdown_form';
import {submit} from '../../utils/forms/process_trigger_block';

const NavDropdownTriggerBlockForm = ({conversationId}) => {
    const currentIsLocallyBlocked = useSelector(state => state.conversations.details.blockage?.local);

    return (
        <NavDropdownForm
            form="navDropdownTriggerBlock"
            processSubmit={submit}
            value={currentIsLocallyBlocked ? 'Unblock' : 'Block'}
            conversationId={conversationId}
        />
    );
};

NavDropdownTriggerBlockForm.propTypes = {
    conversationId: PropTypes.string.isRequired,
};

export default NavDropdownTriggerBlockForm;
