import React from 'react';
import PropTypes from 'prop-types';
import {getDisplayName} from '../../../app/utils/display_name';
import {reverseUrl} from '../../../app/utils/router';
import TableListRow from '../../../app/components/partials/table_list_row';
import UserListElementActiveStatus from '../../../users/components/partials/user_list_element_active_status';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../../../users/constants';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../../../groups/constants';
import ConversationListElementInfo from './conversation_list_element_info';

const ConversationListElement = ({id, toEntity, lastMessage, isActive}) => {
    const conversationName = getDisplayName(toEntity);

    return (
        <TableListRow to={reverseUrl('conversations:details', {conversationId: id}).path} isActive={isActive}>
            <UserListElementActiveStatus isOnline={toEntity.is_online} style={{width: '10%'}} />
            <ConversationListElementInfo conversationName={conversationName} lastMessage={lastMessage} />
        </TableListRow>
    );
};

ConversationListElement.propTypes = {
    id: PropTypes.string.isRequired,
    toEntity: PropTypes.oneOfType([USER_DETAILS_PROP_TYPE(PropTypes), GROUP_DETAILS_PROP_TYPE(PropTypes)]).isRequired,
    lastMessage: PropTypes.string,
    isActive: PropTypes.bool,
};

ConversationListElement.defaultProps = {
    lastMessage: null,
    isActive: false,
};

export default ConversationListElement;
