import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {MESSAGES_RESULTS_PROP_TYPE, DETAILS_PROP_TYPE} from '../../constants';
import MessageBox from './message_box';
import MessageForm from './message_form';
import GroupInfoModal from './group_info_modal';
import ClearChatModalForm from './clear_chat_modal_form';
import LeaveGroupModalForm from './leave_group_modal_form';
import MessageAttachmentListModal from './message_attachment_list_modal';
import MessageSearch from './message_search';

const ConversationBox = ({details, messagesResults, recipients}) => {
    const messagesSearchReference = useRef();
    const messagesSearchVisible = useSelector(state => state.conversations.messagesSearch.visible);

    return (
        <div className="d-flex flex-column align-items-between h-100">
            {messagesSearchVisible && (
                <MessageSearch ref={messagesSearchReference} conversationId={details.id} results={messagesResults} />
            )}
            <MessageBox
                conversationId={details?.id}
                results={messagesResults}
                messagesSearchVisible={messagesSearchVisible}
                messagesSearchReference={messagesSearchReference}
            />
            <div className="border-top">
                <MessageForm
                    conversation={details}
                    isConversationEmpty={messagesResults.length === 0}
                    recipients={recipients}
                />
            </div>
            {details && (
                <>
                    {details.to_group && (
                        <>
                            <GroupInfoModal group={details.to_group} />
                            <LeaveGroupModalForm conversationId={details.id} />
                        </>
                    )}
                    <ClearChatModalForm conversationId={details.id} removeFromList={!!details.to_user} />
                    <MessageAttachmentListModal />
                </>
            )}
        </div>
    );
};

ConversationBox.propTypes = {
    details: DETAILS_PROP_TYPE(PropTypes),
    messagesResults: MESSAGES_RESULTS_PROP_TYPE(PropTypes).isRequired,
    recipients: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

ConversationBox.defaultProps = {
    details: null,
};

export default ConversationBox;
