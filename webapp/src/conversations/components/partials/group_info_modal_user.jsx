import React from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {getDisplayName} from '../../../app/utils/display_name';
import {MEMBER_PROP_TYPE as GROUP_MEMBER_PROP_TYPE} from '../../../groups/constants';
import ActiveStatusBadge from '../../../users/components/partials/active_status_badge';

const GroupInfoModalUser = ({user}) => {
    const currentUserId = useSelector(state => state.auth.currentUser.id);
    // TODO: $('#myModal').modal('handleUpdate') when scrollbar appears

    return (
        <p>
            <ActiveStatusBadge appendClassName="mb-1 mr-2" isOnline={user.is_online} />
            <span>
                {getDisplayName(user)}
                {user.id === currentUserId && ' (You)'}
            </span>
        </p>
    );
};

GroupInfoModalUser.propTypes = {
    user: GROUP_MEMBER_PROP_TYPE(PropTypes).isRequired,
};

export default GroupInfoModalUser;
