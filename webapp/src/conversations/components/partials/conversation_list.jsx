import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import deepEqual from 'deep-equal';
import PaginatedTableList from '../../../app/components/partials/paginated_table_list';
import NoResultsText from '../../../app/components/partials/no_results_text';
import {SCROLL_DIRECTION} from '../../../app/utils/api/constants';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../../../users/constants';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../../../groups/constants';
import {retrieveList} from '../../actions';
import ConversationListElement from './conversation_list_element';

const ConversationList = ({selectedEntity}) => {
    const results = useSelector(state => state.conversations.list.results, deepEqual);
    const nextPageLink = useSelector(state => state.conversations.list.next);

    // Count could be 0 during search.
    return results.length > 0 ? (
        <PaginatedTableList
            scrollDirection={SCROLL_DIRECTION.BOTTOM}
            nextPageLink={nextPageLink}
            actionCallback={retrieveList}
        >
            {results.map(({id, to_user, to_group, last_message}) => {
                const toEntity = to_user || to_group;

                return (
                    <ConversationListElement
                        key={id}
                        id={id}
                        toEntity={toEntity}
                        lastMessage={last_message}
                        isActive={selectedEntity && selectedEntity.id === toEntity.id}
                    />
                );
            })}
        </PaginatedTableList>
    ) : (
        <NoResultsText value="No conversations." />
    );
};

ConversationList.propTypes = {
    selectedEntity: PropTypes.oneOfType([USER_DETAILS_PROP_TYPE(PropTypes), GROUP_DETAILS_PROP_TYPE(PropTypes)]),
};

ConversationList.defaultProps = {
    selectedEntity: null, // Not needed in list component.
};

export default memo(ConversationList);
