import React, {useEffect, useState, memo} from 'react';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import Button from '../../../app/components/partials/button';
import {throttle} from '../../../app/utils/events';
import {autoClickMessage} from '../../actions';

const MessageSearchNavigateButtons = () => {
    const dispatch = useDispatch();
    const [currentIndex, setCurrentIndex] = useState(-1);
    const matchingIds = useSelector(state => state.conversations.messagesSearch.matchingIds, shallowEqual);

    useEffect(() => {
        const isInBounds = currentIndex > -1 && currentIndex < matchingIds.length;

        if (isInBounds) {
            const messageId = matchingIds?.[currentIndex];
            dispatch(autoClickMessage(messageId));
        }
    }, [matchingIds, currentIndex, dispatch]);

    const incrementIndex = throttle(
        () => (currentIndex < matchingIds.length ? setCurrentIndex(currentIndex + 1) : undefined),
        500
    );
    const decrementIndex = throttle(() => (currentIndex > -1 ? setCurrentIndex(currentIndex - 1) : undefined), 500);

    return (
        <>
            <Button appendClassName="p-3" onClick={incrementIndex}>
                <span className="material-icons">expand_less</span>
            </Button>
            <Button appendClassName="p-3" onClick={decrementIndex}>
                <span className="material-icons">expand_more</span>
            </Button>
        </>
    );
};

export default memo(MessageSearchNavigateButtons);
