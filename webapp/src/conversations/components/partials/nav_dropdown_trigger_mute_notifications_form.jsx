import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import NavDropdownForm from '../../../app/components/partials/nav_dropdown_form';
import {submit} from '../../utils/forms/process_trigger_mute_notifications';

const NavDropdownTriggerMuteNotificationsForm = ({conversationId}) => {
    const currentIsMuted = useSelector(state => state.conversations.details.is_muted);

    return (
        <NavDropdownForm
            form="navDropdownTriggerMuteNotifications"
            processSubmit={submit}
            value={`${currentIsMuted ? 'Unmute' : 'Mute'} notifications`}
            conversationId={conversationId}
            currentIsMuted={currentIsMuted}
        />
    );
};

NavDropdownTriggerMuteNotificationsForm.propTypes = {
    conversationId: PropTypes.string.isRequired,
};

export default NavDropdownTriggerMuteNotificationsForm;
