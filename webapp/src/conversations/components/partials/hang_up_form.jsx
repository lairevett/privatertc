import React from 'react';
import PropTypes from 'prop-types';
import {propTypes, reduxForm} from 'redux-form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {MODULE_NAME} from '../../constants';
import {submit} from '../../utils/forms/process_hang_up';

const HangUpForm = ({handleSubmit, submitting}) => (
    <form onSubmit={handleSubmit(submit)}>
        <SubmitButton disabled={submitting} title="Hang up" colorClassName="btn-danger" appendClassName="bmd-btn-fab">
            <span className="material-icons">call_end</span>
        </SubmitButton>
    </form>
);

HangUpForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    redirectToDetails: PropTypes.bool, // Used in submit().
};

HangUpForm.defaultProps = {
    redirectToDetails: false,
};

export default reduxForm({form: `${MODULE_NAME}HangUp`})(HangUpForm);
