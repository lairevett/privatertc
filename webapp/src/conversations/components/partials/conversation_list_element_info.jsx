import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import SlackMessage from '../../../app/components/partials/slack_message';

const ConversationListElementInfo = ({conversationName, lastMessage}) => (
    <TableListCell appendClassName="text-truncate w-100">
        <span className="vertical-center text-truncate">
            {conversationName}
            <br />
            <span className="text-muted font-italic" style={{whiteSpace: 'nowrap'}}>
                <SlackMessage content={lastMessage != null ? lastMessage : 'Conversation started.'} />
            </span>
        </span>
    </TableListCell>
);

ConversationListElementInfo.propTypes = {
    conversationName: PropTypes.string.isRequired,
    lastMessage: PropTypes.string,
};

ConversationListElementInfo.defaultProps = {
    lastMessage: null,
};

export default ConversationListElementInfo;
