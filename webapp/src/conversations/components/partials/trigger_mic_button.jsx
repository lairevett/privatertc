import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../../../app/components/partials/button';
import {throttle} from '../../../app/utils/events';
import {triggerAudioTrackMute as wrtcTriggerAudioTrackMute} from '../../../wrtc/actions';

const TriggerMicButton = () => {
    const dispatch = useDispatch();
    const isAudioTrackEnabled = useSelector(state => state.calls.active.isAudioTrackEnabled);
    const buttonColorClass = isAudioTrackEnabled ? 'btn-primary' : 'btn-danger';
    const handleClick = throttle(() => dispatch(wrtcTriggerAudioTrackMute()), 250);

    return (
        <Button colorClassName={buttonColorClass} appendClassName="bmd-btn-fab" onClick={handleClick}>
            <span className="material-icons">{isAudioTrackEnabled ? 'mic' : 'mic_off'}</span>
        </Button>
    );
};

export default TriggerMicButton;
