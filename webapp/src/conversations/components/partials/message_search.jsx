import React, {forwardRef} from 'react';
import PropTypes from 'prop-types';
import MessageSearchNavigateButtons from './message_search_navigate_buttons';
import MessageSearchInput from './message_search_input';
import MessageSearchCloseButton from './message_search_close_button';

// TODO: display over messagebox, so that it doesn't change scroll position.
const MessageSearch = forwardRef(({messageBoxReference, ...properties}, reference) => (
    <div ref={reference} className="d-flex align-items-center w-100 border-bottom bg-light display-over">
        <MessageSearchNavigateButtons messageBoxReference={messageBoxReference} />
        <MessageSearchInput {...properties} />
        <MessageSearchCloseButton />
    </div>
));

MessageSearch.propTypes = {
    messageBoxReference: PropTypes.shape({current: PropTypes.instanceOf(Element)}).isRequired,
};

export default MessageSearch;
