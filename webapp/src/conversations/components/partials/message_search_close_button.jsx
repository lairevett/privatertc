import React, {memo} from 'react';
import {useDispatch} from 'react-redux';
import Button from '../../../app/components/partials/button';
import {triggerMessagesSearchVisible} from '../../actions';

const MessageSearchCloseButton = () => {
    const dispatch = useDispatch();

    return (
        <Button appendClassName="p-3" onClick={() => dispatch(triggerMessagesSearchVisible())}>
            <span className="material-icons">close</span>
        </Button>
    );
};

export default memo(MessageSearchCloseButton);
