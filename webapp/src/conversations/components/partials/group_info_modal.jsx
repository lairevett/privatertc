import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../app/components/partials/modal';
import ModalBody from '../../../app/components/partials/modal_body';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../../../groups/constants';
import {getSortedMembersByRanks} from '../../../groups/utils/conversations';
import {MODAL} from '../../constants';
import GroupInfoModalUser from './group_info_modal_user';

const GroupInfoModal = ({group}) => {
    const [ordinary, moderators, administrator] = getSortedMembersByRanks(group);
    const onlineParticipantsCount = group.members.filter(({is_online}) => is_online).length;

    return (
        <Modal id={MODAL.GROUP_INFO} title="Group info">
            <ModalBody>
                <p>
                    Participants: {group.members.length}
                    <br />
                    <small className="text-muted"> ({onlineParticipantsCount} online)</small>
                </p>
                <h6>Administrator</h6>
                {administrator ? <GroupInfoModalUser user={administrator} /> : <p className="font-italic">None</p>}
                <h6>Moderators</h6>
                {moderators.length > 0 ? (
                    moderators.map(moderator => <GroupInfoModalUser key={moderator.id} user={moderator} />)
                ) : (
                    <p className="font-italic">None</p>
                )}
                <h6>Users</h6>
                {ordinary.length > 0 ? (
                    ordinary.map(user => <GroupInfoModalUser key={user.id} user={user} />)
                ) : (
                    <p className="font-italic">None</p>
                )}
            </ModalBody>
        </Modal>
    );
};

GroupInfoModal.propTypes = {
    group: GROUP_DETAILS_PROP_TYPE(PropTypes).isRequired,
};

export default GroupInfoModal;
