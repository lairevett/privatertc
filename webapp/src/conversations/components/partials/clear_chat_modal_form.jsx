import React from 'react';
import {reduxForm, propTypes} from 'redux-form';
import PropTypes from 'prop-types';
import Modal from '../../../app/components/partials/modal';
import ModalBody from '../../../app/components/partials/modal_body';
import ModalFooter from '../../../app/components/partials/modal_footer';
import ModalCloseButton from '../../../app/components/partials/modal_close_button';
import SubmitButton from '../../../app/components/partials/submit_button';
import {MODAL, MODULE_NAME} from '../../constants';
import {submit} from '../../utils/forms/process_clear_chat';

const ClearChatModalForm = ({form, handleSubmit}) => (
    <Modal id={MODAL.CLEAR_CHAT} title="Confirmation">
        <ModalBody>Do you really want to clear this chat?</ModalBody>
        <ModalFooter>
            <form id={form} className="d-none" onSubmit={handleSubmit(submit)} />
            <ModalCloseButton colorClassName="btn-secondary" value="No" />
            <SubmitButton form={form} value="Yes" />
        </ModalFooter>
    </Modal>
);

ClearChatModalForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    conversationId: PropTypes.string.isRequired, // Used in submit().
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    removeFromList: PropTypes.bool.isRequired, // Used in submit().
};

export default reduxForm({form: `${MODULE_NAME}ClearChatModal`})(ClearChatModalForm);
