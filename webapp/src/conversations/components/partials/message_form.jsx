import React from 'react';
import {useSelector} from 'react-redux';
import {reduxForm, Field, propTypes} from 'redux-form';
import PropTypes from 'prop-types';
import {APP_IS_MOBILE} from '../../../app/constants';
import {FormFileField, FormField} from '../../../app/components/partials/form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {validate, submit} from '../../utils/forms/process_message';
import {DETAILS_PROP_TYPE} from '../../constants';

const MessageForm = ({form, handleSubmit, valid, pristine, submitting, conversation}) => {
    const submitForm = handleSubmit(submit);

    const onKeyDown = evnt => {
        // Send message when enter is clicked without shift on desktops.
        if (APP_IS_MOBILE) {
            if (evnt.keyCode === 13 && evnt.shiftKey === false) {
                evnt.preventDefault();
                submitForm();
            }
        }
    };

    const isUserConversation = !!conversation?.to_user;
    const isUnblockedUserConversation =
        isUserConversation && !conversation.blockage.local && !conversation.blockage.remote;
    const isGroupConversation = !!conversation?.to_group;
    const isRoom = !(isUserConversation || isGroupConversation);

    const [placeholder, isDisabled] =
        isUnblockedUserConversation || isGroupConversation || isRoom
            ? ['Type a message...', false]
            : ["You can't reply to this conversation.", true];

    const attachments = useSelector(state => state.form?.[form]?.values?.attachments);
    const attachmentsCount = attachments?.length ?? 0;

    return (
        <form onSubmit={submitForm} className="mb-2 pt-2">
            <div className="d-flex justify-content-around align-items-center">
                <Field
                    component={FormField}
                    id="content"
                    name="content"
                    type="textarea"
                    className="form-control message-textarea"
                    formGroupClassNames="w-100 pt-0 ml-2 mr-1 mb-0"
                    onKeyDown={onKeyDown}
                    placeholder={placeholder}
                    disabled={isDisabled}
                />
                <Field
                    component={FormFileField}
                    id="attachments"
                    name="attachments"
                    formGroupClassNames="pt-0 mb-0 mr-2"
                    labelClassNames="d-flex justify-content-center align-items-center btn has-badge bmd-btn-icon"
                    label={
                        <>
                            <span className="btn-badge">{attachmentsCount}</span>
                            <span className="material-icons">attach_file</span>
                        </>
                    }
                    disabled={isDisabled}
                    multiple
                />
                <SubmitButton disabled={!valid || pristine || submitting} appendClassName="bmd-btn-fab mr-2 send-btn">
                    <span className="material-icons">send</span>
                </SubmitButton>
            </div>
        </form>
    );
};

MessageForm.propTypes = {
    ...propTypes,
    conversation: DETAILS_PROP_TYPE(PropTypes), // Used in submit(), not required since the component is used in rooms, too.
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    isConversationEmpty: PropTypes.bool.isRequired, // Used in submit().
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    recipients: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired, // Used in submit().
};

MessageForm.defaultProps = {
    conversation: null,
};

export default reduxForm({
    form: 'conversationsMessage',
    validate,
})(MessageForm);
