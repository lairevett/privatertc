import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import AttachmentRawIcon from './attachment_raw_icon';

const AttachmentIcon = ({name}) => (
    <TableListCell appendClassName="justify-content-center">
        <AttachmentRawIcon name={name} />
    </TableListCell>
);

AttachmentIcon.propTypes = {
    name: PropTypes.string.isRequired,
};

export default AttachmentIcon;
