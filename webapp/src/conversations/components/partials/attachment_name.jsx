import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';

const AttachmentName = ({name}) => (
    <TableListCell appendClassName="justify-content-center w-100 h-100 break-all">{name}</TableListCell>
);

AttachmentName.propTypes = {
    name: PropTypes.string.isRequired,
};

export default AttachmentName;
