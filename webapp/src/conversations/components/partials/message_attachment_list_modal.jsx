/* eslint-disable jsx-a11y/media-has-caption, jsx-a11y/alt-text */
import React, {useEffect, useMemo, useState} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import deepEqual from 'deep-equal';
import Modal from '../../../app/components/partials/modal';
import ModalHeader from '../../../app/components/partials/modal_header';
import ModalBody from '../../../app/components/partials/modal_body';
import Carousel from '../../../app/components/partials/carousel';
import {API_URL} from '../../../app/utils/api/constants';
import {MESSAGE_ATTACHMENT_TYPE, MODAL} from '../../constants';
import {clearMessagesAttachments} from '../../actions';
import {getAttachmentsWithTypes} from '../../utils/messages';
import AttachmentRawIcon from './attachment_raw_icon';

const MessageAttachmentListModal = () => {
    const carouselId = 'messageAttachmentsListCarousel';
    const dispatch = useDispatch();
    const attachments = useSelector(state => state.conversations.messagesAttachments.results, deepEqual);
    const [activeAttachment, setActiveAttachment] = useState(null);

    useEffect(() => {
        const listener = () => {
            dispatch(clearMessagesAttachments());
        };

        window.$(`#${MODAL.MESSAGE_ATTACHMENT_LIST}`).on('hidden.bs.modal', listener);
        return () => {
            window.$(`#${MODAL.MESSAGE_ATTACHMENT_LIST}`).off('hidden.bs.modal', listener);
        };
    }, [dispatch]);

    useEffect(() => {
        if (attachments.length > 0 && activeAttachment == null) {
            setActiveAttachment(attachments[0]);
        } else if (attachments.length === 0) {
            setActiveAttachment(null);
        }
    }, [attachments, activeAttachment]);

    useEffect(() => {
        const listener = evnt => {
            const nextActiveAttachmentIndex = [...evnt.relatedTarget.parentNode.childNodes].indexOf(evnt.relatedTarget);
            setActiveAttachment(attachments?.[nextActiveAttachmentIndex]);
        };

        window.$(`#${carouselId}`).on('slide.bs.carousel', listener);
        return () => {
            window.$(`#${carouselId}`).off('slide.bs.carousel', listener);
        };
    }, [attachments]);

    const attachmentsWithTypes = useMemo(() => getAttachmentsWithTypes(attachments), [attachments]);

    return (
        <Modal id={MODAL.MESSAGE_ATTACHMENT_LIST} isFullScreen appendContentClassName="bg-black" hideHeader>
            <ModalHeader title={activeAttachment?.name}>
                <Link
                    to={{pathname: API_URL + activeAttachment?.url}}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="modal-button"
                >
                    <span className="material-icons">file_download</span>
                </Link>
            </ModalHeader>
            <ModalBody>
                <Carousel id={carouselId} appendClassName="h-100" withIndicators withControls>
                    {attachmentsWithTypes.map(({type, name, url}) => {
                        return type === MESSAGE_ATTACHMENT_TYPE.IMAGE ? (
                            <img key={name} src={API_URL + url} />
                        ) : type === MESSAGE_ATTACHMENT_TYPE.AUDIO ? (
                            <audio key={name} src={API_URL + url} controls />
                        ) : type === MESSAGE_ATTACHMENT_TYPE.VIDEO ? (
                            <video key={name} src={API_URL + url} controls />
                        ) : (
                            <AttachmentRawIcon key={name} name={name} isLarge />
                        );
                    })}
                </Carousel>
            </ModalBody>
        </Modal>
    );
};

export default MessageAttachmentListModal;
