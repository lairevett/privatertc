import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import deepEqual from 'deep-equal';
import NavDropdownButton from '../../../app/components/partials/nav_dropdown_button';
import NavDropdownModalShowButton from '../../../app/components/partials/nav_dropdown_modal_show_button';
import NavDropdownLink from '../../../app/components/partials/nav_dropdown_link';
import {API_URL} from '../../../app/utils/api/constants';
import {reverseUrl} from '../../../app/utils/router';
import {USER_RANK} from '../../../groups/constants';
import {MODAL, ENDPOINT} from '../../constants';
import {triggerMessagesSearchVisible} from '../../actions';
import NavDropdownTriggerMuteNotificationsForm from './nav_dropdown_trigger_mute_notifications_form';
import NavDropdownTriggerBlockForm from './nav_dropdown_trigger_block_form';

const NavDropdownDetailsContent = () => {
    // TODO: break this down into smaller components.
    const dispatch = useDispatch();
    const conversationId = useSelector(state => state.conversations.details.id);
    const toGroup = useSelector(state => state.conversations.details.to_group, deepEqual);
    const currentUserId = useSelector(state => state.auth.currentUser.id);

    const currentGroupMemberEntry = toGroup?.members?.find(({id}) => id === currentUserId);
    const isCurrentUserPrivileged = (currentGroupMemberEntry?.user_rank ?? USER_RANK.ORDINARY) > USER_RANK.ORDINARY;
    const groupUpdateUrl = reverseUrl('groups:update', {groupId: toGroup?.id});

    return (
        <>
            {toGroup && (
                <>
                    <NavDropdownModalShowButton modalId={MODAL.GROUP_INFO} value="Group info" />
                    {isCurrentUserPrivileged && (
                        <NavDropdownLink to={groupUpdateUrl.path} value={groupUpdateUrl.wrappedViewProperties.title} />
                    )}
                    <NavDropdownModalShowButton modalId={MODAL.LEAVE_GROUP} value="Leave group" />
                    <div className="dropdown-divider" />
                </>
            )}
            <NavDropdownLink to="#" value="Media" />
            <NavDropdownButton value="Search" onClick={() => dispatch(triggerMessagesSearchVisible())} />
            <NavDropdownTriggerMuteNotificationsForm conversationId={conversationId} />
            {!toGroup && <NavDropdownTriggerBlockForm conversationId={conversationId} />}
            <NavDropdownLink
                to={{pathname: API_URL + ENDPOINT.MESSAGES_EXPORT(conversationId)}}
                target="_blank"
                rel="noopener noreferrer"
                value="Export chat"
            />
            <NavDropdownModalShowButton modalId={MODAL.CLEAR_CHAT} value="Clear chat" />
        </>
    );
};

export default NavDropdownDetailsContent;
