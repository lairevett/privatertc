import React from 'react';
import PropTypes from 'prop-types';
import {propTypes, reduxForm} from 'redux-form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {TYPE as CALL_TYPE, PARTICIPANTS_PROP_TYPE as CALL_PARTICIPANTS_PROP_TYPE} from '../../../calls/constants';
import {MODULE_NAME} from '../../constants';
import {submit} from '../../utils/forms/process_answer';

const AnswerForm = ({handleSubmit, submitting, type}) => (
    <form onSubmit={handleSubmit(submit)}>
        <SubmitButton disabled={submitting} title="Answer" appendClassName="bmd-btn-fab">
            <span className="material-icons">{type === CALL_TYPE.AUDIO ? 'call' : 'videocam'}</span>
        </SubmitButton>
    </form>
);

AnswerForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    id: PropTypes.string.isRequired, // Used in submit().
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    participants: CALL_PARTICIPANTS_PROP_TYPE(PropTypes).isRequired, // Used in submit().
    type: PropTypes.oneOf(Object.values(CALL_TYPE)).isRequired,
};

export default reduxForm({form: `${MODULE_NAME}Answer`})(AnswerForm);
