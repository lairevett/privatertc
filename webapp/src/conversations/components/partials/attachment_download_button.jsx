import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import Button from '../../../app/components/partials/button';
import {API_URL} from '../../../app/utils/api/constants';

const AttachmentDownloadButton = ({url}) => (
    <TableListCell appendClassName="justify-content-center">
        <Button
            to={{pathname: API_URL + url}}
            target="_blank"
            rel="noopener noreferrer"
            colorClassName="color-inherit"
            appendClassName="no-focus-color justify-content-center h-100"
        >
            <span className="material-icons align-self-center">file_download</span>
        </Button>
    </TableListCell>
);

AttachmentDownloadButton.propTypes = {
    url: PropTypes.string.isRequired,
};

export default AttachmentDownloadButton;
