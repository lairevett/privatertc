import React from 'react';

const MessageErrorIcon = () => (
    <span className="material-icons text-danger" style={{fontSize: '2rem'}}>
        error
    </span>
);

export default MessageErrorIcon;
