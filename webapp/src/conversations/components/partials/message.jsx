import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import SlackMessage from '../../../app/components/partials/slack_message';
import {debounce} from '../../../app/utils/events';
import {isOnSameDayAsToday} from '../../../app/utils/dates';
import {useCurrentUser} from '../../../auth/utils/hooks';
import MessageAttachmentListModalShowButton from './message_attachment_list_modal_show_button';
import MessageSpinnerIcon from './message_spinner_icon';
import MessageErrorIcon from './message_error_icon';

const Message = ({tabIndex, id, sender, content, has_attachments, date, hasSpinner, hasError}) => {
    const sendDateDivReference = useRef();
    const dateObject = date ? new Date(date) : null;
    const isSameDay = dateObject && isOnSameDayAsToday(dateObject);

    const currentUser = useCurrentUser();
    const isServerMessage = sender === 'Server';
    const isOutgoingMessage = !isServerMessage && sender === currentUser.id;
    const messageType = isOutgoingMessage ? 'outgoing' : isServerMessage ? 'server' : 'incoming';

    const hideSendDate = debounce(() => {
        const {current} = sendDateDivReference;

        if (current?.classList?.contains('shown')) {
            current.classList.remove('shown');
        }
    }, 250);

    const triggerSendDate = debounce(evnt => {
        const {current} = sendDateDivReference;

        if (current) {
            const keyCode = evnt.which || evnt.keyCode || evnt.charCode || 0;
            const isEnterClicked = evnt.type === 'keypress' && keyCode === 13;

            if (current.classList.contains('shown')) {
                if (isEnterClicked) {
                    // Don't blur when user is navigating with keyboard.
                    hideSendDate();
                    return;
                }

                evnt.currentTarget.blur();
                return;
            }

            if (evnt.type === 'click' || isEnterClicked) {
                current.classList.add('shown');
            }
        }
    }, 250);

    return (
        <li className={`list-group-item p-0 mb-2 chat-message ${messageType}`}>
            <div
                data-id={id}
                tabIndex={tabIndex}
                role="button"
                className={messageType}
                onClick={triggerSendDate}
                onKeyPress={triggerSendDate}
                onBlur={hideSendDate}
            >
                <SlackMessage content={content} />
            </div>
            {has_attachments && <MessageAttachmentListModalShowButton messageId={id} />}
            {hasSpinner && <MessageSpinnerIcon />}
            {hasError && <MessageErrorIcon />}
            {dateObject && (
                <div ref={sendDateDivReference} className="message-send-date w-100 text-muted">
                    {isSameDay ? dateObject.toLocaleTimeString() : dateObject.toLocaleString()}
                </div>
            )}
        </li>
    );
};

Message.propTypes = {
    tabIndex: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]).isRequired,
    id: PropTypes.string,
    sender: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    has_attachments: PropTypes.bool.isRequired,
    date: PropTypes.string,
    hasSpinner: PropTypes.bool,
    hasError: PropTypes.bool,
};

Message.defaultProps = {
    id: '',
    date: '',
    hasSpinner: false,
    hasError: false,
};

export default Message;
