/* eslint-disable jsx-a11y/media-has-caption, no-param-reassign, security/detect-object-injection, react/no-array-index-key */
import React, {useEffect, useState, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import deepEqual from 'deep-equal';
import {setUpVideoNode} from '../../../app/utils/dom';
import {STATUS as CALL_STATUS} from '../../../calls/constants';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../../../groups/constants';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../../../users/constants';
import {getVideoStreams as wrtcGetVideoStreams} from '../../../wrtc/actions';
import {useVideoNodeDenominators, useVideoNodesSizeUpdate, useParticipantsInfo} from '../../utils/hooks';
import ConversationCallBar from './conversation_call_bar';

const ConversationCallBox = ({selectedEntity, isToGroup}) => {
    const videoNodesReference = useRef();
    const dispatch = useDispatch();

    const activeCallStatus = useSelector(state => state.calls.active.status);
    const participantsInfo = useParticipantsInfo(selectedEntity, isToGroup);

    const activeCallVideoSenders = useSelector(state => state.calls.active.videoSenders, deepEqual);
    const [videoStreams, setVideoStreams] = useState([]);

    useEffect(() => {
        setVideoStreams(dispatch(wrtcGetVideoStreams(activeCallVideoSenders)));
    }, [dispatch, activeCallVideoSenders]);

    const videoNodesContainerSize = useVideoNodesSizeUpdate(videoNodesReference);

    const [videoNodeWidthDenominator, videoNodeHeightDenominator] = useVideoNodeDenominators(
        videoNodesReference,
        videoNodesContainerSize,
        participantsInfo.length
    );

    return (
        <div className="d-flex flex-column align-items-between h-100">
            <div ref={videoNodesReference} id="video-nodes" className="h-100">
                {activeCallStatus === CALL_STATUS.CONNECTED && participantsInfo.length > 0 ? (
                    <div className="d-flex flex-wrap align-content-start justify-content-center h-100">
                        {participantsInfo.map(([wsChannelName, displayName]) => {
                            const stream = videoStreams[wsChannelName];

                            if (stream) {
                                return (
                                    <video
                                        key={wsChannelName}
                                        ref={node =>
                                            setUpVideoNode(
                                                node,
                                                videoNodesContainerSize,
                                                videoNodeWidthDenominator,
                                                videoNodeHeightDenominator,
                                                stream
                                            )
                                        }
                                        className="video p-0"
                                        autoPlay
                                    />
                                );
                            }

                            return (
                                <div
                                    key={wsChannelName}
                                    ref={node =>
                                        setUpVideoNode(
                                            node,
                                            videoNodesContainerSize,
                                            videoNodeWidthDenominator,
                                            videoNodeHeightDenominator
                                        )
                                    }
                                    className="video d-flex align-items-center justify-content-center flex-column p-3"
                                >
                                    <span className="material-icons text-muted user-icon">videocam_off</span>
                                    {isToGroup && <span className="text-center text-muted">{displayName}</span>}
                                </div>
                            );
                        })}
                    </div>
                ) : (
                    <div className="d-flex h-100 justify-content-center align-items-center">
                        <span className="display-1 material-icons text-muted">phone_in_talk</span>
                    </div>
                )}
            </div>
            <div className="border-top">
                <ConversationCallBar />
            </div>
        </div>
    );
};

ConversationCallBox.propTypes = {
    selectedEntity: PropTypes.oneOfType([
        USER_DETAILS_PROP_TYPE(PropTypes).isRequired,
        GROUP_DETAILS_PROP_TYPE(PropTypes).isRequired,
    ]).isRequired,
    isToGroup: PropTypes.bool.isRequired,
};

export default ConversationCallBox;
