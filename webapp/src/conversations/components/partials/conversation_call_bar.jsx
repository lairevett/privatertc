import React, {useEffect, useRef} from 'react';
import {useSelector, shallowEqual} from 'react-redux';
import {convertMillisecondsToElapsedTime} from '../../../app/utils/dates';
import {
    TYPE as CALL_TYPE,
    STATUS as CALL_STATUS,
    HUMAN_READABLE_STATUS as CALL_HUMAN_READABLE_STATUS,
} from '../../../calls/constants';
import AnswerForm from './answer_form';
import HangUpForm from './hang_up_form';
import TriggerMicButton from './trigger_mic_button';
import TriggerCameraButton from './trigger_camera_button';

const ConversationCallBar = () => {
    const {status, id, participants, startedAt} = useSelector(state => state.calls.active, shallowEqual);
    const mappedStatus = CALL_HUMAN_READABLE_STATUS.get(status);
    const callElapsedReference = useRef();

    useEffect(() => {
        let now = Date.now();
        const lastStatusChange = new Date(startedAt).getTime();
        const {current} = callElapsedReference;

        const updateCallElapsedInterval = setInterval(() => {
            now += 1000;

            if (current) {
                const deltaMilliseconds = now - lastStatusChange;
                // eslint-disable-next-line xss/no-mixed-html
                current.innerHTML = convertMillisecondsToElapsedTime(deltaMilliseconds);
            }
        }, 1000);

        return () => {
            clearInterval(updateCallElapsedInterval);
        };
    }, [startedAt]);

    return (
        <>
            <div className={`alert alert-${mappedStatus.alertColor} p-1 mb-1 d-flex justify-content-between`}>
                <div className="ml-2">{mappedStatus.statusString}</div>
                <div className="mr-2" ref={callElapsedReference}>
                    00:00
                </div>
            </div>
            <div className="mb-2 pt-2 d-flex justify-content-around align-items-center">
                {status === CALL_STATUS.INCOMING ? (
                    <>
                        <AnswerForm id={id} participants={participants} type={CALL_TYPE.AUDIO} />
                        <AnswerForm id={id} participants={participants} type={CALL_TYPE.AUDIO_AND_VIDEO} />
                        <HangUpForm />
                    </>
                ) : status === CALL_STATUS.OUTGOING ? (
                    <HangUpForm />
                ) : (
                    <>
                        <TriggerMicButton />
                        <HangUpForm redirectToDetails />
                        <TriggerCameraButton />
                    </>
                )}
            </div>
        </>
    );
};

export default ConversationCallBar;
