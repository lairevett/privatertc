import React from 'react';
import Spinner from '../../../app/components/partials/spinner';

const MessageSpinnerIcon = () => <Spinner size="2rem" />;

export default MessageSpinnerIcon;
