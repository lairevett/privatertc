import React, {useCallback, useEffect, useRef, useMemo} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {usePageIterator} from '../../../app/utils/api/hooks';
import {makeContext as makeAxiosContext} from '../../../app/utils/api/requests';
import {debounce} from '../../../app/utils/events';
import {MESSAGES_RESULTS_PROP_TYPE} from '../../constants';
import {clearMessagesSearchMatchingIds, retrieveMessages} from '../../actions';
import {search} from '../../utils/messages';

const MessageSearchInput = ({conversationId, results}) => {
    const inputReference = useRef();
    const dispatch = useDispatch();
    const messagesNext = useSelector(state => state.conversations.messages.next);
    const pageIterator = usePageIterator(messagesNext);

    const context = useMemo(makeAxiosContext, []);
    const actionCallback = useCallback(nextPage => retrieveMessages(conversationId, nextPage)(context), [
        conversationId,
        context,
    ]);

    const onResponse = useCallback(
        keyword => response => {
            const {payload} = dispatch(response);
            search(dispatch, payload?.results ?? [], keyword);
        },
        [dispatch]
    );

    const handleChange = debounce(evnt => {
        dispatch(clearMessagesSearchMatchingIds());

        if (evnt.target.value.trim().length > 0) {
            search(dispatch, results, evnt.target.value); // Search already fetched messages.

            if (pageIterator) {
                // Fetch and search next pages if there are any.
                pageIterator.start(actionCallback, onResponse(evnt.target.value), 500);
            }
        } else if (pageIterator) {
            pageIterator.stop();
        }
    }, 250);

    useEffect(() => {
        return () => {
            if (pageIterator?.stop()) {
                context.axios.cancel();
                dispatch(clearMessagesSearchMatchingIds());
            }

            const {current} = inputReference;
            if (current) {
                current.value = '';
            }
        };
    }, [context, conversationId, pageIterator, dispatch]);

    return (
        <span className="bmd-form-group w-100 pt-0 ml-1">
            <input
                ref={inputReference}
                className="form-control bg-img-none font-size-inherit w-100 pt-0 pb-0"
                type="text"
                placeholder="Enter search keyword..."
                onChange={handleChange}
            />
        </span>
    );
};

MessageSearchInput.propTypes = {
    conversationId: PropTypes.string.isRequired,
    results: MESSAGES_RESULTS_PROP_TYPE(PropTypes).isRequired,
};

export default MessageSearchInput;
