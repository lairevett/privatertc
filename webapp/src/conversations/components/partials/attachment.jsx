import React from 'react';
import PropTypes from 'prop-types';
import TableListRow from '../../../app/components/partials/table_list_row';
import AttachmentIcon from './attachment_icon';
import AttachmentName from './attachment_name';
import AttachmentDownloadButton from './attachment_download_button';
import AttachmentRemoveButton from './attachment_remove_button';

const Attachment = ({name, url}) => (
    <TableListRow appendClassName="align-items-center">
        <AttachmentIcon name={name} />
        <AttachmentName name={name} />
        {url ? <AttachmentDownloadButton url={url} /> : <AttachmentRemoveButton name={name} />}
    </TableListRow>
);

Attachment.propTypes = {
    name: PropTypes.string.isRequired,
    url: PropTypes.string,
};

Attachment.defaultProps = {
    url: null,
};

export default Attachment;
