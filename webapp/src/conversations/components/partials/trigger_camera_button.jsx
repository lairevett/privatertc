import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../../../app/components/partials/button';
import {throttle} from '../../../app/utils/events';
import {TYPE as CALL_TYPE} from '../../../calls/constants';
import {triggerVideoTrackVisibility as wrtcTriggerVideoTrackVisibility} from '../../../wrtc/actions';

const TriggerCameraButton = () => {
    const dispatch = useDispatch();
    const callType = useSelector(state => state.calls.active.type);
    const isVideoTrackEnabled = callType === CALL_TYPE.AUDIO_AND_VIDEO;
    const buttonColorClass = isVideoTrackEnabled ? 'btn-primary' : 'btn-danger';
    const handleOnClick = throttle(() => dispatch(wrtcTriggerVideoTrackVisibility()), 500);

    return (
        <Button colorClassName={buttonColorClass} appendClassName="bmd-btn-fab" onClick={handleOnClick}>
            <span className="material-icons">{isVideoTrackEnabled ? 'videocam' : 'videocam_off'}</span>
        </Button>
    );
};

export default TriggerCameraButton;
