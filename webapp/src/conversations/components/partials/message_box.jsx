import React, {memo, useEffect, useRef} from 'react';
import {useSelector} from 'react-redux';
import deepEqual from 'deep-equal';
import PropTypes from 'prop-types';
import PaginatedTableList from '../../../app/components/partials/paginated_table_list';
import {SCROLL_DIRECTION} from '../../../app/utils/api/constants';
import {scrollToBottom} from '../../../app/utils/dom';
import {MESSAGES_RESULTS_PROP_TYPE} from '../../constants';
import {useChatScrollDownOnNewMessage} from '../../utils/hooks';
import {retrieveMessages} from '../../actions';
import Message from './message';
import SendingMessage from './sending_message';
import AttachmentList from './attachment_list';

const MessageBox = ({conversationId, results, messagesSearchVisible, messagesSearchReference}) => {
    const reference = useRef();
    const autoClickMessageId = useSelector(state => state.conversations.autoClickMessageId);
    const nextPageLink = useSelector(state => state.conversations.messages.next);
    const sendingMessages = useSelector(state => state.conversations.sendingMessages, deepEqual);
    const attachments = useSelector(state => state.form?.conversationsMessage?.values?.attachments, deepEqual);

    useChatScrollDownOnNewMessage(reference);

    useEffect(() => {
        if (reference.current) {
            scrollToBottom(reference.current);
        }
    }, []);

    useEffect(() => {
        if (reference.current) {
            // Block scrolling, since appending to the message box breaks it.
            const toggledOn = reference.current.classList.toggle('paginated-hidden', messagesSearchVisible);
            if (toggledOn) {
                // Fixes firefox's auto-scrolling to top on container prepend behavior.
                reference.current.scrollTo(0, reference.current.scrollTop);
            }
        }
    }, [messagesSearchVisible]);

    useEffect(() => {
        const message = reference.current.querySelector(`[data-id="${autoClickMessageId}"]`);

        if (message) {
            message.scrollIntoView();
            let additionalScrollMargin = 24; // Arbitrary value.
            // Search box has position: absolute, include its height in scrollIntoView().
            const searchBoxHeight = messagesSearchReference?.current?.clientHeight ?? 0;
            additionalScrollMargin += searchBoxHeight;
            reference.current.scrollTo(0, reference.current.scrollTop - additionalScrollMargin);

            message.click();
            message.focus();
        }
    }, [autoClickMessageId, reference, messagesSearchReference]);

    return (
        <PaginatedTableList
            ref={reference}
            scrollDirection={SCROLL_DIRECTION.TOP}
            nextPageLink={nextPageLink}
            actionCallback={nextPage => retrieveMessages(conversationId, nextPage)}
            appendClassName="list-group bmd-list-group-sm chat-messages pt-3 pl-3 pr-3 break-word"
        >
            {results.length > 0 ? (
                <>
                    {results.map((result, i) => (
                        <Message key={result.id} tabIndex="-1" {...result} />
                    ))}
                </>
            ) : (
                <Message tabIndex="-1" sender="Server" content="Conversation started." />
            )}
            {sendingMessages.map(result => (
                <SendingMessage key={result.id} {...result} />
            ))}
            {attachments && <AttachmentList appendClassName="mxw-75 align-self-end" attachments={attachments} />}
        </PaginatedTableList>
    );
};

MessageBox.propTypes = {
    conversationId: PropTypes.string,
    results: MESSAGES_RESULTS_PROP_TYPE(PropTypes).isRequired,
    messagesSearchVisible: PropTypes.bool.isRequired,
    messagesSearchReference: PropTypes.shape({current: PropTypes.instanceOf(Element)}).isRequired,
};

MessageBox.defaultProps = {
    conversationId: '',
};

export default memo(MessageBox);
