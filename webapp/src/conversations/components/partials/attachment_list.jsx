import React from 'react';
import PropTypes from 'prop-types';
import TableList from '../../../app/components/partials/table_list';
import {MESSAGES_RESULTS_ATTACHMENT_LIST_PROP_TYPE} from '../../constants';
import Attachment from './attachment';

const AttachmentList = ({attachments, ...properties}) => (
    <TableList {...properties}>
        {attachments.map(file => (
            <Attachment key={file.name} name={file.name} url={file?.url} />
        ))}
    </TableList>
);

AttachmentList.propTypes = {
    attachments: MESSAGES_RESULTS_ATTACHMENT_LIST_PROP_TYPE(PropTypes).isRequired,
};

export default AttachmentList;
