import React from 'react';
import PropTypes from 'prop-types';
import {Field, propTypes, reduxForm} from 'redux-form';
import {FormField} from '../../../app/components/partials/form';
import SubmitButton from '../../../app/components/partials/submit_button';
import Button from '../../../app/components/partials/button';
import {validate, submit} from '../../utils/forms/process_search';
import {MODULE_NAME, MESSAGES_RESULTS_PROP_TYPE} from '../../constants';

const SearchForm = ({handleSubmit, valid, pristine, submitting, pageIterator}) => (
    <div className="border-bottom pl-3">
        <form onSubmit={handleSubmit(submit)}>
            <div className="d-flex flex-row">
                <Field
                    component={FormField}
                    id="keyword"
                    name="keyword"
                    type="text"
                    label="Keyword"
                    formGroupClassNames="w-100 mr-2"
                />
                <SubmitButton value="Search" disabled={!valid || pristine || submitting} />
                <Button type="reset" value="Stop" disabled={!submitting} onClick={pageIterator?.stop} />
            </div>
        </form>
    </div>
);

SearchForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    conversationId: PropTypes.string.isRequired, // Used in submit().
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    messagesResults: MESSAGES_RESULTS_PROP_TYPE(PropTypes).isRequired, // Used in submit().
    pageIterator: PropTypes.shape({start: PropTypes.func.isRequired, stop: PropTypes.func.isRequired}).isRequired, // Used in submit().
};

export default reduxForm({form: `${MODULE_NAME}Search`, validate})(SearchForm);
