import {DEFAULT_LIST_INITIAL_STATE, DEFAULT_META_INITIAL_STATE, META_PROP_TYPE} from '../app/utils/api/constants';
import {DETAILS_PROP_TYPE as GROUP_DETAILS_PROP_TYPE} from '../groups/constants';
import {DETAILS_PROP_TYPE as USER_DETAILS_PROP_TYPE} from '../users/constants';

export const MODULE_NAME = 'conversations';

export const ENDPOINT = {
    LIST: page => `/v1/${MODULE_NAME}/?page=${page}`,
    SEARCH: keyword => `/v1/${MODULE_NAME}/?search=${keyword}`,
    DETAILS: conversationId => `/v1/${MODULE_NAME}/${conversationId}/`,
    DETAILS_TRIGGER_BLOCK: conversationId => `/v1/${MODULE_NAME}/${conversationId}/trigger_block/`,
    DETAILS_USER_IDS: conversationId => `/v1/${MODULE_NAME}/${conversationId}/user_ids/`,
    CALLS_OFFER_INFO: callId => `/v1/${MODULE_NAME}/call_offer_info/?id=${callId}`,
    MESSAGES: conversationId => `/v1/${MODULE_NAME}/${conversationId}/messages/`,
    MESSAGES_LIST: (conversationId, page) => `/v1/${MODULE_NAME}/${conversationId}/messages/?page=${page}`,
    MESSAGES_ATTACHMENTS_LIST: (conversationId, messageId) =>
        `/v1/${MODULE_NAME}/${conversationId}/attachments/?message_id=${messageId}`,
    MESSAGES_SUPPLEMENTARY_DATA: (conversationId, messageIndex) =>
        `/v1/${MODULE_NAME}/${conversationId}/supplementary_message_data/?message_index=${messageIndex}`,
    MESSAGES_EXPORT: conversationId => `/v1/${MODULE_NAME}/${conversationId}/messages_export/`,
};

export const ACTION = {
    RETRIEVE_LIST: `@${MODULE_NAME}/RETRIEVE_LIST`,
    SEARCH_LIST: `@${MODULE_NAME}/SEARCH_LIST`,
    RETRIEVE_LIST_APPEND: `@${MODULE_NAME}/RETRIEVE_LIST_APPEND`,
    PREPEND_RESULT: `@${MODULE_NAME}/PREPEND_RESULT`,
    REMOVE_RESULT: `@${MODULE_NAME}/REMOVE_RESULT`,
    UPDATE_LIST_META_STATUS: `@${MODULE_NAME}/UPDATE_LIST_META_STATUS`,
    RETRIEVE_DETAILS: `@${MODULE_NAME}/RETRIEVE_DETAILS`,
    UPDATE_IS_MUTED: `@${MODULE_NAME}/UPDATE_IS_MUTED`,
    TRIGGER_BLOCK: `@${MODULE_NAME}/TRIGGER_BLOCK`,
    QUERY_DETAILS: `@${MODULE_NAME}/QUERY_DETAILS`,
    CLEAR_DETAILS: `@${MODULE_NAME}/CLEAR_DETAILS`,
    QUERY_USER_IDS: `@${MODULE_NAME}/QUERY_USER_IDS`,
    DESTROY: `@${MODULE_NAME}/DESTROY`,
    UPDATE_USER_ONLINE_STATUS: `@${MODULE_NAME}/UPDATE_USER_ONLINE_STATUS`,
    UPDATE_ONLINE_USER_IDS: `@${MODULE_NAME}/UPDATE_ONLINE_USER_IDS`,
    UPDATE_GROUP_ONLINE_STATUS: `@${MODULE_NAME}/UPDATE_GROUP_ONLINE_STATUS`,
    REMOVE_GROUP_MEMBER: `@${MODULE_NAME}/REMOVE_GROUP_MEMBER`,
    QUERY_CALL_OFFER_INFO: `@${MODULE_NAME}/QUERY_CALL_OFFER_INFO`,
    CREATE_MESSAGE: `@${MODULE_NAME}/CREATE_MESSAGE`,
    RETRIEVE_MESSAGES: `@${MODULE_NAME}/RETRIEVE_MESSAGES`,
    RETRIEVE_MESSAGES_PREPEND: `@${MODULE_NAME}/RETRIEVE_MESSAGES_PREPEND`,
    RETRIEVE_MESSAGES_ATTACHMENTS: `@${MODULE_NAME}/RETRIEVE_MESSAGES_ATTACHMENTS`,
    CLEAR_MESSAGES_ATTACHMENTS: `@${MODULE_NAME}/CLEAR_MESSAGES_ATTACHMENTS`,
    QUERY_SUPPLEMENTARY_MESSAGE_DATA: `@${MODULE_NAME}/QUERY_SUPPLEMENTARY_MESSAGE_DATA`,
    AUTO_CLICK_MESSAGE: `@${MODULE_NAME}/AUTO_CLICK_MESSAGE`,
    ADD_SENDING_MESSAGE: `@${MODULE_NAME}/ADD_SENDING_MESSAGE`,
    REMOVE_SENDING_MESSAGE: `@${MODULE_NAME}/REMOVE_SENDING_MESSAGE`,
    PUSH_MESSAGE: `@${MODULE_NAME}/PUSH_MESSAGE`,
    SET_LAST_MESSAGE: `@${MODULE_NAME}/SET_LAST_MESSAGE`,
    DESTROY_MESSAGES: `@${MODULE_NAME}/DESTROY_MESSAGES`,
    CLEAR_MESSAGES: `@${MODULE_NAME}/CLEAR_MESSAGES`,
    TRIGGER_MESSAGES_SEARCH_VISIBLE: `@${MODULE_NAME}/TRIGGER_MESSAGES_SEARCH_VISIBLE`,
    APPEND_MESSAGES_SEARCH_MATCHING_IDS: `@${MODULE_NAME}/APPEND_MESSAGES_SEARCH_MATCHING_IDS`,
    CLEAR_MESSAGES_SEARCH_MATCHING_IDS: `@${MODULE_NAME}/CLEAR_MESSAGES_SEARCH_MATCHING_IDS`,
};

export const LIST_INITIAL_STATE = {...DEFAULT_LIST_INITIAL_STATE, resultsIds: []};

export const DETAILS_INITIAL_STATE = {
    id: '',
    to_user: null,
    to_group: null,
    online_user_ids: [],
    active_call: null,
    is_muted: false,
    blockage: null,
    ...DEFAULT_META_INITIAL_STATE,
};

export const MESSAGES_INITIAL_STATE = {...DEFAULT_LIST_INITIAL_STATE};

export const MESSAGES_ATTACHMENTS_INITIAL_STATE = {results: [], ...DEFAULT_META_INITIAL_STATE};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
    messages: MESSAGES_INITIAL_STATE,
    sendingMessages: [],
    messagesAttachments: MESSAGES_ATTACHMENTS_INITIAL_STATE,
    autoClickedMessageId: '',
    messagesSearch: {
        visible: false,
        matchingIds: [],
    },
};

export const DETAILS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        id: PropertyTypes.string.isRequired,
        to_user: USER_DETAILS_PROP_TYPE(PropertyTypes),
        to_group: GROUP_DETAILS_PROP_TYPE(PropertyTypes),
        online_user_ids: PropertyTypes.arrayOf(PropertyTypes.string.isRequired),
        active_call: PropertyTypes.string,
        is_muted: PropertyTypes.bool.isRequired,
        blockage: PropertyTypes.shape({local: PropertyTypes.bool.isRequired, remote: PropertyTypes.bool.isRequired}),
        __meta__: META_PROP_TYPE(PropertyTypes),
    });

export const MESSAGES_RESULTS_ATTACHMENT_DETAILS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        name: PropertyTypes.string.isRequired,
        size: PropertyTypes.number.isRequired,
        url: PropertyTypes.string, // API-only.
    });

export const MESSAGES_RESULTS_ATTACHMENT_LIST_PROP_TYPE = PropertyTypes =>
    PropertyTypes.arrayOf(MESSAGES_RESULTS_ATTACHMENT_DETAILS_PROP_TYPE(PropertyTypes).isRequired);

export const LIST_RESULTS_PROP_TYPE = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPE(PropertyTypes));

export const MESSAGES_RESULTS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.arrayOf(
        PropertyTypes.shape({
            id: PropertyTypes.string.isRequired,
            sender: PropertyTypes.string.isRequired,
            content: PropertyTypes.string.isRequired,
            has_attachments: PropertyTypes.bool.isRequired,
            date: PropertyTypes.string,
        })
    );

export const MODAL = {
    GROUP_INFO: `${MODULE_NAME}_MODAL_GROUP_INFO`,
    LEAVE_GROUP: `${MODULE_NAME}_MODAL_LEAVE_GROUP`,
    CLEAR_CHAT: `${MODULE_NAME}_MODAL_CLEAR_CHAT`,
    MESSAGE_ATTACHMENT_LIST: `${MODULE_NAME}_MODAL_MESSAGE_ATTACHMENT_LIST`,
};

export const MEMO_API_QUERY = {
    USER_IDS: 'conversation_user_ids',
};

export const MESSAGE_QUEUE = {
    API: 'MESSAGE_QUEUE_API',
    WEBRTC: 'MESSAGE_QUEUE_WEBRTC',
};

export const MESSAGE_ATTACHMENT_TYPE = {
    IMAGE: 'MESSAGE_ATTACHMENT_TYPE_IMAGE',
    AUDIO: 'MESSAGE_ATTACHMENT_TYPE_AUDIO',
    VIDEO: 'MESSAGE_ATTACHMENT_TYPE_VIDEO',
    OTHER: 'MESSAGE_ATTACHMENT_TYPE_OTHER',
};
