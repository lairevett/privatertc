import {NAV_HEADER} from '../app/constants';
import {registerNavSearchFunction, registerNavHeaderComponent, registerNavDropdownComponent} from '../app/utils/nav';
import NavAudioCall from './components/partials/nav_audio_call';
import NavAudioAndVideoCall from './components/partials/nav_audio_and_video_call';
import NavDropdownDetailsContent from './components/partials/nav_dropdown_details_content';
import {MODULE_NAME} from './constants';
import {searchList} from './actions';

registerNavSearchFunction(`${MODULE_NAME}:list`, searchList);

[`${MODULE_NAME}:details`].forEach(viewName => {
    registerNavHeaderComponent(viewName, NavAudioCall, NAV_HEADER.RIGHT);
    registerNavHeaderComponent(viewName, NavAudioAndVideoCall, NAV_HEADER.RIGHT);
    registerNavDropdownComponent(viewName, NavDropdownDetailsContent);
});
