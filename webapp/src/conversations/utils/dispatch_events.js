import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {
    retrieveList,
    retrieveDetails,
    clearDetails,
    retrieveMessages,
    clearMessages,
    autoClickMessage,
} from '../actions';

export const listCallbackGetDispatchEvents = () => () => makeDispatchEvents([retrieveList(1)]);

export const detailsCallbackGetDispatchEvents = () => ({conversationId}) =>
    makeDispatchEvents(
        [retrieveList(1), retrieveDetails(conversationId), retrieveMessages(conversationId, 1)],
        [clearDetails, () => clearMessages(true), () => autoClickMessage(null)],
        [conversationId]
    );
