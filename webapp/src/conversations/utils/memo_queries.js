import {memoizedQuery} from '../../app/utils/api/memo';
import {MEMO_API_QUERY} from '../constants';
import {queryUserIds} from '../actions';

export const memoizedQueryConversationUserIds = async (dispatch, conversationId) =>
    memoizedQuery(dispatch, conversationId, MEMO_API_QUERY.USER_IDS, queryUserIds(conversationId));
