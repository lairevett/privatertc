import {updateIsMuted} from '../../actions';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';

export const submit = async (_, dispatch, {conversationId, currentIsMuted}) => {
    const {payload} = dispatch(await updateIsMuted(conversationId, !currentIsMuted)());
    apiCheckSubmitErrors(payload);
    // TODO: web workers
};
