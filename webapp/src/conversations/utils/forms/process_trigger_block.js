import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {triggerBlock} from '../../actions';

export const submit = async (_, dispatch, {conversationId}) => {
    const {payload} = dispatch(await triggerBlock(conversationId)());
    apiCheckSubmitErrors(payload);
};
