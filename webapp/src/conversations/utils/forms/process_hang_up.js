import {checkSubmitErrors as wsCheckSubmitErrors} from '../../../app/utils/ws/redux_form';
import {tearDownCall as wrtcTearDownCall} from '../../../wrtc/actions';
import {sendRequest as wsSendRequest} from '../../../ws/actions';
import {makeHangUpCallMessage} from '../../../ws/utils/factory';

export const submit = async (_, dispatch, {redirectToDetails}) => {
    const message = makeHangUpCallMessage();
    await wsCheckSubmitErrors(() => dispatch(wsSendRequest(message)));
    dispatch(wrtcTearDownCall(redirectToDetails));
};
