import {SubmissionError} from 'redux-form';
import {updateActive as callsUpdateActive} from '../../../calls/actions';
import {obtainMediaContext as wrtcObtainMediaContext, setUpCall as wrtcSetUpCall} from '../../../wrtc/actions';
import {sendMessage as wsSendMessage} from '../../../ws/actions';
import {makeAnswerCallMessage} from '../../../ws/utils/factory';

export const submit = async (_, dispatch, {id, participants, type}) => {
    const succeeded = await dispatch(wrtcObtainMediaContext(type));
    if (!succeeded) {
        throw new SubmissionError({_error: 'Microphone permissions were not granted.'});
    }

    const participantsEntries = Object.entries(participants);
    const lastParticipantEntryIndex = participantsEntries.length - 1;
    dispatch(callsUpdateActive({participants}));

    const setupResults = [];

    participantsEntries.forEach(([userId, userChannelNames], participantEntryIndex) => {
        const lastUserChannelNameIndex = userChannelNames.length - 1;

        userChannelNames.forEach((userChannelName, channelUserNameIndex) => {
            const isFinished =
                participantEntryIndex === lastParticipantEntryIndex &&
                channelUserNameIndex === lastUserChannelNameIndex;

            const result = dispatch(wrtcSetUpCall(userId, userChannelName, isFinished));
            setupResults.push(result);
        });
    });

    if ((await Promise.all(setupResults)).every(result => !!result)) {
        const message = makeAnswerCallMessage(id);
        dispatch(wsSendMessage(message));
        // TODO: redirect back when call does not exist anymore
    } else {
        throw new SubmissionError({_error: 'Something went wrong while trying to set up the call.'});
    }
};
