import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {MODAL} from '../../constants';
import {destroy} from '../../actions';

export const submit = async (_, dispatch, {conversationId}) => {
    const {payload} = dispatch(await destroy(conversationId)());
    apiCheckSubmitErrors(payload);
    window.$(`#${MODAL.LEAVE_GROUP}`).modal('hide');
};
