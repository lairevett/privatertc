import {v4 as uuidv4} from 'uuid';
import {reset, SubmissionError} from 'redux-form';
import {validateFieldRequired} from '../../../app/utils/validation';
import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {sendToDataChannels as wrtcSendToDataChannels} from '../../../wrtc/actions';
import {addSendingMessage, createMessage, removeSendingMessage} from '../../actions';
import {makeMessageContext, makeConversationContext} from '../factory';

const parseAttachments = attachments => attachments?.map(({instance}) => ({file: instance}));

export const validate = ({content}) => {
    const errors = {};
    errors.content = validateFieldRequired(content);
    return errors;
};

export const submit = ({content, attachments}, dispatch, {conversation, isConversationEmpty, recipients}) => {
    const isConversation = !!conversation;
    const messageContext = makeMessageContext(content, false);
    const conversationContext = makeConversationContext(recipients, conversation, isConversationEmpty);

    const sendingMessageId = uuidv4();
    dispatch(addSendingMessage(sendingMessageId, content));

    (async () => {
        try {
            if (isConversation) {
                const parsedAttachments = parseAttachments(attachments);

                const {payload} = dispatch(
                    await createMessage(conversation.id, content, parsedAttachments)({multiPartFields: [attachments]})
                );

                apiCheckSubmitErrors(payload);

                const attachmentsRequested = attachments?.length > 0;
                if (attachmentsRequested) {
                    const {__meta__, attachments: actualAttachments, ...message} = payload;
                    messageContext.actualMessage = {...message, has_attachments: actualAttachments.length > 0};
                    messageContext.useActualMessage = true;
                }
            }
        } catch (error) {
            if (error instanceof SubmissionError) {
                messageContext.isError = true;
                return;
            }

            console.trace(error);
        } finally {
            dispatch(removeSendingMessage(sendingMessageId));

            // Also pushes message locally, into redux conversation.messages.
            // Doesn't send to remote data channels if messageContext.isError.
            // TODO: decouple local message push from wrtc middleware, it doesn't really belong there.
            // TODO: use django's createMessage post_save signal instead of webrtc data channel.
            dispatch(wrtcSendToDataChannels(messageContext, conversationContext));
        }
    })();

    dispatch(reset('conversationsMessage'));
};
