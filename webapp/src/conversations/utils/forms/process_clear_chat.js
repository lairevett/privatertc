import {checkSubmitErrors as apiCheckSubmitErrors} from '../../../app/utils/api/redux_form';
import {MODAL} from '../../constants';
import {destroyMessages, removeResult, updateListMetaStatus, clearMessages} from '../../actions';

export const submit = async (_, dispatch, {conversationId, removeFromList}) => {
    const {payload} = dispatch(await destroyMessages(conversationId)());
    apiCheckSubmitErrors(payload);

    if (removeFromList) {
        dispatch(removeResult(conversationId));
        dispatch(updateListMetaStatus());
    }

    dispatch(clearMessages(false));
    window.$(`#${MODAL.CLEAR_CHAT}`).modal('hide');
};
