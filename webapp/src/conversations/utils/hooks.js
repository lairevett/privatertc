import {useEffect, useState, useCallback} from 'react';
import {useSelector} from 'react-redux';
import deepEqual from 'deep-equal';
import {getDisplayName} from '../../app/utils/display_name';
import {isScrolledToBottom, scrollToBottom} from '../../app/utils/dom';
import {debounce, throttle} from '../../app/utils/events';
import {findBestProductApproximation} from './calls';

export const useChatScrollDownOnNewMessage = chatMessagesReference => {
    useEffect(() => {
        const {current} = chatMessagesReference;

        const [updateWasAtChatEndBeforeReference, scrollDownReference, observerReference] = (() => {
            if (current) {
                let wasAtChatEndBefore = true;

                const updateWasAtChatEndBefore = debounce(() => {
                    // Use previous value since isScrolledToBottom()
                    // isn't true anymore when a new message arrives or onresize fires.
                    wasAtChatEndBefore = isScrolledToBottom(current);
                }, 125);
                current.addEventListener('scroll', updateWasAtChatEndBefore);

                const scrollDown = throttle(() => {
                    // Scroll down when chat messages block overflows,
                    // for example when virtual keyboard on mobile shows up or screen changes its orientation.
                    if (wasAtChatEndBefore) {
                        scrollToBottom(current);
                    }
                }, 250);
                window.addEventListener('resize', scrollDown);

                const observer = new MutationObserver((mutations, _) => {
                    mutations.forEach(mutation => {
                        // Scroll down when a new message comes from a conversation participant.
                        if (mutation.type === 'childList') {
                            // Check, since it sometimes fires with an empty array.
                            if (mutation.addedNodes.length > 0) {
                                const addedNode = mutation.addedNodes[mutation.addedNodes.length - 1];
                                const newMessageArrived = addedNode && !addedNode.nextSibling; // Ignore retrieveOlderMessages();
                                const isMessageIncoming = addedNode?.firstChild.classList.contains('incoming');
                                const isMessageOutgoing = !isMessageIncoming;

                                // Let the message receiver scroll his conversation freely when a new message arrives.
                                const scrollNeeded =
                                    newMessageArrived &&
                                    ((isMessageIncoming && wasAtChatEndBefore) || isMessageOutgoing);

                                if (scrollNeeded) {
                                    scrollToBottom(current);
                                }
                            }
                        }
                    });
                });
                observer.observe(current, {childList: true});

                return [updateWasAtChatEndBefore, scrollDown, observer];
            }

            return [null, null, null];
        })();

        return () => {
            if (current && updateWasAtChatEndBeforeReference) {
                current.removeEventListener('scroll', updateWasAtChatEndBeforeReference);
            }

            if (scrollDownReference) {
                window.removeEventListener('resize', scrollDownReference);
            }

            if (observerReference) {
                observerReference.disconnect();
            }
        };
    }, [chatMessagesReference]);
};

export const useParticipantsInfo = (selectedEntity, isToGroup) => {
    const [participantsInfo, setParticipantsInfo] = useState([]);
    const participants = useSelector(state => state.calls.active.participants, deepEqual);

    useEffect(() => {
        if (selectedEntity) {
            const nextParticipantsInfo = [];
            Object.entries(participants).forEach(([userId, wsChannelNames]) => {
                const {broadcasted_name} = isToGroup
                    ? selectedEntity.members.find(({id}) => id === userId)
                    : selectedEntity;
                const displayName = getDisplayName({id: userId, broadcasted_name});
                wsChannelNames.forEach(wsChannelName => nextParticipantsInfo.push([wsChannelName, displayName]));
            });
            setParticipantsInfo(nextParticipantsInfo);
        }
    }, [participants, isToGroup, selectedEntity]);

    return participantsInfo;
};

export const useVideoNodesSizeUpdate = videoNodesReference => {
    const [videoNodesContainerSize, setVideoNodesContainerSize] = useState([0, 0]);

    const updateVideoNodesSize = useCallback(() => {
        if (videoNodesReference.current) {
            const {clientWidth, clientHeight} = videoNodesReference.current;
            setVideoNodesContainerSize([clientWidth, clientHeight]);
        }
    }, [videoNodesReference]);

    useEffect(() => {
        updateVideoNodesSize();
        const onResize = debounce(updateVideoNodesSize, 250);
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
        };
    }, [updateVideoNodesSize]);

    return videoNodesContainerSize;
};

export const useVideoNodeDenominators = (videoNodesReference, videoNodesContainerSize, participantsCount) => {
    const [videoNodeWidthDenominator, setVideoNodeWidthDenominator] = useState(1);
    const [videoNodeHeightDenominator, setVideoNodeHeightDenominator] = useState(1);

    useEffect(() => {
        // Check if length != 0, so the denominators won't get set to that value.
        if (videoNodesReference.current && participantsCount > 0) {
            let minDenominator;
            let maxDenominator;

            if (participantsCount === 3) {
                // Prefer 2x2 grids instead of 1x3.
                minDenominator = maxDenominator = 2;
            } else {
                const denominators = findBestProductApproximation(participantsCount, 1, 3);
                minDenominator = Math.min(...denominators);
                maxDenominator = Math.max(...denominators);
            }

            // Prefer horizontal splits more than vertical ones.
            const [width, height] = videoNodesContainerSize;
            const preferVerticalSplits = width > height * 1.5;

            if (preferVerticalSplits) {
                setVideoNodeWidthDenominator(maxDenominator);
                setVideoNodeHeightDenominator(minDenominator);
            } else {
                setVideoNodeWidthDenominator(minDenominator);
                setVideoNodeHeightDenominator(maxDenominator);
            }
        }
    }, [videoNodesReference, videoNodesContainerSize, participantsCount]);

    return [videoNodeWidthDenominator, videoNodeHeightDenominator];
};
