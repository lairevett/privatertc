export const updateUserIds = (userIds, userId, isOnline) => {
    const userIdsSet = new Set(userIds);
    (isOnline ? userIdsSet.add : userIdsSet.delete).bind(userIdsSet)(userId);
    return [...userIdsSet];
};

export const getAttachmentsWithNameFields = (() => {
    const fileNameFromUrlRegex = /(?:[^/])+$/;

    return attachment => {
        const copy = {...attachment};
        [copy.name] = copy.url.match(fileNameFromUrlRegex);
        // Trim trailing 32 uuid characters + an underscore added by the api.
        copy.name = copy.name.slice(33);
        return copy;
    };
})();
