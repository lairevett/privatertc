/* eslint-disable security/detect-object-injection */
import {MESSAGE_QUEUE} from '../constants';

const _storage = new Map([
    [MESSAGE_QUEUE.API, window.localStorage],
    [MESSAGE_QUEUE.WEBRTC, window.sessionStorage],
]);

const _setMessageQueue = (name, value) => {
    try {
        const stringifiedValue = JSON.stringify(value);
        _storage.get(name).setItem(name, stringifiedValue);
    } catch (error) {
        console.debug(error);
    }
};

const _createMessageQueue = name => {
    const queue = {};
    _setMessageQueue(name, queue);
    return queue;
};

const _getCurrentMessageQueueState = name => {
    try {
        const stringifiedQueue = _storage.get(name).getItem(name);

        if (!stringifiedQueue) {
            return _createMessageQueue(name);
        }

        return JSON.parse(stringifiedQueue);
    } catch (error) {
        console.debug(error);
    }

    return _createMessageQueue(name);
};

const _walkThroughMessageQueue = (currentQueueState, keys, callback) => {
    keys.forEach(key => {
        // eslint-disable-next-line no-unused-vars
        const [conversationId, _] = parseKey(key);
        currentQueueState[key].forEach(item => callback(conversationId, item));
    });
};

const _clearMessageQueue = (name, currentQueueState, keys) => {
    // eslint-disable-next-line no-param-reassign
    keys.forEach(key => currentQueueState[key] && delete currentQueueState[key]);
    _setMessageQueue(name, currentQueueState);
};

export const makeKey = (conversationId, userId = '') => [conversationId, userId].join('_');
export const parseKey = key => key.split('_');

export const makeMessageQueueItem = (name, key, content) => ({key, queueName: name, content});

export const pushItemToMessageQueue = item => {
    const {key, queueName, content} = item;

    if (content) {
        const currentQueueState = _getCurrentMessageQueueState(queueName);

        _setMessageQueue(queueName, {
            ...currentQueueState,
            [key]: [...(currentQueueState[key] || []), content],
        });
    }
};

export const queryMessageQueue = (name, userId) => {
    const currentQueueState = _getCurrentMessageQueueState(name);

    const keysWithMessages = Object.keys(currentQueueState).filter(key => {
        // eslint-disable-next-line no-unused-vars
        const [_, qUserId] = parseKey(key);
        return userId === qUserId && currentQueueState[key].length > 0;
    });

    return {currentQueueState, keysWithMessages};
};

export const flushMessageQueue = (name, currentQueueState, keys, callback) => {
    if (callback) {
        _walkThroughMessageQueue(currentQueueState, keys, callback);
    }

    _clearMessageQueue(name, currentQueueState, keys);
};
