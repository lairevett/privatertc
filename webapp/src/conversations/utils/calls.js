let _leaveCallAlertBeforeUnload = null;

export const addLeaveCallAlertBeforeUnload = () => {
    if (!_leaveCallAlertBeforeUnload) {
        _leaveCallAlertBeforeUnload = evnt => {
            // eslint-disable-next-line no-param-reassign
            evnt.returnValue = 'Do you really want to leave this call?';
        };

        window.addEventListener('beforeunload', _leaveCallAlertBeforeUnload);
    }
};

export const removeLeaveCallAlertBeforeUnload = () => {
    if (_leaveCallAlertBeforeUnload) {
        window.removeEventListener('beforeunload', _leaveCallAlertBeforeUnload);
        _leaveCallAlertBeforeUnload = null;
    }
};

export const findBestProductApproximation = (number, minFactor, maxFactor) => {
    const result = {deviation: number, firstFactor: 0, secondFactor: 0};
    let abort = false;

    for (let i = minFactor; i <= maxFactor; i++) {
        if (abort) {
            break;
        }

        for (let j = i; j <= maxFactor; j++) {
            const deviation = i * j - number;

            if (deviation >= 0 && result.deviation > deviation) {
                result.firstFactor = i;
                result.secondFactor = j;
            }

            if (deviation === 0 || i * j > number) {
                abort = true;
                break;
            }
        }
    }

    return [result.firstFactor, result.secondFactor];
};
