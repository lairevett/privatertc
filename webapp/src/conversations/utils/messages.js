import {
    URL_IMAGE_REGEXP_OBJECT,
    URL_VIDEO_REGEXP_OBJECT,
    URL_AUDIO_REGEXP_OBJECT,
    CATCH_ALL_REGEXP_OBJECT,
} from '../../app/utils/regexp';
import {appendMessagesSearchMatchingIds} from '../actions';
import {MESSAGE_ATTACHMENT_TYPE} from '../constants';

export const getSearchResults = (results, keyword) =>
    results
        .filter(message => message.content.includes(keyword))
        .map(({id}) => id)
        .reverse();

export const search = (dispatch, messagesResults, keyword) => {
    const searchResults = getSearchResults(messagesResults, keyword);
    dispatch(appendMessagesSearchMatchingIds(searchResults));
};

export const getAttachmentsWithTypes = (() => {
    const checks = [
        [URL_IMAGE_REGEXP_OBJECT, MESSAGE_ATTACHMENT_TYPE.IMAGE],
        [URL_AUDIO_REGEXP_OBJECT, MESSAGE_ATTACHMENT_TYPE.AUDIO],
        [URL_VIDEO_REGEXP_OBJECT, MESSAGE_ATTACHMENT_TYPE.VIDEO],
        [CATCH_ALL_REGEXP_OBJECT, MESSAGE_ATTACHMENT_TYPE.OTHER],
    ];

    return attachments =>
        attachments.map(attachment => {
            const type = checks.find(([regexp]) => regexp.test(attachment.url))[1];
            return {...attachment, type};
        });
})();
