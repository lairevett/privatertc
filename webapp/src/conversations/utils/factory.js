import {v4 as uuidv4} from 'uuid';

export const makeMessageContext = (content, isZipped) => ({content, isZipped, isError: false});

// TODO: change 'Server' to some constant.
export const makeConversationMessage = (content, sender = 'Server') => ({
    id: uuidv4().replace(/-/g, ''), // Just make some id up for react to use as a key property in render .map().
    date: new Date().toJSON(),
    sender,
    content,
    has_attachments: false,
});

export const makeConversationContext = (recipients, conversation, isConversationEmpty) => ({
    recipients,
    conversation,
    isConversationEmpty,
});

export const makeStrippedConversationContext = (recipients, conversationId = '', groupId = '') => ({
    recipients,
    conversationId,
    groupId,
});

export const stripConversationContext = ({recipients, conversation}) =>
    makeStrippedConversationContext(recipients, conversation?.id, conversation?.to_group?.id);
