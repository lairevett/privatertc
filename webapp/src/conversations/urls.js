import {RequireSignedIn, RequireInCall} from '../app/components/partials/route_access';
import {registerUrl} from '../app/utils/router';
import {listCallbackGetDispatchEvents, detailsCallbackGetDispatchEvents} from './utils/dispatch_events';
import List from './components/list';
import Details from './components/details';
import Call from './components/call';
import {MODULE_NAME} from './constants';

registerUrl(`${MODULE_NAME}:list`, `/${MODULE_NAME}/list`, [RequireSignedIn, List], {
    callbackGetDispatchEvents: listCallbackGetDispatchEvents,
});

registerUrl(`${MODULE_NAME}:details`, `/${MODULE_NAME}/details/:conversationId`, [RequireSignedIn, Details], {
    showBackButton: true,
    hasDynamicTitle: true,
    callbackGetDispatchEvents: detailsCallbackGetDispatchEvents,
});

registerUrl(
    `${MODULE_NAME}:details_call`,
    `/${MODULE_NAME}/details/:conversationId/call`,
    [RequireSignedIn, RequireInCall, Call],
    {
        showBackButton: true,
        hasDynamicTitle: true,
        callbackGetDispatchEvents: detailsCallbackGetDispatchEvents,
    }
);
