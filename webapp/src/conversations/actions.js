import {
    apiGetListRequest,
    apiGetRequest,
    apiPostRequest,
    apiDeleteRequest,
    apiPatchRequest,
} from '../app/utils/api/requests';
import {
    ACTION,
    LIST_INITIAL_STATE,
    ENDPOINT,
    DETAILS_INITIAL_STATE,
    MESSAGES_INITIAL_STATE,
    MESSAGES_ATTACHMENTS_INITIAL_STATE,
} from './constants';

export const retrieveList = (page = 1) =>
    apiGetListRequest(
        page > 1 ? ACTION.RETRIEVE_LIST_APPEND : ACTION.RETRIEVE_LIST,
        LIST_INITIAL_STATE,
        ENDPOINT.LIST(page)
    );

export const searchList = keyword =>
    apiGetListRequest(ACTION.SEARCH_LIST, LIST_INITIAL_STATE, ENDPOINT.SEARCH(keyword));

export const prependResult = conversationResult => ({type: ACTION.PREPEND_RESULT, payload: conversationResult});

export const removeResult = conversationId => ({type: ACTION.REMOVE_RESULT, payload: conversationId});

export const updateListMetaStatus = () => ({type: ACTION.UPDATE_LIST_META_STATUS});

export const retrieveDetails = conversationId =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(conversationId));

export const updateIsMuted = (conversationId, isMuted) =>
    apiPatchRequest(ACTION.UPDATE_IS_MUTED, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(conversationId), {
        is_muted: isMuted,
    });

export const triggerBlock = conversationId =>
    apiPostRequest(ACTION.TRIGGER_BLOCK, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS_TRIGGER_BLOCK(conversationId));

export const queryDetails = conversationId =>
    apiGetRequest(ACTION.QUERY_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(conversationId));

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});

export const queryUserIds = conversationId =>
    apiGetRequest(ACTION.QUERY_USER_IDS, {user_ids: []}, ENDPOINT.DETAILS_USER_IDS(conversationId));

export const destroy = conversationId => apiDeleteRequest(ACTION.DESTROY, null, ENDPOINT.DETAILS(conversationId));

export const updateUserOnlineStatus = (userId, isOnline) => ({
    type: ACTION.UPDATE_USER_ONLINE_STATUS,
    payload: {userId, isOnline},
});

export const updateOnlineUserIds = currentUserId => ({type: ACTION.UPDATE_ONLINE_USER_IDS, payload: currentUserId});

export const updateGroupOnlineStatus = currentUserId => ({
    type: ACTION.UPDATE_GROUP_ONLINE_STATUS,
    payload: currentUserId,
});

export const removeGroupMember = (groupId, userId) => ({type: ACTION.REMOVE_GROUP_MEMBER, payload: {groupId, userId}});

export const queryCallOfferInfo = callId =>
    apiGetRequest(ACTION.QUERY_CALL_OFFER_INFO, {}, ENDPOINT.CALLS_OFFER_INFO(callId));

export const createMessage = (conversationId, content, attachments = undefined) =>
    apiPostRequest(ACTION.CREATE_MESSAGE, null, ENDPOINT.MESSAGES(conversationId), {content, attachments});

export const retrieveMessages = (conversationId, page = 1) =>
    apiGetListRequest(
        page > 1 ? ACTION.RETRIEVE_MESSAGES_PREPEND : ACTION.RETRIEVE_MESSAGES,
        MESSAGES_INITIAL_STATE,
        ENDPOINT.MESSAGES_LIST(conversationId, page)
    );

export const retrieveMessagesAttachments = (conversationId, messageId) =>
    apiGetRequest(
        ACTION.RETRIEVE_MESSAGES_ATTACHMENTS,
        MESSAGES_ATTACHMENTS_INITIAL_STATE,
        ENDPOINT.MESSAGES_ATTACHMENTS_LIST(conversationId, messageId)
    );

export const clearMessagesAttachments = () => ({type: ACTION.CLEAR_MESSAGES_ATTACHMENTS});

export const querySupplementaryMessageData = (conversationId, messageIndex) =>
    apiGetRequest(
        ACTION.QUERY_SUPPLEMENTARY_MESSAGE_DATA,
        {id: '', has_attachments: false},
        ENDPOINT.MESSAGES_SUPPLEMENTARY_DATA(conversationId, messageIndex)
    );

export const autoClickMessage = messageId => ({type: ACTION.AUTO_CLICK_MESSAGE, payload: messageId});

export const addSendingMessage = (id, content) => ({type: ACTION.ADD_SENDING_MESSAGE, payload: {id, content}});

export const removeSendingMessage = id => ({type: ACTION.REMOVE_SENDING_MESSAGE, payload: id});

export const pushMessage = message => ({type: ACTION.PUSH_MESSAGE, payload: message});

export const setLastMessage = (conversationId, content) => ({
    type: ACTION.SET_LAST_MESSAGE,
    payload: {conversationId, content},
});

export const destroyMessages = conversationId =>
    apiDeleteRequest(ACTION.DESTROY_MESSAGES, null, ENDPOINT.MESSAGES(conversationId));

export const clearMessages = resetMeta => ({type: ACTION.CLEAR_MESSAGES, payload: resetMeta});

export const triggerMessagesSearchVisible = () => ({type: ACTION.TRIGGER_MESSAGES_SEARCH_VISIBLE});

export const appendMessagesSearchMatchingIds = messageIds => ({
    type: ACTION.APPEND_MESSAGES_SEARCH_MATCHING_IDS,
    payload: messageIds,
});

export const clearMessagesSearchMatchingIds = () => ({type: ACTION.CLEAR_MESSAGES_SEARCH_MATCHING_IDS});
