import {STATUS} from '../app/utils/api/constants';
import {updateMemberIsOnline, getOnlineMemberIds, isGroupOnline} from '../groups/utils/reducers';
import {
    INITIAL_STATE,
    ACTION,
    DETAILS_INITIAL_STATE,
    MESSAGES_INITIAL_STATE,
    MESSAGES_ATTACHMENTS_INITIAL_STATE,
} from './constants';
import {getAttachmentsWithNameFields} from './utils/reducers';

const conversationsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_LIST:
        case ACTION.SEARCH_LIST:
            return {
                ...state,
                list: {
                    ...state.list,
                    ...action.payload,
                    resultsIds: action.payload.results.map(conversation => conversation.id),
                },
            };
        case ACTION.RETRIEVE_LIST_APPEND:
            return {
                ...state,
                list: {
                    ...state.list,
                    ...action.payload,
                    results: [...state.list.results, ...action.payload.results],
                    resultsIds: [
                        ...state.list.resultsIds,
                        ...action.payload.results.map(conversation => conversation.id),
                    ],
                },
            };
        case ACTION.PREPEND_RESULT:
            return {
                ...state,
                list: {
                    ...state.list,
                    count: state.list.count + 1,
                    results: [action.payload, ...state.list.results],
                    resultsIds: [action.payload.id, ...state.list.resultsIds],
                },
            };
        case ACTION.REMOVE_RESULT: {
            const resultsIds = state.list.resultsIds.filter(id => id !== action.payload);

            return {
                ...state,
                list: {
                    ...state.list,
                    count: resultsIds.length, // In case conversationId doesn't match.
                    results: state.list.results.filter(({id}) => id !== action.payload),
                    resultsIds,
                },
            };
        }
        case ACTION.UPDATE_LIST_META_STATUS:
            return {
                ...state,
                list: {
                    ...state.list,
                    __meta__: {
                        ...state.list.__meta__,
                        status: state.list.count === 0 ? STATUS.OK_NO_RESULTS : STATUS.OK,
                    },
                },
            };
        case ACTION.RETRIEVE_DETAILS:
        case ACTION.UPDATE_IS_MUTED:
        case ACTION.TRIGGER_BLOCK:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return {
                ...state,
                details: DETAILS_INITIAL_STATE,
            };
        case ACTION.UPDATE_USER_ONLINE_STATUS: {
            const {userId, isOnline} = action.payload;

            // Change online status in list.
            const newListResultsState = state.list.results.map(conversation => {
                if (conversation.to_user && conversation.to_user.id === userId) {
                    return {...conversation, to_user: {...conversation.to_user, is_online: isOnline}};
                }

                if (conversation.to_group) {
                    return {
                        ...conversation,
                        to_group: {
                            ...conversation.to_group,
                            members: updateMemberIsOnline(conversation.to_group.members, userId, isOnline),
                        },
                    };
                }

                return conversation;
            });

            // Change online status in details and add/remove it to/from the userIds if it's necessary.
            const newDetailsToUser =
                state.details.to_user && state.details.to_user.id === userId
                    ? {...state.details.to_user, is_online: isOnline}
                    : state.details.to_user;

            const newDetailsToGroup = state.details.to_group
                ? {
                      ...state.details.to_group,
                      members: updateMemberIsOnline(state.details.to_group.members, userId, isOnline),
                  }
                : state.details.to_group;

            const newDetailsState = {
                ...state.details,
                to_user: newDetailsToUser,
                to_group: newDetailsToGroup,
            };

            return {
                ...state,
                list: {
                    ...state.list,
                    results: newListResultsState,
                },
                details: newDetailsState,
            };
        }
        case ACTION.UPDATE_ONLINE_USER_IDS: {
            const {to_user, to_group} = state.details;

            // Send and queue messages only to the online ones.
            return {
                ...state,
                details: {
                    ...state.details,
                    online_user_ids: to_user
                        ? to_user.is_online
                            ? [to_user.id]
                            : []
                        : getOnlineMemberIds(to_group, action.payload),
                },
            };
        }
        case ACTION.UPDATE_GROUP_ONLINE_STATUS:
            return {
                ...state,
                list: {
                    ...state.list,
                    results: state.list.results.map(conversation =>
                        conversation.to_group
                            ? {
                                  ...conversation,
                                  to_group: {
                                      ...conversation.to_group,
                                      is_online: isGroupOnline(conversation.to_group.members, action.payload),
                                  },
                              }
                            : conversation
                    ),
                },
                details: {
                    ...state.details,
                    to_group: state.details.to_group
                        ? {
                              ...state.details.to_group,
                              is_online: state.details.online_user_ids.length > 0,
                          }
                        : state.details.to_group,
                },
            };
        case ACTION.REMOVE_GROUP_MEMBER: {
            const {groupId, userId} = action.payload;

            return {
                ...state,
                list: {
                    ...state.list,
                    results: state.list.results.map(conversation =>
                        conversation.to_group
                            ? {
                                  ...conversation,
                                  to_group:
                                      conversation.to_group.id === groupId
                                          ? {
                                                ...conversation.to_group,
                                                members: conversation.to_group.members.filter(({id}) => id !== userId),
                                            }
                                          : conversation.to_group,
                              }
                            : conversation
                    ),
                },
                details:
                    state.details.to_group?.id === groupId
                        ? {
                              ...state.details,
                              to_group: {
                                  ...state.details.to_group,
                                  members: state.details.to_group.members.filter(({id}) => id !== userId),
                              },
                          }
                        : state.details,
            };
        }
        case ACTION.RETRIEVE_MESSAGES:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    ...action.payload,
                    results: [...action.payload.results, ...state.messages.results],
                },
            };
        case ACTION.RETRIEVE_MESSAGES_PREPEND:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    ...action.payload,
                    results: [...action.payload.results, ...state.messages.results],
                },
            };
        case ACTION.RETRIEVE_MESSAGES_ATTACHMENTS:
            return {
                ...state,
                messagesAttachments: {
                    ...action.payload,
                    results: action.payload.results.map(getAttachmentsWithNameFields),
                },
            };
        case ACTION.CLEAR_MESSAGES_ATTACHMENTS:
            return {
                ...state,
                messagesAttachments: MESSAGES_ATTACHMENTS_INITIAL_STATE,
            };
        case ACTION.AUTO_CLICK_MESSAGE:
            return {
                ...state,
                autoClickMessageId: action.payload,
            };
        case ACTION.ADD_SENDING_MESSAGE:
            return {
                ...state,
                sendingMessages: [...state.sendingMessages, action.payload],
            };
        case ACTION.REMOVE_SENDING_MESSAGE:
            return {
                ...state,
                sendingMessages: state.sendingMessages.filter(({id}) => id !== action.payload),
            };
        case ACTION.PUSH_MESSAGE:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    count: state.messages.count + 1,
                    results: [...state.messages.results, action.payload],
                },
            };
        case ACTION.SET_LAST_MESSAGE: {
            const {conversationId, content} = action.payload;

            return {
                ...state,
                list: {
                    ...state.list,
                    results: state.list.results.map(conversation =>
                        conversation.id === conversationId ? {...conversation, last_message: content} : conversation
                    ),
                },
            };
        }
        case ACTION.CLEAR_MESSAGES: {
            const {__meta__, ...rest} = MESSAGES_INITIAL_STATE;

            return {
                ...state,
                messages: action.payload ? MESSAGES_INITIAL_STATE : {...state.messages, ...rest},
            };
        }
        case ACTION.TRIGGER_MESSAGES_SEARCH_VISIBLE:
            return {
                ...state,
                messagesSearch: {
                    ...state.messagesSearch,
                    visible: !state.messagesSearch.visible,
                },
            };
        case ACTION.APPEND_MESSAGES_SEARCH_MATCHING_IDS:
            return {
                ...state,
                messagesSearch: {
                    ...state.messagesSearch,
                    matchingIds: [...state.messagesSearch.matchingIds, ...action.payload],
                },
            };
        case ACTION.CLEAR_MESSAGES_SEARCH_MATCHING_IDS:
            return {
                ...state,
                messagesSearch: {
                    ...state.messagesSearch,
                    matchingIds: [],
                },
            };
        default:
            return state;
    }
};

export default conversationsReducer;
