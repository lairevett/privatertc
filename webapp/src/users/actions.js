import {apiGetListRequest, apiGetRequest, apiPostRequest} from '../app/utils/api/requests';
import {DETAILS_INITIAL_STATE as CONVERSATION_DETAILS_INITIAL_STATE} from '../conversations/constants';
import {ACTION, LIST_INITIAL_STATE, ENDPOINT, DETAILS_INITIAL_STATE} from './constants';

export const retrieveList = (page = 1) =>
    apiGetListRequest(
        page > 1 ? ACTION.RETRIEVE_LIST_APPEND : ACTION.RETRIEVE_LIST,
        LIST_INITIAL_STATE,
        ENDPOINT.LIST(page)
    );

export const searchList = keyword =>
    apiGetListRequest(ACTION.SEARCH_LIST, LIST_INITIAL_STATE, ENDPOINT.SEARCH(keyword));

export const clearList = () => ({type: ACTION.CLEAR_LIST});

export const retrieveDetails = id =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.DETAILS(id));

export const queryChannelsCount = id =>
    apiGetRequest(ACTION.QUERY_CHANNELS_COUNT, {channels_count: 0}, ENDPOINT.CHANNELS_COUNT(id));

export const createConversation = id =>
    apiPostRequest(ACTION.CREATE_CONVERSATION, CONVERSATION_DETAILS_INITIAL_STATE, ENDPOINT.DETAILS_CONVERSATION(id));

export const queryConversationId = id =>
    apiGetRequest(ACTION.QUERY_CONVERSATION_ID, {conversation_id: ''}, ENDPOINT.DETAILS_CONVERSATION_ID(id));

export const updateOnlineStatus = (id, isOnline) => ({type: ACTION.UPDATE_ONLINE_STATUS, payload: {id, isOnline}});
