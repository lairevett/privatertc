import {INITIAL_STATE, ACTION, LIST_INITIAL_STATE} from './constants';

const usersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.RETRIEVE_LIST:
        case ACTION.SEARCH_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case ACTION.RETRIEVE_LIST_APPEND:
            return {
                ...state,
                list: {
                    ...state.list,
                    ...action.payload,
                },
            };
        case ACTION.CLEAR_LIST:
            return {
                ...state,
                list: LIST_INITIAL_STATE,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.UPDATE_ONLINE_STATUS: {
            const {id, isOnline} = action.payload;

            return {
                ...state,
                list: {
                    ...state.list,
                    results: state.list.results.map(user =>
                        user.id === id
                            ? {...user, is_online: isOnline, last_seen: isOnline ? null : new Date().toJSON()}
                            : user
                    ),
                },
                details:
                    state.details.id === id
                        ? {...state.details, is_online: isOnline, last_seen: isOnline ? null : new Date().toJSON()}
                        : state.details,
            };
        }
        default:
            return state;
    }
};

export default usersReducer;
