import {registerNavSearchFunction} from '../app/utils/nav';
import {searchList} from './actions';

registerNavSearchFunction('users:list', searchList);
