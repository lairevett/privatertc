import {DEFAULT_META_INITIAL_STATE, DEFAULT_LIST_INITIAL_STATE, META_PROP_TYPE} from '../app/utils/api/constants';

export const MODULE_NAME = 'users';

// API endpoints.
export const ENDPOINT = {
    LIST: page => `/v1/${MODULE_NAME}/?page=${page}`,
    SEARCH: keyword => `/v1/${MODULE_NAME}/?search=${keyword}`,
    DETAILS: id => `/v1/${MODULE_NAME}/${id}/`,
    CHANNELS_COUNT: id => `/v1/${MODULE_NAME}/${id}/channels_count/`,
    DETAILS_CONVERSATION: id => `/v1/${MODULE_NAME}/${id}/conversation/`,
    DETAILS_CONVERSATION_ID: id => `/v1/${MODULE_NAME}/${id}/conversation/?show_id`,
};

// Redux action types.
export const ACTION = {
    RETRIEVE_LIST: `@${MODULE_NAME}/RETRIEVE_LIST`,
    RETRIEVE_LIST_APPEND: `@${MODULE_NAME}/RETRIEVE_LIST_APPEND`,
    SEARCH_LIST: `@${MODULE_NAME}/SEARCH_LIST`,
    CLEAR_LIST: `@${MODULE_NAME}/CLEAR_LIST`,
    RETRIEVE_DETAILS: `@${MODULE_NAME}/RETRIEVE_DETAILS`,
    QUERY_CHANNELS_COUNT: `@${MODULE_NAME}/QUERY_CHANNELS_COUNT`,
    CREATE_CONVERSATION: `@${MODULE_NAME}/CREATE_CONVERSATION`,
    QUERY_CONVERSATION_ID: `@${MODULE_NAME}/QUERY_CONVERSATION_ID`,
    UPDATE_ONLINE_STATUS: `@${MODULE_NAME}/UPDATE_ONLINE_STATUS`,
};

// Memoized API queries.
export const MEMO_API_QUERY = {
    CHANNELS_COUNT: 'user_channels_count',
    CONVERSATION_ID: 'user_conversation_id',
};

// Reducer initial states.
export const LIST_INITIAL_STATE = {...DEFAULT_LIST_INITIAL_STATE};

export const DETAILS_INITIAL_STATE = {
    id: '',
    broadcasted_name: '',
    is_online: false,
    last_seen: null,
    is_active: false,
    is_staff: false,
    is_superuser: false,
    ...DEFAULT_META_INITIAL_STATE,
};

export const SELF_DETAILS_INITIAL_STATE = {
    username: '',
    ...DETAILS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    list: LIST_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Prop types.
export const DETAILS_PROP_TYPE = PropertyTypes =>
    PropertyTypes.shape({
        id: PropertyTypes.string.isRequired,
        username: PropertyTypes.string,
        broadcasted_name: PropertyTypes.string.isRequired,
        is_online: PropertyTypes.bool.isRequired,
        last_seen: PropertyTypes.string,
        is_active: PropertyTypes.bool,
        is_staff: PropertyTypes.bool,
        is_superuser: PropertyTypes.bool,
        __meta__: META_PROP_TYPE(PropertyTypes),
    });

export const LIST_RESULTS_PROP_TYPE = PropertyTypes => PropertyTypes.arrayOf(DETAILS_PROP_TYPE(PropertyTypes));
