import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import deepEqual from 'deep-equal';
import PaginatedTableList from '../../../app/components/partials/paginated_table_list';
import NoResultsText from '../../../app/components/partials/no_results_text';
import {SCROLL_DIRECTION} from '../../../app/utils/api/constants';
import {getDisplayName} from '../../../app/utils/display_name';
import {retrieveList} from '../../actions';

const UserList = ({JSXElement, displayCurrentUser}) => {
    const currentUserId = useSelector(state => state.auth.currentUser.id);
    const results = useSelector(state => state.users.list.results, deepEqual);
    const nextPageLink = useSelector(state => state.users.list.next);

    // Count could be 0 during search.
    return results.length > 0 ? (
        <PaginatedTableList
            scrollDirection={SCROLL_DIRECTION.BOTTOM}
            nextPageLink={nextPageLink}
            actionCallback={retrieveList}
        >
            {results.map(result => {
                const {id, is_online, last_seen} = result;
                const displayName = getDisplayName(result);

                return (
                    (id !== currentUserId || displayCurrentUser) && (
                        <JSXElement
                            key={id}
                            id={id}
                            displayName={displayName}
                            isOnline={is_online}
                            lastSeen={last_seen}
                            currentUserId={currentUserId}
                        />
                    )
                );
            })}
        </PaginatedTableList>
    ) : (
        <NoResultsText />
    );
};

UserList.propTypes = {
    JSXElement: PropTypes.func.isRequired,
    displayCurrentUser: PropTypes.bool,
};

UserList.defaultProps = {
    displayCurrentUser: false,
};

export default memo(UserList);
