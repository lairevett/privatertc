import React from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import ActiveStatusBadge from './active_status_badge';

const UserListElementActiveStatus = ({isOnline}) => (
    <TableListCell appendClassName="pr-0">
        <ActiveStatusBadge isOnline={isOnline} />
    </TableListCell>
);

UserListElementActiveStatus.propTypes = {
    isOnline: PropTypes.bool.isRequired,
};

export default UserListElementActiveStatus;
