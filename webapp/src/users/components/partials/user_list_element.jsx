import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';
import {debounce} from '../../../app/utils/events';
import TableListRow from '../../../app/components/partials/table_list_row';
import UserListElementActiveStatus from './user_list_element_active_status';
import UserListElementInfo from './user_list_element_info';
import UserListElementPopOver from './user_list_element_popover';

const UserListElement = ({id, displayName, isOnline, lastSeen, currentUserId}) => {
    const [isPopOverActive, setIsPopOverActive] = useState(false);
    const popOverReference = useRef();

    const hidePopOver = debounce(() => {
        const {current} = popOverReference;
        if (current) {
            current.classList.remove('shown');
            setIsPopOverActive(false);
        }
    }, 150);

    const triggerPopOver = debounce(evnt => {
        const {current} = popOverReference;
        if (current) {
            if (current.classList.contains('shown')) {
                evnt.currentTarget.blur();
                return;
            }

            current.classList.add('shown');
            setIsPopOverActive(true);
        }
    }, 150);

    const popOver = <UserListElementPopOver ref={popOverReference} userId={id} />;

    return (
        <TableListRow
            to="#"
            onClick={triggerPopOver}
            onKeyPress={triggerPopOver}
            onBlur={hidePopOver}
            isActive={isPopOverActive}
            turnSymbolOnActive
            afterLink={popOver}
        >
            <UserListElementActiveStatus isOnline={isOnline} />
            <UserListElementInfo
                displayName={displayName}
                isOnline={isOnline}
                lastSeen={lastSeen}
                isSelf={id === currentUserId}
            />
        </TableListRow>
    );
};

UserListElement.propTypes = {
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    isOnline: PropTypes.bool.isRequired,
    lastSeen: PropTypes.string,
    currentUserId: PropTypes.string.isRequired,
};

UserListElement.defaultProps = {
    lastSeen: null,
};

export default UserListElement;
