import React from 'react';
import PropTypes from 'prop-types';
import {propTypes, reduxForm} from 'redux-form';
import SubmitButton from '../../../app/components/partials/submit_button';
import {submit} from '../../utils/forms/process_conversation';

const ConversationForm = ({handleSubmit, submitting}) => (
    <form onSubmit={handleSubmit(submit)} className="h-100">
        <SubmitButton disabled={submitting} title="Start conversation" appendClassName="h-100">
            <span className="material-icons">chat_bubble</span>
        </SubmitButton>
    </form>
);

ConversationForm.propTypes = {
    ...propTypes,
    // eslint-disable-next-line react/no-unused-prop-types, react-redux/no-unused-prop-types
    userId: PropTypes.string.isRequired, // Used in submit().
};

// Name set in user_list_element.
export default reduxForm()(ConversationForm);
