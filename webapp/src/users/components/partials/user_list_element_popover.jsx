import React, {forwardRef} from 'react';
import PropTypes from 'prop-types';
import TableListCell from '../../../app/components/partials/table_list_cell';
import {TYPE as CALL_TYPE} from '../../../calls/constants';
import CallForm from '../../../calls/components/partials/call_form';
import ConversationForm from './conversation_form';

const UserListElementPopOver = forwardRef(({userId}, reference) => (
    <TableListCell ref={reference} appendClassName="pop-over">
        <ConversationForm form={`usersConversation_${userId}`} userId={userId} />
        <div className="mr-4" />
        <CallForm form={`usersAudioCall_${userId}`} userId={userId} type={CALL_TYPE.AUDIO} />
        <div className="mr-4" />
        <CallForm form={`usersAudioAndVideoCall_${userId}`} userId={userId} type={CALL_TYPE.AUDIO_AND_VIDEO} />
    </TableListCell>
));

UserListElementPopOver.propTypes = {
    userId: PropTypes.string.isRequired,
};

export default UserListElementPopOver;
