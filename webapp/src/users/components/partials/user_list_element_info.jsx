import React from 'react';
import PropTypes from 'prop-types';
import {isOnSameDayAsToday} from '../../../app/utils/dates';
import TableListCell from '../../../app/components/partials/table_list_cell';

const UserListElementInfo = ({displayName, isOnline, lastSeen, isSelf}) => {
    const lastSeenDateObject = lastSeen ? new Date(lastSeen) : null;
    const wasLastSeenOnSameDay = lastSeenDateObject && isOnSameDayAsToday(lastSeenDateObject);

    return (
        <TableListCell appendClassName="w-100">
            <span className="vertical-center">
                {displayName}
                <br />
                {isSelf ? (
                    <span className="badge badge-pill bg-info text-white">You</span>
                ) : (
                    !isOnline &&
                    lastSeen && (
                        <span className="text-muted font-italic" style={{whiteSpace: 'nowrap'}}>
                            Last seen:{' '}
                            {wasLastSeenOnSameDay
                                ? lastSeenDateObject.toLocaleTimeString()
                                : lastSeenDateObject.toLocaleString()}
                        </span>
                    )
                )}
            </span>
        </TableListCell>
    );
};

UserListElementInfo.propTypes = {
    displayName: PropTypes.string.isRequired,
    isOnline: PropTypes.bool.isRequired,
    lastSeen: PropTypes.string,
    isSelf: PropTypes.bool,
};

UserListElementInfo.defaultProps = {
    lastSeen: null,
    isSelf: false,
};

export default UserListElementInfo;
