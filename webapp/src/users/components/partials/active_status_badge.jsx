import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

const ActiveStatusBadge = ({appendClassName, isOnline}) => {
    const isConnected = useSelector(state => state.ws.isConnected);

    return (
        <span
            className={`font-half vertical-center badge badge-pill ${
                isConnected && isOnline ? 'bg-success' : 'bg-danger'
            } ${appendClassName}`}
        >
            &nbsp;
        </span>
    );
};

ActiveStatusBadge.propTypes = {
    appendClassName: PropTypes.string,
    isOnline: PropTypes.bool.isRequired,
};

ActiveStatusBadge.defaultProps = {
    appendClassName: '',
};

export default ActiveStatusBadge;
