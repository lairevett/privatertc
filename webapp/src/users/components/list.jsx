import React from 'react';
import {useSelector} from 'react-redux';
import RegularView from '../../app/components/partials/regular_view';
import Suspense from '../../app/components/partials/suspense';
import Loading from '../../app/components/partials/loading';
import UserList from './partials/user_list';
import UserListElement from './partials/user_list_element';

const List = () => {
    const status = useSelector(state => state.users.list.__meta__.status);

    return (
        <RegularView>
            <Suspense status={status} fallback={<Loading />}>
                <UserList JSXElement={UserListElement} displayCurrentUser />
            </Suspense>
        </RegularView>
    );
};

export default List;
