import {memoizedQuery} from '../../app/utils/api/memo';
import {MEMO_API_QUERY} from '../constants';
import {queryChannelsCount, queryConversationId} from '../actions';

export const memoizedQueryUserChannelsCount = async (dispatch, userId) =>
    memoizedQuery(dispatch, userId, MEMO_API_QUERY.CHANNELS_COUNT, queryChannelsCount(userId));

export const memoizedQueryUserConversationId = async (dispatch, userId) =>
    memoizedQuery(dispatch, userId, MEMO_API_QUERY.CONVERSATION_ID, queryConversationId(userId));
