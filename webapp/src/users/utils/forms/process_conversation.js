import {checkSubmitErrors} from '../../../app/utils/api/redux_form';
import {history, reverseUrl} from '../../../app/utils/router';
import {STATUS} from '../../../app/utils/api/constants';
import {createConversation} from '../../actions';

export const submit = async (_, dispatch, {userId}) => {
    const {payload} = dispatch(await createConversation(userId)());

    const goToConversation = () => {
        history.push(reverseUrl('conversations:details', {conversationId: payload.id}).path);
    };

    if (payload.__meta__.status === STATUS.RESPONSE_CONFLICT) {
        // Conversation already exists.
        goToConversation();
        return;
    }

    checkSubmitErrors(payload);
    goToConversation();
};
