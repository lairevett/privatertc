import {SCROLL_DIRECTION} from '../../app/utils/api/constants';
import {usePaginationActionDispatchOnScroll} from '../../app/utils/api/hooks';
import {retrieveNextList} from '../actions';

export const useAppendNextUsersWhenListEndReached = (listViewReference, nextPageLink) =>
    usePaginationActionDispatchOnScroll(SCROLL_DIRECTION.BOTTOM, listViewReference, nextPageLink, retrieveNextList);
