import {makeDispatchEvents} from '../../app/utils/dispatch_events';
import {retrieveList, clearList} from '../actions';

export const callbackGetDispatchEvents = () => () => makeDispatchEvents([retrieveList(1)], [clearList]);
