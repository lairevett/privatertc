/* eslint-disable security/detect-object-injection */
import {v4 as uuidv4} from 'uuid';
import {WS_URL} from '../app/utils/ws/constants';
import {ACTION, ALLOWED_MESSAGE_TYPES} from './constants';
import {setIsConnected} from './actions';

const wsMiddleware = () => {
    let connection = null;

    const _addEventListener = (type, listener) => {
        if (connection == null) {
            console.error(`Connect to the WebSocket before adding "${type}" event listener.`);
            return;
        }

        connection.addEventListener(type, listener);
    };

    const _removeEventListener = (type, listener) => {
        if (connection == null) {
            console.error(`Connect to the WebSocket before removing "${type}" event listener.`);
            return;
        }

        connection.removeEventListener(type, listener);
    };

    const _parseMessage = evnt => {
        try {
            const message = JSON.parse(evnt.data);
            const jsonMessageEvent = new CustomEvent('jsonMessage', {detail: message});
            connection.dispatchEvent(jsonMessageEvent);
        } catch (error) {
            console.error(error);
            console.debug(evnt);
        }
    };

    const _handleMessage = store => {
        const memo = {};

        return async evnt => {
            console.log(evnt.detail);
            const {message_type} = evnt.detail.payload;

            if (ALLOWED_MESSAGE_TYPES.includes(message_type)) {
                let handler = memo[message_type];

                if (!handler) {
                    try {
                        handler = await import(`./utils/handlers/${message_type}`);
                        memo[message_type] = handler;
                    } catch (error) {
                        console.warn(`No handler found for "${message_type}" message type.`);
                        console.trace(error);
                    }
                }

                handler.default(store, evnt.detail.payload);
            }
        };
    };

    const openConnection = store => {
        if (connection != null) {
            console.error('Close previous connection before opening a new one.');
            return;
        }

        connection = new WebSocket(WS_URL);
        connection.addEventListener('error', closeConnection);
        connection.addEventListener('close', () => store.dispatch(setIsConnected(false)));
        connection.addEventListener('message', _parseMessage);
        connection.addEventListener('jsonMessage', _handleMessage(store));
        connection.addEventListener('open', () => store.dispatch(setIsConnected(true)));
    };

    const sendMessage = message => {
        if (connection == null) {
            console.error('Connect to the WebSocket before sending a message.');
            return;
        }

        try {
            const stringifiedMessage = JSON.stringify(message);
            connection.send(stringifiedMessage);
        } catch (error) {
            console.error(error.message);
            console.debug(message);
        }
    };

    const sendRequest = message => {
        const id = uuidv4();

        return new Promise((resolve, reject) => {
            let timeout;

            const listener = evnt => {
                // Listen for socket response message.
                if (evnt.detail.id === id) {
                    clearTimeout(timeout);

                    // Fire this handler only once, detach when socket responds.
                    _removeEventListener('jsonMessage', listener);
                    resolve(evnt.detail.payload);
                }
            };

            _addEventListener('jsonMessage', listener);
            sendMessage({id, ...message});

            // Don't wait more than a minute for the response message.
            timeout = setTimeout(() => {
                _removeEventListener('jsonMessage', listener);
                reject(new Error('Request timed out, try again later.'));
            }, 3000);
        });
    };

    const closeConnection = () => {
        if (connection == null) {
            console.warn('There was no connection to close.');
            return;
        }

        connection.close();
        connection = null;
    };

    return store => next => action => {
        switch (action.type) {
            case ACTION.CONNECT:
                openConnection(store, action.payload);
                break;
            case ACTION.RECONNECT:
                closeConnection();
                openConnection(store);
                break;
            case ACTION.SEND_MESSAGE:
                sendMessage(action.payload);
                break;
            case ACTION.SEND_REQUEST:
                return sendRequest(action.payload);
            case ACTION.DISCONNECT:
                closeConnection();
                break;
            default:
                return next(action);
        }
    };
};

export default wsMiddleware();
