const makeMessage = (type, additionalPayload = {}) => ({message_type: type, ...additionalPayload});

export const makeJoinMessage = userIds => makeMessage('join', {user_ids: userIds});

export const makeCreateRoomMessage = (name, pin) => makeMessage('create_room', {room_name: name, room_pin: pin});
export const makeJoinRoomMessage = (broadcastedName, name, pin) =>
    makeMessage('join_room', {broadcasted_name: broadcastedName, room_name: name, room_pin: pin});
export const makeLeaveRoomMessage = name => makeMessage('leave_room', {room_name: name});

export const makeRelayIceCandidateMessage = (wsChannelName, iceCandidate) =>
    makeMessage('relay_ice_candidate', {
        ws_channel_name: wsChannelName,
        ice_candidate: iceCandidate,
    });

export const makeRelayDescriptionMessage = (wsChannelName, description) =>
    makeMessage('relay_description', {
        ws_channel_name: wsChannelName,
        description,
    });

export const makeOfferCallMessage = id => makeMessage('offer_call', {call_id: id});
export const makeAnswerCallMessage = id => makeMessage('answer_call', {call_id: id});
export const makeHangUpCallMessage = () => makeMessage('hang_up_call');

// export const makeGroupJoinConversationMessage = conversationId =>
// makeMessage('signal.relay_conversation_message', {
// conversation_id: conversationId,
// conversation_message: 'group_join',
// });

// export const makeGroupInviteConversationMessage = (conversationId, userId) =>
// makeMessage('signal.relay_conversation_message', {
// conversation_id: conversationId,
// user_id: userId,
// conversation_message: 'group_invite',
// });

// export const makeGroupChangeRankConversationMessage = (conversationId, userId, newRank) =>
// makeMessage('signal.relay_conversation_message', {
// conversation_id: conversationId,
// user_id: userId,
// conversation_message: 'change_rank',
// new_rank: newRank,
// });

// export const makeGroupKickConversationMessage = (conversationId, userId) =>
// makeMessage('signal.relay_conversation_message', {
// conversation_id: conversationId,
// user_id: userId,
// conversation_message: 'group_kick',
// });

// export const makeGroupDropConversationMessage = conversationId =>
// makeMessage('signal.relay_conversation_message', {
// conversation_id: conversationId,
// conversation_message: 'group_drop',
// });
