import {STATUS as CALL_STATUS} from '../../../calls/constants';
import {updateActive as callsUpdateActive} from '../../../calls/actions';
import {redirectToCall} from '../../../calls/utils/router';
import {queryCallOfferInfo as conversationsQueryCallOfferInfo} from '../../../conversations/actions';

const notifyAboutIncomingCall = (store, message, offerInfoPayload) => {
    const {conversation_id, participants} = offerInfoPayload;

    store.dispatch(
        callsUpdateActive({
            status: CALL_STATUS.INCOMING,
            conversationId: conversation_id,
            id: message.call_id,
            participants,
            startedAt: new Date().toJSON(),
        })
    );

    redirectToCall(conversation_id);
};

const onReceivedOfferCall = async (store, message) => {
    const {active: activeCall} = store.getState().calls;

    const isBusy = activeCall.status !== CALL_STATUS.DISCONNECTED;
    if (isBusy) {
        return;
    }

    const {payload: offerInfoPayload} = store.dispatch(await conversationsQueryCallOfferInfo(message.call_id)());
    notifyAboutIncomingCall(store, message, offerInfoPayload);
};

export default onReceivedOfferCall;
