import {retrieveUrl, history, reverseUrl} from '../../../app/utils/router';
import {MODAL} from '../../../conversations/constants';
import {
    removeResult,
    updateListMetaStatus,
    removeGroupMember,
    updateOnlineUserIds,
    updateGroupOnlineStatus,
} from '../../../conversations/actions';

const redirectIfBrowsingDeletedConversation = (_, message) => {
    const {owner_conversation_id: conversationId} = message;
    const {pathname: currentPath} = history.location;
    const {path: deletedConversationDetailsPath} = reverseUrl('conversations:details', {conversationId});

    if (currentPath === deletedConversationDetailsPath) {
        history.replace(retrieveUrl('conversations:list').path);
    }
};

const onReceivedDeleteGroupConversation = (store, message) => {
    const currentUserId = store.getState().auth.currentUser.id;
    const {owner_user_id, owner_conversation_id, group_id} = message;

    if (owner_user_id === currentUserId) {
        // owner_conversation_id is present in message only if owner_user_id === currentUserId.
        store.dispatch(removeResult(owner_conversation_id));
        store.dispatch(updateListMetaStatus());

        if (!document.body.classList.contains('modal-open')) {
            redirectIfBrowsingDeletedConversation(store, message);
            return;
        }

        // Modal show/hide code runs asynchronously, when user submits leave group form,
        // the redirect is faster and doesn't remove the modal backdrop, which leaves app in
        // modal-shown unresponsive state.
        window.$(`#${MODAL.LEAVE_GROUP}`).on('hidden.bs.modal', () => {
            redirectIfBrowsingDeletedConversation(store, message);
        });
    } else {
        store.dispatch(removeGroupMember(group_id, owner_user_id));
        store.dispatch(updateOnlineUserIds(currentUserId));
        store.dispatch(updateGroupOnlineStatus(currentUserId));
    }
};

export default onReceivedDeleteGroupConversation;
