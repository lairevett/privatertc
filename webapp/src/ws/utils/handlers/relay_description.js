import {relayDescription} from '../helpers';
import {addLinkToStorage} from './__shared__';

const sendNonCollidingDescription = async (store, message, link) => {
    const {sender_channel_name, description} = message;

    const offerCollision =
        description.type === 'offer' && (link.makingOffer || link.connection.signalingState !== 'stable');

    const ignoreOffer = !link.polite && offerCollision;
    if (ignoreOffer) {
        return;
    }

    // TODO: TypeError, RTCError, DOMException: InvalidAccessError, InvalidStateError, OperationError
    await link.connection.setRemoteDescription(description);

    if (description.type === 'offer') {
        relayDescription(store.dispatch, link, sender_channel_name);
    }
};

const onReceivedRelayDescription = (store, message) => {
    const {link} = addLinkToStorage(store, message, null, true);
    sendNonCollidingDescription(store, message, link);
};

export default onReceivedRelayDescription;
