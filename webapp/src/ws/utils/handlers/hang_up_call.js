import {STATUS as CALL_STATUS} from '../../../calls/constants';
import {updateActive as callsUpdateActive} from '../../../calls/actions';
import {tearDownCall as wrtcTearDownCall} from '../../../wrtc/actions';

const hangUpWhenAllParticipantsLeave = (store, message, activeCall) => {
    const {status, participants} = activeCall;
    if (status === CALL_STATUS.INCOMING && Object.keys(participants).length === 0) {
        store.dispatch(wrtcTearDownCall());
    }
};

const getUpdatedCallParticipants = (_, message, activeCall) => {
    const isInCall = !!activeCall.id;
    if (isInCall) {
        const {sender_user_id, sender_channel_name} = message;

        const targetChannelNames = activeCall.participants?.[sender_user_id] || [];
        const isTargetParticipant = targetChannelNames.includes(sender_channel_name);

        if (isTargetParticipant) {
            const updatedParticipants = {...activeCall.participants};

            // eslint-disable-next-line security/detect-object-injection
            updatedParticipants[sender_user_id] = targetChannelNames.filter(
                channelName => channelName !== sender_channel_name
            );

            return updatedParticipants;
        }
    }

    return null;
};

const onReceivedHangUpCall = (store, message) => {
    // NOTE: onReceivedLeave uses this to clean after the user exits browser without sending call_hang_up.
    const {active: activeCall} = store.getState().calls;
    const updatedCallParticipants = getUpdatedCallParticipants(store, message, activeCall);

    if (updatedCallParticipants != null) {
        const {sender_user_id, sender_channel_name} = message;
        store.dispatch(callsUpdateActive({participants: updatedCallParticipants}));
        store.dispatch(wrtcTearDownCall(true, sender_user_id, sender_channel_name));
    }

    hangUpWhenAllParticipantsLeave(store, message, activeCall);
};

export default onReceivedHangUpCall;
