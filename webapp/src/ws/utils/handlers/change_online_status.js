import {
    updateUserOnlineStatus as conversationsUpdateUserOnlineStatus,
    updateOnlineUserIds as conversationsUpdateOnlineUserIds,
    updateGroupOnlineStatus as conversationsUpdateGroupOnlineStatus,
} from '../../../conversations/actions';
import {updateOnlineStatus as usersUpdateOnlineStatus} from '../../../users/actions';

const onReceivedChangeOnlineStatus = (store, message) => {
    const {id: currentUserId} = store.getState().auth.currentUser;
    const {sender_user_id, is_online} = message;

    store.dispatch(conversationsUpdateUserOnlineStatus(sender_user_id, is_online));
    store.dispatch(conversationsUpdateOnlineUserIds(currentUserId));
    store.dispatch(conversationsUpdateGroupOnlineStatus(currentUserId));
    store.dispatch(usersUpdateOnlineStatus(sender_user_id, is_online));
};

export default onReceivedChangeOnlineStatus;
