import {getDisplayName} from '../../../app/utils/display_name';
import {makeConversationMessage} from '../../../conversations/utils/factory';
import {pushMessage as roomsPushMessage} from '../../../rooms/actions';
import {isRoomMessage, relayDescription} from '../helpers';
import {DATA_CHANNEL_LABEL} from '../../../wrtc/constants';
import {addLinkToStorage} from './__shared__';

const sendJoinedRoomConversationMessage = (store, message) => {
    const {sender_user_id, sender_user_broadcasted_name} = message;

    const senderUserDisplayName = getDisplayName({
        id: sender_user_id,
        broadcasted_name: sender_user_broadcasted_name,
    });

    const conversationMessage = makeConversationMessage(`${senderUserDisplayName} just joined this room.`);
    store.dispatch(roomsPushMessage(conversationMessage));
};

export const startSignaling = (store, message, link) => {
    relayDescription(store.dispatch, link, message.sender_channel_name);
};

const onReceivedJoin = (store, message) => {
    const isRoom = isRoomMessage(message);
    const dataChannelLabel = isRoom ? DATA_CHANNEL_LABEL.ROOM : DATA_CHANNEL_LABEL.CONVERSATION;

    const {link, isDataChannelNew} = addLinkToStorage(store, message, dataChannelLabel, false);

    if (isDataChannelNew) {
        // Don't send another offer if data channel is already in stable state.
        startSignaling(store, message, link);
    }

    if (isRoom) {
        sendJoinedRoomConversationMessage(store, message);
    }
};

export default onReceivedJoin;
