import {STATUS as CALL_STATUS} from '../../../calls/constants';
import {updateActive as callsUpdateActive} from '../../../calls/actions';
import {finalizeCallSetup as wrtcFinalizeCallSetup} from '../../../wrtc/actions';

const getUpdatedCallParticipants = (_, message, activeCall) => {
    const {sender_user_id, sender_channel_name} = message;
    const targetChannelNames = activeCall.participants?.[sender_user_id] || [];
    const isTargetParticipant = targetChannelNames.includes(sender_channel_name);

    if (!isTargetParticipant) {
        const updatedTargetChannelNames = [...targetChannelNames];
        updatedTargetChannelNames.push(sender_channel_name);

        const updatedParticipants = {...activeCall.participants, [sender_user_id]: updatedTargetChannelNames};
        return updatedParticipants;
    }

    return null;
};

const onReceivedAnswerCall = async (store, message) => {
    const {active: activeCall} = store.getState().calls;

    const isFromCurrentCall = activeCall.id === message.call_id;
    if (!isFromCurrentCall) {
        return;
    }

    const updatedCallParticipants = getUpdatedCallParticipants(store, message, activeCall);
    if (updatedCallParticipants != null) {
        store.dispatch(callsUpdateActive({participants: updatedCallParticipants}));
    }

    if (activeCall.status === CALL_STATUS.INCOMING) {
        // Prevent answering incoming calls for other logged in devices.
        return;
    }

    const {sender_user_id, sender_channel_name} = message;
    store.dispatch(wrtcFinalizeCallSetup(sender_user_id, sender_channel_name));
};

export default onReceivedAnswerCall;
