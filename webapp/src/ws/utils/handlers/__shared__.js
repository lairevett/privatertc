import {
    addUserToStorage as wrtcAddUserToStorage,
    addConnection as wrtcAddConnection,
    addDataChannel as wrtcAddDataChannel,
} from '../../../wrtc/actions';

export const addLinkToStorage = (store, message, dataChannelLabel, polite) => {
    // If link is already in storage in stable state, it just returns the stored one.
    const returnValue = {};

    const {sender_user_id, sender_channel_name, sender_user_broadcasted_name} = message;
    store.dispatch(wrtcAddUserToStorage(sender_user_id, sender_user_broadcasted_name));

    const link = store.dispatch(wrtcAddConnection(sender_user_id, sender_channel_name, polite));
    returnValue.link = link;

    if (dataChannelLabel) {
        const isDataChannelNew = store.dispatch(
            wrtcAddDataChannel(sender_user_id, sender_channel_name, dataChannelLabel)
        );

        returnValue.isDataChannelNew = isDataChannelNew;
    }

    return returnValue;
};
