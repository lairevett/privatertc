import {addIceCandidate as wrtcAddIceCandidate} from '../../../wrtc/actions';

const onReceivedRelayIceCandidate = (store, message) => {
    const {sender_user_id, sender_channel_name, ice_candidate} = message;
    store.dispatch(wrtcAddIceCandidate(sender_user_id, sender_channel_name, ice_candidate));
};

export default onReceivedRelayIceCandidate;
