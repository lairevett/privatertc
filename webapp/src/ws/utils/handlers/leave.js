import {getDisplayName} from '../../../app/utils/display_name';
import {makeConversationMessage} from '../../../conversations/utils/factory';
import {pushMessage as roomsPushMessage} from '../../../rooms/actions';
import {DATA_CHANNEL_LABEL} from '../../../wrtc/constants';
import {cleanUserStorageGarbage as wrtcCleanUserStorageGarbage} from '../../../wrtc/actions';
import {isRoomMessage} from '../helpers';
import onReceivedHangUpCall from './hang_up_call';

const sendLeftRoomConversationMessage = (store, message) => {
    const {sender_user_id, sender_user_broadcasted_name} = message;

    const senderUserDisplayName = getDisplayName({
        id: sender_user_id,
        broadcasted_name: sender_user_broadcasted_name,
    });

    const conversationMessage = makeConversationMessage(`${senderUserDisplayName} just left this room.`);
    store.dispatch(roomsPushMessage(conversationMessage));
};

const removeLinkFromStorage = (store, message, dataChannelLabel) => {
    const {sender_user_id, sender_channel_name} = message;
    // Clears everything including established data channels, user data stored in memory, etc.
    store.dispatch(wrtcCleanUserStorageGarbage(sender_user_id, sender_channel_name, dataChannelLabel));
};

const onReceivedLeave = (store, message) => {
    const isRoom = isRoomMessage(message);
    const dataChannelLabel = isRoom ? DATA_CHANNEL_LABEL.ROOM : DATA_CHANNEL_LABEL.CONVERSATION;

    if (isRoom) {
        sendLeftRoomConversationMessage(store, message);
    }

    onReceivedHangUpCall(store, message);
    removeLinkFromStorage(store, message, dataChannelLabel);
};

export default onReceivedLeave;
