import {makeRelayDescriptionMessage} from './factory';
import {sendMessage} from '../actions';

export const isGenericMessage = evntDetail => evntDetail.type && evntDetail.type === 'signal.send_generic_message';

export const isRoomMessage = payload => !!payload.room_name;

export const relayDescription = async (dispatch, link, wsChannelName) => {
    await link.connection.setLocalDescription();
    const relayDescriptionMessage = makeRelayDescriptionMessage(wsChannelName, link.connection.localDescription);
    dispatch(sendMessage(relayDescriptionMessage));
};
