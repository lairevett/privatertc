import {INITIAL_STATE, ACTION} from './constants';

const wsReducer = (state = INITIAL_STATE, action) => {
    if (action.type === ACTION.SET_IS_CONNECTED) {
        return {isConnected: action.payload};
    }

    return state;
};

export default wsReducer;
