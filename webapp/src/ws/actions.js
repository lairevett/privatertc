import {ACTION} from './constants';

export const connect = () => ({type: ACTION.CONNECT});
export const reconnect = () => ({type: ACTION.RECONNECT});
export const setIsConnected = isConnected => ({type: ACTION.SET_IS_CONNECTED, payload: isConnected});
export const sendMessage = message => ({type: ACTION.SEND_MESSAGE, payload: message});
export const sendRequest = message => ({type: ACTION.SEND_REQUEST, payload: message});
export const disconnect = () => ({type: ACTION.DISCONNECT});
