export const MODULE_NAME = 'ws';

// Redux action types.
export const ACTION = {
    CONNECT: `@${MODULE_NAME}/CONNECT`,
    RECONNECT: `@${MODULE_NAME}/RECONNECT`,
    SET_IS_CONNECTED: `@${MODULE_NAME}/SET_IS_CONNECTED`,
    SEND_MESSAGE: `@${MODULE_NAME}/SEND_MESSAGE`,
    SEND_REQUEST: `@${MODULE_NAME}/SEND_REQUEST`,
    DISCONNECT: `@${MODULE_NAME}/DISCONNECT`,
};

// Reducer initial states.
export const INITIAL_STATE = {
    isConnected: false,
};

export const ALLOWED_MESSAGE_TYPES = [
    'join',
    'relay_description',
    'relay_ice_candidate',
    'change_online_status',
    'delete_group_conversation',
    'offer_call',
    'answer_call',
    'hang_up_call',
    'leave',
];
