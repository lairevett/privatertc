from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import permissions, status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from users.serializers import UserSerializerV1
from users.permissions import IsAnonymousUser


class AuthAPIView(APIView):

    @permission_classes((permissions.IsAuthenticated,))
    def get(self, request):
        return Response(UserSerializerV1(self.request.user).data)

    @permission_classes((IsAnonymousUser,))
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        if not (username and password):
            raise ValidationError({'detail': 'You have to provide username and/or password.'})

        user = authenticate(username=username, password=password)
        if not user:
            raise ValidationError({'detail': 'Invalid credentials.'})

        login(request, user)
        return Response()

    def delete(self, request):
        logout(request)
        return Response()


@api_view(('POST',))
@permission_classes((permissions.IsAuthenticated,))
def update_broadcasted_name(request):
    user = request.user
    broadcasted_name = request.data.get('broadcasted_name')

    serializer = UserSerializerV1(data={'broadcasted_name': broadcasted_name})
    if not serializer.is_valid():
        raise ValidationError({'errors': {'broadcasted_name': 'Broadcasted name is not valid.'}})

    user.broadcasted_name = broadcasted_name
    user.save()
    return Response({'broadcasted_name': broadcasted_name}, status=status.HTTP_200_OK)


@api_view(('POST',))
@permission_classes((permissions.IsAuthenticated,))
def update_password(request):
    user = request.user
    current_password = request.data.get('current_password')
    password = request.data.get('password')

    if current_password == password:
        raise ValidationError({'errors': {'password': 'New password can\'t be the same as the current password.'}})

    if not user.check_password(current_password):
        raise ValidationError({'errors': {'current_password': 'Current password is not valid.'}})

    try:
        validate_password(password)
    except DjangoValidationError as error:
        try:
            message = error.messages[0]
        except IndexError:
            message = 'Password is not valid.'

        raise ValidationError({'errors': {'password': message}})

    user.set_password(password)
    user.save()
    return Response(status=status.HTTP_200_OK)


@api_view(('POST',))
@permission_classes((permissions.IsAuthenticated,))
def delete_account(request):
    user = request.user
    password = request.data.get('password')
    if not user.check_password(password):
        raise ValidationError({'errors': {'password': 'Invalid password.'}})

    user.broadcasted_name = ''
    user.is_active = False
    user.messages.filter(conversation__in=user.conversations_of.all()).delete()
    user.save()

    return Response(status=status.HTTP_204_NO_CONTENT)
