from django.urls import path

from .views import AuthAPIView, update_broadcasted_name, update_password, delete_account


urlpatterns = [
    path('', AuthAPIView.as_view()),
    path('update_broadcasted_name/', update_broadcasted_name),
    path('update_password/', update_password),
    path('delete_account/', delete_account),
]
