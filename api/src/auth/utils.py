from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token


class HttpOnlyCookieTokenAuthentication(TokenAuthentication):

    def authenticate(self, request):
        cookie_name = settings.AUTH_COOKIE_NAME
        if cookie_name in request.COOKIES and 'HTTP_AUTHORIZATION' not in request.META:
            return self.authenticate_credentials(request.COOKIES[cookie_name])

        return super().authenticate(request)


def get_auth_cookie(user):
    token, _ = Token.objects.get_or_create(user=user)
    return token.key


def delete_auth_cookie(user):
    try:
        user.auth_token.delete()
    except ObjectDoesNotExist:
        pass
