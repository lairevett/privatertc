from django.urls import path

from .consumers import WebsocketConsumer


websocket_urlpatterns = [
    path('', WebsocketConsumer),
]
