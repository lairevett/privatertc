import asyncio
from asgiref.sync import sync_to_async

from django.contrib.auth.hashers import check_password

from ws.utils.validators import get_valid_room_name, get_valid_no_whitespace_string, get_valid_string
from ws.utils.messages import send_error_message_response, send_success_message_response
from ws.utils.rooms import get_room, join_room


async def on_received_join_room(consumer, payload):
    id_ = payload.get('id', None)

    room_name = await sync_to_async(get_valid_room_name)(payload.get('room_name'))
    if not room_name:
        await send_error_message_response(consumer, id_, 'Invalid room name.')
        return

    room = get_room(room_name)
    if not room:
        await send_error_message_response(consumer, id_, 'Invalid room name (does not exist).')
        return

    room_pin = await sync_to_async(get_valid_no_whitespace_string)(payload.get('room_pin'))
    if not room_pin:
        await send_error_message_response(consumer, id_, 'Invalid room pin.')
        return

    if not check_password(room_pin, room['pin']):
        await send_error_message_response(consumer, id_, 'Invalid room pin (access denied).')
        return

    already_member = consumer.active_room_name is not None
    if already_member:
        await send_error_message_response(
            consumer, id_, f'You are already a member of "{consumer.active_room_name}" room.')
        return

    # Set temporary broadcasted name if user is not authenticated.
    if not consumer.scope['user'].is_authenticated:
        broadcasted_name = await sync_to_async(get_valid_string)(payload.get('broadcasted_name'))
        consumer.user.broadcasted_name = broadcasted_name

    await asyncio.wait({join_room(consumer, room_name), send_success_message_response(consumer, id_)})
