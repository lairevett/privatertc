import asyncio
from asgiref.sync import sync_to_async

from ws.utils.messages import send_error_message_response, send_success_message_response
from ws.utils.rooms import leave_room


async def on_received_leave_room(consumer, payload):
    id_ = payload.get('id', None)

    not_member = consumer.active_room_name is None
    if not_member:
        await send_error_message_response(consumer, id_, 'You are not a member of any room.')
        return

    await asyncio.wait({leave_room(consumer), send_success_message_response(consumer, id_)})
