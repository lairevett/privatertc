import asyncio
from channels.db import database_sync_to_async

from ws.utils.calls import hang_up_call
from ws.utils.messages import send_success_message_response, relay_message_to_group


async def on_received_hang_up_call(consumer, payload):
    id_ = payload.get('id', None)

    tasks = {
        send_success_message_response(consumer, id_),
    }

    if consumer.active_call:
        conversation = await database_sync_to_async(consumer.active_call.get_own_conversation)(consumer.user)
        to_entity = await database_sync_to_async(getattr)(conversation, 'to_entity')
        tasks.add(relay_message_to_group(consumer, to_entity.id, payload))
        tasks.add(hang_up_call(consumer))

    await asyncio.wait(tasks)
