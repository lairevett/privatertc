import asyncio
from asgiref.sync import sync_to_async

from ws.utils.validators import get_valid_string, get_valid_ice_candidate
from ws.utils.messages import send_error_message_response, send_success_message_response, relay_message_to_channel


async def on_received_relay_ice_candidate(consumer, payload):
    id_ = payload.get('id', None)

    ws_channel_name = await sync_to_async(get_valid_string)(payload.get('ws_channel_name'))
    if not ws_channel_name:
        await send_error_message_response(consumer, id_, 'Invalid ws channel name.')
        return

    ice_candidate = await sync_to_async(get_valid_ice_candidate)(payload.get('ice_candidate'))
    if not ice_candidate:
        await send_error_message_response(consumer, id_, 'Invalid ICE candidate.')
        return

    await asyncio.wait({
        relay_message_to_channel(consumer, ws_channel_name, payload),
        send_success_message_response(consumer, id_)
    })
