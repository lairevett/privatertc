async def on_sent_create_group_conversation(consumer, payload):
    owner_user_id = payload.get('owner_user_id')

    if owner_user_id == consumer.user.id:
        group_id = payload.get('group_id')
        await consumer.channel_layer.group_add(group_id, consumer.channel_name)
