import asyncio
from asgiref.sync import sync_to_async

from ws.utils.validators import get_valid_string, get_valid_description_type
from ws.utils.messages import send_error_message_response, send_success_message_response, relay_message_to_channel


async def on_received_relay_description(consumer, payload):
    id_ = payload.get('id', None)

    ws_channel_name = await sync_to_async(get_valid_string)(payload.get('ws_channel_name'))
    if not ws_channel_name:
        await send_error_message_response(consumer, id_, 'Invalid ws channel name.')
        return

    description = payload.get('description')
    if not isinstance(description, dict):
        await send_error_message_response(consumer, id_, 'Invalid description.')
        return

    description_type = await sync_to_async(get_valid_description_type)(description.get('type'))
    if not description_type:
        await send_error_message_response(consumer, id_, 'Invalid description type.')
        return

    sdp = await sync_to_async(get_valid_string)(description.get('sdp'))
    if not sdp:
        await send_error_message_response(consumer, id_, 'Invalid sdp.')
        return

    await asyncio.wait({
        relay_message_to_channel(consumer, ws_channel_name, payload),
        send_success_message_response(consumer, id_),
    })


async def on_sent_relay_description(consumer, payload):
    # TODO: remove the leaving interacted user from other's cache?
    sender_user_id = payload.get('sender_user_id', consumer.user.id)
    if sender_user_id != consumer.user.id:
        consumer.user.add_interacted_user_id(sender_user_id)
