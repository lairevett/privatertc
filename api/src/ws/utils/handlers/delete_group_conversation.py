async def on_sent_delete_group_conversation(consumer, payload):
    owner_user_id = payload.get('owner_user_id')

    if owner_user_id == consumer.user.id:
        owner_conversation_id = payload.get('owner_conversation_id')
        group_id = payload.get('group_id')
        await consumer.channel_layer.group_discard(group_id, consumer.channel_name)
    else:
        # Other group participants don't need to see the deleted conversation id.
        del payload['owner_conversation_id']
