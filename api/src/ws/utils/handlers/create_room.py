import asyncio
from asgiref.sync import sync_to_async

from ws.utils.validators import get_valid_room_name, get_valid_no_whitespace_string
from ws.utils.messages import send_error_message_response, send_success_message_response
from ws.utils.rooms import get_room, create_room


async def on_received_create_room(consumer, payload):
    id_ = payload.get('id', None)

    if not consumer.scope['user'].is_authenticated:
        await send_error_message_response(consumer, id_, 'Authentication required.')
        return

    room_name = await sync_to_async(get_valid_room_name)(payload.get('room_name'))
    if not room_name:
        await send_error_message_response(consumer, id_, 'Invalid room name.')
        return

    if get_room(room_name):
        await send_error_message_response(consumer, id_, 'Invalid room name (already exists).')
        return

    room_pin = await sync_to_async(get_valid_no_whitespace_string)(payload.get('room_pin'))
    if not room_pin:
        await send_error_message_response(consumer, id_, 'Invalid room pin.')
        return

    already_member = consumer.active_room_name is not None
    if already_member:
        await send_error_message_response(
            consumer, id_, f'You are already a member of "{consumer.active_room_name}" room.')
        return

    await asyncio.wait({create_room(consumer, room_name, room_pin), send_success_message_response(consumer, id_)})
