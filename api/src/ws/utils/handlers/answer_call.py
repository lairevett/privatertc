import asyncio
from asgiref.sync import sync_to_async

from django.core.exceptions import ObjectDoesNotExist

from channels.db import database_sync_to_async

from calls.models import Call

from ws.utils.calls import join_call
from ws.utils.messages import send_error_message_response, send_success_message_response, relay_message_to_group
from ws.utils.validators import get_valid_uuid


async def on_received_answer_call(consumer, payload):
    id_ = payload.get('id', None)

    call_id = await sync_to_async(get_valid_uuid)(payload.get('call_id'))
    if not call_id:
        await send_error_message_response(consumer, id_, 'Invalid call id.')
        return

    try:
        call = await database_sync_to_async(Call.objects.get)(id=call_id)
        conversation = await database_sync_to_async(call.get_own_conversation)(consumer.user)

        if not conversation:
            # Don't reveal that the call exists.
            raise ObjectDoesNotExist

        if call.ended_at:
            await send_error_message_response(consumer, id_, 'The call has already ended.')
            return

        join_call(consumer, call)
        # receiver_conversation == None in group calls
        to_entity = await database_sync_to_async(getattr)(conversation, 'to_entity')
        payload['call_id'] = call.id
        await asyncio.wait({
            relay_message_to_group(consumer, to_entity.id, payload),
            send_success_message_response(consumer, id_),
        })
    except ObjectDoesNotExist:
        await send_error_message_response(consumer, id_, 'Invalid call id (does not exist).')
        return
