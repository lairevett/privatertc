import asyncio
from asgiref.sync import sync_to_async

from ws.utils.validators import get_valid_array
from ws.utils.messages import send_error_message_response, send_success_message_response, relay_message_to_group


async def on_received_join(consumer, payload):
    id_ = payload.get('id', None)

    user_ids = await sync_to_async(get_valid_array)(payload.get('user_ids'))
    if not user_ids:
        await send_error_message_response(consumer, id_, 'Invalid user ids.')
        return

    tasks = set({send_success_message_response(consumer, id_)})
    for user_id in user_ids:
        tasks.add(relay_message_to_group(consumer, user_id, payload))

    await asyncio.wait(tasks)
