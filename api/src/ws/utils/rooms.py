from django.conf import settings
from django.core.cache import cache
from django.contrib.auth.hashers import make_password

from .messages import make_meta_message, relay_message_to_group


def _make_room(name, pin):
    return {
        'name': name,
        'pin': make_password(pin),
        'channels_count': 0
    }


def get_room(name):
    return cache.get(f'{settings.ROOMS_CACHE_KEY_PREFIX}_{name}', None)


def _set_room(room):
    cache.set(f'{settings.ROOMS_CACHE_KEY_PREFIX}_{room["name"]}', room, settings.ROOMS_CACHE_TIMEOUT)


def _delete_room(room):
    cache.delete(f'{settings.ROOMS_CACHE_KEY_PREFIX}_{room["name"]}')


async def create_room(consumer, name, pin):
    room = _make_room(name, pin)
    _set_room(room)
    await join_room(consumer, name)


async def join_room(consumer, name):
    room = get_room(name)

    if room:
        room['channels_count'] += 1
        _set_room(room)

        message = make_meta_message('join', {'room_name': name})
        await relay_message_to_group(consumer, f'room_{name}', message)
        await consumer.channel_layer.group_add(f'room_{name}', consumer.channel_name)
        consumer.active_room_name = name

    return bool(room)


async def leave_room(consumer):
    await consumer.channel_layer.group_discard(f'room_{consumer.active_room_name}', consumer.channel_name)

    message = make_meta_message('leave', {'room_name': consumer.active_room_name})
    await relay_message_to_group(consumer, f'room_{consumer.active_room_name}', message)

    room = get_room(consumer.active_room_name)
    room['channels_count'] -= 1
    if room['channels_count'] == 0:
        _delete_room(room)
    else:
        _set_room(room)

    consumer.active_room_name = None
