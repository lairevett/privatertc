import asyncio

from channels.db import database_sync_to_async

from .messages import relay_message_to_group, make_meta_message


async def join_own_channels(consumer):
    message = make_meta_message('join')
    await relay_message_to_group(consumer, consumer.user.id, message)
    await consumer.channel_layer.group_add(consumer.user.id, consumer.channel_name)


async def join_groups(consumer):
    group_ids = await database_sync_to_async(list)(consumer.user.group_ids_of)

    tasks = set()
    for group_id in group_ids:
        tasks.add(consumer.channel_layer.group_add(group_id, consumer.channel_name))

    if len(tasks) > 0:
        await asyncio.wait(tasks)


async def join_interacted_users(consumer):
    message = make_meta_message('join')
    interacted_user_ids = consumer.user.get_interacted_user_ids()
    tasks = {relay_message_to_group(consumer, user_id, message) for user_id in interacted_user_ids}

    if len(tasks) > 0:
        await asyncio.wait(tasks)


async def leave_own_channels(consumer):
    message = make_meta_message('leave')
    await consumer.channel_layer.group_discard(consumer.user.id, consumer.channel_name)
    await relay_message_to_group(consumer, consumer.user.id, message)


async def leave_groups(consumer):
    group_ids = await database_sync_to_async(list)(consumer.user.group_ids_of)

    tasks = set()
    for group_id in group_ids:
        tasks.add(consumer.channel_layer.group_discard(group_id, consumer.channel_name))

    if len(tasks) > 0:
        await asyncio.wait(tasks)


async def leave_interacted_users(consumer):
    message = make_meta_message('leave')
    interacted_user_ids = consumer.user.get_interacted_user_ids()
    tasks = {relay_message_to_group(consumer, user_id, message) for user_id in interacted_user_ids}

    if len(tasks) > 0:
        await asyncio.wait(tasks)


async def relay_change_online_status(consumer, is_online):
    message = make_meta_message('change_online_status', {'is_online': is_online})
    await relay_message_to_group(consumer, 'everyone', message)
