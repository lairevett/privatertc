import re

valid_uuid_regex = re.compile('^[0-9a-f]{32}$', re.IGNORECASE)


def get_valid_string(string):
    return string if isinstance(string, str) and len(string) > 0 else None


def get_valid_no_whitespace_string(string):
    valid = get_valid_string(string)
    stripped = valid.strip() if valid else ''
    return stripped if stripped and len(stripped) > 0 else None


def get_valid_array(array):
    return array if isinstance(array, list) else None


def get_valid_uuid(uuid):
    is_valid = valid_uuid_regex.fullmatch(get_valid_no_whitespace_string(uuid) or '')
    return uuid if is_valid else None


def get_valid_content(content):
    return content if isinstance(content, dict) else None


def get_valid_message_type(type_, valid_types):
    return type_ if type_ in valid_types else None


def get_valid_room_name(room_name):
    regex = re.compile('[a-zA-Z0-9-_\s]{4,}')
    is_valid = regex.fullmatch(get_valid_string(room_name) or '')
    return room_name if is_valid else None


def get_valid_ice_candidate(ice_candidate):
    keys = {'candidate', 'sdpMid', 'sdpMLineIndex', 'usernameFragment'}
    is_valid = set(ice_candidate.keys()) <= keys and \
        isinstance(ice_candidate['candidate'], str) and \
        isinstance(ice_candidate['sdpMid'], str) and \
        isinstance(ice_candidate['sdpMLineIndex'], int)

    return ice_candidate if is_valid else None


def get_valid_description_type(description_type):
    is_valid = description_type in ('offer', 'answer')
    return description_type if is_valid else None
