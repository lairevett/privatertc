from channels.db import database_sync_to_async


def join_call(consumer, call):
    call.add_participant(consumer.user.id, consumer.channel_name)
    consumer.active_call = call


async def hang_up_call(consumer):
    participants = consumer.active_call.remove_participant(consumer.user.id, consumer.channel_name)
    await database_sync_to_async(consumer.active_call.attempt_to_end)(participants)
    consumer.active_call = None
