from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


def make_meta_message(message_type, additional_payload=None):
    if not additional_payload:
        additional_payload = dict()

    assert isinstance(message_type, str)
    assert isinstance(additional_payload, dict)

    return {'message_type': message_type, **additional_payload}


def make_incoming_message(content):
    assert isinstance(content, dict)

    return {
        'type': 'on_received_message',
        'payload': content
    }


def make_outgoing_non_consumer_message(payload):
    assert isinstance(payload, dict)

    return {
        'type': 'on_sent_message',
        'payload': payload
    }


def make_outgoing_message(consumer, content):
    assert isinstance(content, dict)

    message = make_outgoing_non_consumer_message({
        **content,
        'sender_user_id': consumer.user.id,
        'sender_user_broadcasted_name': consumer.user.broadcasted_name,
        'sender_channel_name': consumer.channel_name,
    })

    return message


def send_message_to_sockets_sync(receivers, message_type, additional_payload=None):
    channel_layer = get_channel_layer()
    message = make_meta_message(message_type, additional_payload)
    payload = make_outgoing_non_consumer_message(message)

    for receiver in receivers:
        async_to_sync(channel_layer.group_send)(receiver, payload)


def notify_sockets_of_model_operation(model, operation, receivers, instance_id):
    model_name = model._meta.model_name
    send_message_to_sockets_sync(receivers, f'{operation}_{model_name}', {f'{model_name}_id': instance_id})


async def send_message_response(consumer, id_, additional_payload=None):
    if not additional_payload:
        additional_payload = dict()

    await consumer.send_json({'id': id_, 'payload': {**additional_payload, 'user_id': consumer.user.id}})


async def send_error_message_response(consumer, id_, reason=''):
    await send_message_response(consumer, id_, {'is_success': False, 'reason': reason})


async def send_success_message_response(consumer, id_):
    await send_message_response(consumer, id_, {'is_success': True})


async def relay_message_to_channel(consumer, channel_name, payload):
    message = make_outgoing_message(consumer, payload)
    await consumer.channel_layer.send(channel_name, message)


async def relay_message_to_group(consumer, group_name, payload):
    message = make_outgoing_message(consumer, payload)
    await consumer.channel_layer.group_send(group_name, message)
