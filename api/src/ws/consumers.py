import json
import asyncio
from asgiref.sync import sync_to_async

from channels.generic.websocket import AsyncJsonWebsocketConsumer

import users

from .utils.connection import (
    relay_change_online_status,
    join_own_channels,
    join_groups,
    join_interacted_users,
    leave_own_channels,
    leave_groups,
    leave_interacted_users,
)
from .utils.handlers import (
    on_received_join,
    on_received_create_room,
    on_received_join_room,
    on_received_leave_room,
    on_received_relay_ice_candidate,
    on_received_relay_description,
    on_received_answer_call,
    on_received_hang_up_call,
    on_sent_relay_description,
    on_sent_create_group_conversation,
    on_sent_delete_group_conversation,
)
from .utils.messages import send_error_message_response, make_incoming_message
from .utils.validators import get_valid_content, get_valid_message_type
from .utils.calls import hang_up_call
from .utils.rooms import leave_room


class WebsocketConsumer(AsyncJsonWebsocketConsumer):
    groups = ('everyone',)
    receive_handlers = {
        'join': on_received_join,
        'create_room': on_received_create_room,
        'join_room': on_received_join_room,
        'leave_room': on_received_leave_room,
        'relay_ice_candidate': on_received_relay_ice_candidate,
        'relay_description': on_received_relay_description,
        'answer_call': on_received_answer_call,
        'hang_up_call': on_received_hang_up_call,
    }
    send_handlers = {
        'relay_description': on_sent_relay_description,
        'create_group_conversation': on_sent_create_group_conversation,
        'delete_group_conversation': on_sent_delete_group_conversation,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.active_room_name = None
        self.active_call = None

    async def connect(self):
        await self.accept()

        # Assign some id to the socket no matter if authenticated or not.
        is_authenticated = self.scope['user'].is_authenticated
        self.user = self.scope['user'] if is_authenticated else users.models.User()

        tasks = {
            join_own_channels(self),
            join_groups(self),
            join_interacted_users(self),
        }

        current_channels_count = self.user.increment_channels_count()
        is_first_registered = current_channels_count == 1
        if is_authenticated and is_first_registered:
            tasks.add(relay_change_online_status(self, is_online=True))

        await asyncio.wait(tasks)

    async def receive(self, text_data=None, bytes_data=None, **kwargs):
        try:
            await super().receive(text_data, bytes_data, **kwargs)
        except (json.decoder.JSONDecodeError, ValueError):
            await send_error_message_response(self, None, 'Received data is not a valid JSON.')
        except Exception as exception:
            await send_error_message_response(self, None, str(exception))

    async def receive_json(self, content, **kwargs):
        valid_content = await sync_to_async(get_valid_content)(content)
        if not valid_content:
            await send_error_message_response(self, None, 'Invalid format.')
            return

        type_ = await sync_to_async(get_valid_message_type)(
            valid_content.get('message_type'), self.receive_handlers.keys())
        if not type_:
            await send_error_message_response(self, None, 'Invalid message type.')
            return

        incoming_message = make_incoming_message(valid_content)
        await self.channel_layer.send(self.channel_name, incoming_message)

    async def on_received_message(self, event):
        payload = event['payload']
        type_ = payload.get('message_type', None)

        # type_ checked in receive_json()
        await self.receive_handlers[type_](self, payload)

    async def on_sent_message(self, event):
        payload = event['payload']
        type_ = payload.get('message_type', None)

        sender_channel_name = payload.get('sender_channel_name')
        if sender_channel_name == self.channel_name:
            return

        handler = self.send_handlers.get(type_, sync_to_async(lambda *_: None))
        await asyncio.wait({handler(self, payload), self.send_json(event)})

    async def disconnect(self, _):
        tasks = {
            leave_own_channels(self),
            leave_groups(self),
            leave_interacted_users(self),
        }

        if self.active_room_name:
            tasks.add(leave_room(self))

        if self.active_call:
            tasks.add(hang_up_call(self))

        current_channels_count = self.user.decrement_channels_count()
        if current_channels_count == 0 and self.scope['user'].is_authenticated:
            tasks.add(relay_change_online_status(self, is_online=False))

        await asyncio.wait(tasks)
        self.user.clear_interacted_user_ids()
        self.user.delete_channels_count()
