from rest_framework.permissions import BasePermission

from conversations.models import GroupConversation


class RejectUpdateIfOrdinary(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ('PUT', 'PATCH'):
            current_user_rank = obj.groupconversations_to.get(conversation__of_user=request.user).user_rank
            return current_user_rank > GroupConversation.UserRank.ORDINARY

        return True


class RejectAddingModeratorsIfNotAdministrator(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ('PUT', 'PATCH'):
            members = request.data.get('members', [])
            added_moderators = filter(
                lambda entry: entry.get('user_rank') == GroupConversation.UserRank.MODERATOR, members
            )

            if len(list(added_moderators)) > 0:
                current_user_rank = obj.groupconversations_to.get(conversation__of_user=request.user).user_rank
                return current_user_rank == GroupConversation.UserRank.ADMINISTRATOR

        return True


class RejectUpdatingDeletingModeratorsIfNotAdministrator(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ('PUT', 'PATCH', 'DELETE'):
            members = request.data.get('members')
            user_ids = list(map(lambda entry: entry.get('user_id'), members))

            if len(user_ids) > 0:
                current_member_rank_values = map(
                    lambda groupconversation: (groupconversation.conversation.of_user.id, groupconversation.user_rank),
                    obj.groupconversations_to.all()
                )

                current_member_ranks = dict(current_member_rank_values)
                affected_moderators = filter(
                    lambda user_id: current_member_ranks.get(user_id) == GroupConversation.UserRank.MODERATOR, user_ids
                )

                if len(list(affected_moderators)) > 0:
                    current_user_rank = obj.groupconversations_to.get(conversation__of_user=request.user).user_rank
                    return current_user_rank == GroupConversation.UserRank.ADMINISTRATOR

        return True
