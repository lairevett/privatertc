def make_sort_members_on_update_function(current_member_user_ids):

    def iterate(accumulator, next_):
        is_new_member = next_['identification']['conversation__of_user__id'] not in current_member_user_ids
        dict_key = 'added' if is_new_member else 'updated'
        accumulator[dict_key].append(next_)
        return accumulator

    return iterate
