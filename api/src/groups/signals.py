from django.db.models.signals import post_save
from django.dispatch import receiver

from ws.utils.messages import notify_sockets_of_model_operation


@receiver(post_save, sender='groups.Group')
def notify_sockets_of_group_update(sender, instance, created, **kwargs):
    if not created:
        # TODO: send updated fields?
        notify_sockets_of_model_operation(sender, 'update', instance.member_user_ids, instance.id)
