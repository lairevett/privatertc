from django.db import transaction, DatabaseError

from app.mixins import BroadcastedNameModelMixin, UUIDAsPrimaryKeyModelMixin

from conversations.models import Conversation

from users.models import User


class Group(BroadcastedNameModelMixin, UUIDAsPrimaryKeyModelMixin):

    @property
    def member_user_ids(self):
        return set(self.groupconversations_to.values_list('conversation__of_user__id', flat=True))

    @property
    def member_user_instances(self):
        return User.objects.filter(id__in=self.member_user_ids)

    def add_member(self, user_id, user_rank):
        assert user_id not in self.member_user_ids
        Conversation.objects.create(of_user_id=user_id, to_group=self, user_rank=user_rank)

    def add_members(self, members):
        try:
            with transaction.atomic():
                for member in members:
                    user_id = member['identification']['conversation__of_user__id']
                    user_rank = member['field_serializer_data'].get('user_rank')
                    self.add_member(user_id, user_rank)
        except DatabaseError:
            pass

    def remove_member(self, user_id):
        member_conversation = self.groupconversations_to.get(conversation__of_user__id=user_id).conversation
        member_conversation.delete()

    def remove_members(self, user_ids):
        try:
            with transaction.atomic():
                for user_id in user_ids:
                    self.remove_member(user_id)
        except DatabaseError:
            pass
