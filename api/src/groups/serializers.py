from copy import deepcopy
from functools import reduce
from collections import defaultdict

from rest_framework import serializers

from app.mixins import FlattenedFieldsSerializerMixin, ShortFieldsSerializerMixin
from app.utils.serializers import WritableNestedFieldsModelSerializer

from users.serializers import UserSerializerV1

from conversations.models import GroupConversation

from .models import Group
from .utils import make_sort_members_on_update_function


class GroupMembersSerializerV1(FlattenedFieldsSerializerMixin, serializers.ModelSerializer):

    class FlattenedConversationUserSerializer(FlattenedFieldsSerializerMixin, serializers.Serializer):

        of_user = UserSerializerV1(is_short=True, read_only=True)

        class Meta:
            flattened_fields = ('of_user',)

    conversation = FlattenedConversationUserSerializer(read_only=True)

    class Meta:
        model = GroupConversation
        fields = ('conversation', 'user_rank',)
        flattened_fields = ('conversation',)


class GroupSerializerV1(ShortFieldsSerializerMixin, WritableNestedFieldsModelSerializer):

    members = GroupMembersSerializerV1(many=True, source='groupconversations_to')
    is_online = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Group
        short_fields = ('id', 'broadcasted_name', 'is_online',)
        full_fields = (*short_fields, 'members',)
        read_only_fields = ('id',)
        writable_nested_fields = (('members', {'conversation__of_user__id': 'user_id'}),)

    def validate_members(self, value):
        request = self.context['request']
        requested_members = request.data.get('members', [])
        if len(requested_members) == 0:
            return value

        administrator_ranks = filter(
            lambda entry: entry.get('user_rank') == GroupConversation.UserRank.ADMINISTRATOR, requested_members
        )
        if len(list(administrator_ranks)) > 0:
            raise serializers.ValidationError('Cannot assign new administrator/s to group.')

        requested_user_ids = set(map(lambda entry: entry.get('user_id'), requested_members))
        current_user_id = request.user.id
        if current_user_id in requested_user_ids:
            raise serializers.ValidationError('The list should not contain your user id.')

        is_update = bool(self.instance)
        if is_update:
            administrator_groupconversation = self.instance.groupconversations_to \
                .filter(user_rank=GroupConversation.UserRank.ADMINISTRATOR)

            # Administrator could have left by himself by delete conversation endpoint.
            administrator_id = administrator_groupconversation.exists() and \
                administrator_groupconversation.first().conversation.of_user.id

            if administrator_id and administrator_id in requested_user_ids:
                raise serializers.ValidationError('The list should not contain group administrator\'s user id.')

        if request.method == 'DELETE':
            current_member_user_ids = self.instance.member_user_ids

            if len(current_member_user_ids & requested_user_ids) != len(requested_user_ids):
                raise serializers.ValidationError(
                    'Every user id requested to remove from the group has to belong to it.'
                )

        return value

    def get_is_online(self, instance):
        current_user = self.context['request'].user
        conversations = current_user.conversations_of
        conversation = conversations.get(groupconversation__to_group=instance)
        return len(conversation.online_user_ids) > 0

    def update(self, instance, validated_data):
        members_field_name = self.get_real_field_name('members')
        current_member_user_ids = instance.member_user_ids

        sorted_members = reduce(
            make_sort_members_on_update_function(current_member_user_ids),
            validated_data[members_field_name],
            defaultdict(list)
        )

        validated_data_copy = deepcopy(validated_data)
        validated_data_copy[members_field_name] = sorted_members['updated']
        updated_instance = super().update(instance, validated_data_copy)  # validate first, then add members if update succeeds
        updated_instance.add_members(sorted_members['added'])
        return updated_instance
