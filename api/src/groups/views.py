from rest_framework import status
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from conversations.models import GroupConversation

from .models import Group
from .serializers import GroupSerializerV1
from .permissions import (
    RejectUpdateIfOrdinary,
    RejectAddingModeratorsIfNotAdministrator,
    RejectUpdatingDeletingModeratorsIfNotAdministrator,
)


class GroupViewSetV1(GenericViewSet, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin):
    serializer_class = GroupSerializerV1
    permission_classes = (IsAuthenticated, RejectUpdateIfOrdinary, RejectAddingModeratorsIfNotAdministrator,
                          RejectUpdatingDeletingModeratorsIfNotAdministrator)

    def get_queryset(self):
        return Group.objects.filter(id__in=self.request.user.group_ids_of)

    def get_serializer(self, *args, **kwargs):
        # DRF is sending post request for some reason when visiting /v1/groups/ in BrowsableAPIRenderer.
        if self.action == 'create' and isinstance(self.request.data.get('members'), list):
            administrator = {
                'user_id': self.request.user.id,
                'user_rank': GroupConversation.UserRank.ADMINISTRATOR.value
            }

            request_data_copy = self.request.data.copy()
            request_data_copy['members'] = [*request_data_copy['members'], administrator]
            kwargs['data'] = request_data_copy

        return super().get_serializer(*args, **kwargs)

    def perform_create(self, serializer):
        member_field_name = serializer.get_real_field_name('members')
        members = serializer.validated_data.pop(member_field_name, [])
        group = serializer.save()
        group.add_members(members)

    @action(detail=True, methods=('GET',))
    def conversation_id(self, request, pk=None):
        group = self.get_object()
        groupconversation = group.groupconversations_to.get(conversation__of_user=request.user)
        return Response({'conversation_id': groupconversation.conversation.id})

    def destroy(self, request, *args, **kwargs):
        group = self.get_object()
        serializer = self.get_serializer(instance=group, data=request.data)
        serializer.is_valid(raise_exception=True)
        members_field_name = serializer.get_real_field_name('members')

        validated_user_ids = map(
            lambda entry: entry['identification']['conversation__of_user__id'],
            serializer.validated_data[members_field_name]
        )

        group.remove_members(list(validated_user_ids))
        return Response(status=status.HTTP_204_NO_CONTENT)
