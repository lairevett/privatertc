from collections import defaultdict

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.cache import cache

from app.mixins import UUIDAsPrimaryKeyModelMixin


class CallManager(models.Manager):

    def create(self, *args, **kwargs):
        # Duplicate in case of sender's account deletetion which results in conversations cascade.
        receiver_conversation = kwargs['sender_conversation'].call_receiver_conversation
        return super().create(*args, **kwargs, receiver_conversation=receiver_conversation)

    def get_ended(self):
        return self.filter(ended_at__isnull=False)


class Call(UUIDAsPrimaryKeyModelMixin):
    objects = CallManager()
    sender_conversation = models.ForeignKey('conversations.Conversation', on_delete=models.SET_NULL,
                                            related_name='outgoing_calls', null=True)
    receiver_conversation = models.ForeignKey('conversations.Conversation', on_delete=models.SET_NULL,
                                              related_name='incoming_calls', null=True)
    started_at = models.DateTimeField(auto_now_add=True)
    ended_at = models.DateTimeField(null=True)

    @property
    def participants(self):
        return self._get_participants()

    def _get_participants(self):
        return cache.get(f'{settings.CALL_PARTICIPANTS_CACHE_KEY_PREFIX}_{self.id}', defaultdict(set))

    def _set_participants(self, participants):
        cache.set(f'{settings.CALL_PARTICIPANTS_CACHE_KEY_PREFIX}_{self.id}', participants,
                  settings.CALL_PARTICIPANTS_CACHE_TIMEOUT)

    def get_own_conversation(self, current_user):
        # One of them could be null.
        conversation = self.sender_conversation or self.receiver_conversation

        if conversation.type == conversation.Type.USER:
            return conversation if conversation.of_user == current_user else \
                conversation.userconversation.symmetric_conversation

        if conversation.type == conversation.Type.GROUP:
            # sender_conversation == None here
            group = conversation.groupconversation.to_group
            group_conversation = group.groupconversations_to.filter(conversation__of_user=current_user)
            return group_conversation.first().conversation if group_conversation.exists() else None

        return None

    def attempt_to_end(self, participants):
        if len(participants) == 0:
            self.ended_at = timezone.now()
            self.save()
        # TODO: force conversation end on cache timeout

    def add_participant(self, id_, channel_name):
        participants = self._get_participants()
        participants[id_].add(channel_name)
        self._set_participants(participants)

        return participants

    def remove_participant(self, id_, channel_name):
        participants = self._get_participants()
        participants[id_].discard(channel_name)

        if len(participants[id_]) == 0:
            del participants[id_]

        self._set_participants(participants)

        return participants
