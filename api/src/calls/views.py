from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.permissions import IsAuthenticated

from .serializers import CallSerializerV1


class CallViewSetV1(GenericViewSet, CreateModelMixin, ListModelMixin):

    permission_classes = (IsAuthenticated,)
    serializer_class = CallSerializerV1

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['show_is_outgoing'] = self.action == 'list'
        return context

    def get_queryset(self):
        return self.request.user.all_calls.order_by('-ended_at')
