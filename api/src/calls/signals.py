from django.db.models.signals import post_save
from django.dispatch import receiver

from ws.utils.messages import send_message_to_sockets_sync


@receiver(post_save, sender='calls.Call')
def offer_call_to_sockets(sender, instance, created, **kwargs):
    if created:
        to_entity = instance.sender_conversation.to_entity
        send_message_to_sockets_sync([to_entity.id], 'offer_call', {'call_id': instance.id})
