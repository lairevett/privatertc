from copy import deepcopy

from rest_framework import serializers

from conversations.serializers import ConversationSerializerV1

from .models import Call


class CallSerializerV1(serializers.ModelSerializer):

    class SenderConversationPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):

        def get_queryset(self):
            return self.context['request'].user.conversations_of.all()

    conversation = serializers.SerializerMethodField()
    sender_conversation = SenderConversationPrimaryKeyRelatedField(write_only=True)
    is_outgoing = serializers.SerializerMethodField()

    class Meta:
        model = Call
        full_fields = ('id', 'conversation', 'sender_conversation',
                       'participants', 'started_at', 'ended_at', 'is_outgoing',)
        full_read_only_fields = ('id', 'conversation', 'participants', 'started_at', 'ended_at', 'is_outgoing',)

    def __init__(self, *args, **kwargs):
        self.Meta.fields = list(self.Meta.full_fields)
        self.Meta.read_only_fields = list(self.Meta.full_read_only_fields)
        show_is_outgoing = kwargs['context'].get('show_is_outgoing', False)

        if not show_is_outgoing:
            self.Meta.fields.remove('is_outgoing')
            self.Meta.read_only_fields.remove('is_outgoing')

        super().__init__(*args, **kwargs)

    def get_field_names(self, declared_fields, info):
        if self.context.get('show_is_outgoing', False):
            return super().get_field_names(declared_fields, info)

        next_declared_fields = deepcopy(declared_fields)
        del next_declared_fields['is_outgoing']
        return super().get_field_names(next_declared_fields, info)

    def get_conversation(self, instance):
        conversation = instance.get_own_conversation(self.context['request'].user)
        return ConversationSerializerV1(instance=conversation, is_short=True, context=self.context).data

    def get_is_outgoing(self, instance):
        return instance.is_outgoing

    def validate_sender_conversation(self, value):
        if value.active_call:
            raise serializers.ValidationError('Conversation has an active call already.')

        if value.type == value.Type.USER and not value.call_receiver_conversation:
            raise serializers.ValidationError('Call receiver does not exist.')

        return value
