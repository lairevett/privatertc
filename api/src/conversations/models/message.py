from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction, DatabaseError

from app.mixins import UUIDAsPrimaryKeyModelMixin
from app.utils.models import UUIDField

from . import Conversation


class MessageManager(models.Manager):

    def _make_recipient_message_instances(self, recipient_conversations, sender_kwargs):
        message_instances = set()

        for conversation in recipient_conversations:
            recipient_kwargs = sender_kwargs.copy()
            recipient_kwargs['conversation'] = conversation
            message_instances.add(self.model(**recipient_kwargs))

        return message_instances

    def create(self, **kwargs):
        own_message = self.model(**kwargs)
        sender_conversation = kwargs['conversation']
        tag = UUIDField.generate_uuid4()

        if sender_conversation.type == sender_conversation.Type.USER:
            recipient_conversation = sender_conversation.userconversation.symmetric_conversation
            recipient_message_instances = self._make_recipient_message_instances({recipient_conversation}, kwargs)
        elif sender_conversation.type == sender_conversation.Type.GROUP:
            recipient_conversation_ids = sender_conversation \
                .groupconversation \
                .to_group \
                .groupconversations_to \
                .exclude(conversation=sender_conversation) \
                .values_list('conversation__id', flat=True)

            recipient_conversations = Conversation.objects.filter(id__in=recipient_conversation_ids)
            recipient_message_instances = self._make_recipient_message_instances(recipient_conversations, kwargs)

        with transaction.atomic():
            try:
                own_message.tag = tag
                own_message.save()

                for recipient_message in recipient_message_instances:
                    recipient_message.tag = tag
                    recipient_message.save()
            except DatabaseError:
                pass

        return own_message


class Message(UUIDAsPrimaryKeyModelMixin):
    MAX_CONTENT_LENGTH = 1000

    objects = MessageManager()
    index = models.PositiveIntegerField()
    tag = UUIDField()
    sender = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='messages')
    conversation = models.ForeignKey('conversations.Conversation', on_delete=models.CASCADE, related_name='messages')
    content = models.TextField(max_length=MAX_CONTENT_LENGTH)  # max_length not enforced, it just sets textarea length.
    date = models.DateTimeField(auto_now_add=True)

    @property
    def has_attachments(self):
        # TODO: cache it?
        return self.attachments.exists()

    def save(self, *args, **kwargs):
        # TODO: encrypt with conversation.of_user (recipient's) public key

        try:
            last_message_in_conversation = self.conversation.messages.latest('index')
            self.index = last_message_in_conversation.index + 1
        except ObjectDoesNotExist:
            self.index = 0

        super().save(*args, **kwargs)
