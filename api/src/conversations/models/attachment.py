from django.db import models, transaction, DatabaseError

from app.utils.models import UUIDField


def make_upload_path(instance, filename):
    return f'attachments/{instance.message.conversation.id}/{UUIDField.generate_uuid4()}_{filename}'


class Attachment(models.Model):
    MAX_FILE_NAME_LENGTH = 255

    message = models.ForeignKey(to='conversations.Message', on_delete=models.CASCADE, related_name='attachments')
    file = models.FileField(max_length=MAX_FILE_NAME_LENGTH, upload_to=make_upload_path)

    @classmethod
    def make_recipient_attachment_instances(cls, symmetric_messages, original_file):
        attachment_instances = []

        for message in symmetric_messages:
            recipient_kwargs = {'message': message, 'file': original_file}
            attachment_instances.append(cls(**recipient_kwargs))

        return attachment_instances

    def _save_to_symmetric_messages(self, original_file):
        symmetric_messages = self.message.__class__.objects.filter(tag=self.message.tag).exclude(id=self.message.id)
        recipient_attachment_instances = self.__class__.make_recipient_attachment_instances(
            symmetric_messages, original_file)

        for recipient_attachment in recipient_attachment_instances:
            recipient_attachment.save(save_to_symmetric_messages=False)

    def save(self, *args, save_to_symmetric_messages=True, **kwargs):
        # TODO: encrypt self.file with self.message.conversation.of_user public key

        if save_to_symmetric_messages:
            with transaction.atomic():
                try:
                    super().save(*args, **kwargs)
                    self._save_to_symmetric_messages(self.file.file)
                except DatabaseError:
                    pass
        else:
            super().save(*args, **kwargs)
