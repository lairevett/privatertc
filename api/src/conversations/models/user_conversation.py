from django.db import models


class UserConversation(models.Model):
    conversation = models.OneToOneField(to='conversations.Conversation', on_delete=models.CASCADE, primary_key=True)
    to_user = models.ForeignKey(to='users.User', on_delete=models.PROTECT, related_name='userconversations_to')
    is_blocked = models.BooleanField(default=False)

    @property
    def symmetric_conversation(self):
        return UserConversation.objects.get(conversation__of_user=self.to_user,
                                            to_user=self.conversation.of_user).conversation

    @property
    def online_user_ids(self):
        return [self.to_user.id] if self.to_user.is_online else []

    @property
    def call_receiver_conversation(self):
        return self.symmetric_conversation
