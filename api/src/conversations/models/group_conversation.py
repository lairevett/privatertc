from django.db import models


class GroupConversation(models.Model):
    UserRank = models.IntegerChoices('UserRank', 'ORDINARY MODERATOR ADMINISTRATOR')

    conversation = models.OneToOneField(to='conversations.Conversation', on_delete=models.CASCADE, primary_key=True)
    to_group = models.ForeignKey(to='groups.Group', on_delete=models.CASCADE, related_name='groupconversations_to')
    user_rank = models.SmallIntegerField(choices=UserRank.choices, default=UserRank.ORDINARY)

    @property
    def online_user_ids(self):
        current_user_id = self.conversation.of_user.id
        member_user_instances = self.to_group.member_user_instances
        return [user.id for user in member_user_instances if user.id != current_user_id and user.is_online]

    @property
    def call_receiver_conversation(self):
        return None
