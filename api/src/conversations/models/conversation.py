from django.db import models, transaction, DatabaseError

from app.mixins import UUIDAsPrimaryKeyModelMixin

from . import UserConversation, GroupConversation


class ConversationManager(models.Manager):
    def _create_to_user(self, of_user, to_user):
        conversation = self.model(of_user=of_user)
        symmetric_conversation = self.model(of_user=to_user)

        userconversation = UserConversation(conversation=conversation, to_user=to_user)
        symmetric_userconversation = UserConversation(conversation=symmetric_conversation, to_user=of_user)

        try:
            with transaction.atomic():
                conversation.save(using=self._db)
                symmetric_conversation.save(using=self._db)

                userconversation.save(using=self._db)
                symmetric_userconversation.save(using=self._db)
        except DatabaseError:
            return None

        return conversation

    def _create_to_group(self, of_user_id, to_group, user_rank):
        conversation = self.model(of_user_id=of_user_id)
        groupconversation = GroupConversation(conversation=conversation, to_group=to_group, user_rank=user_rank)

        try:
            with transaction.atomic():
                conversation.save(using=self._db)
                groupconversation.save(using=self._db)
        except DatabaseError:
            pass

        return conversation

    def create(self, **kwargs):
        to_user = kwargs.get('to_user')
        to_group = kwargs.get('to_group')
        create_method = self._create_to_user if to_user else self._create_to_group
        return create_method(**kwargs)


class Conversation(UUIDAsPrimaryKeyModelMixin):
    Type = models.IntegerChoices('Type', 'USER GROUP')

    objects = ConversationManager()
    of_user = models.ForeignKey(to='users.User', on_delete=models.CASCADE, related_name='conversations_of')
    is_muted = models.BooleanField(default=False)

    @property
    def type(self):
        if hasattr(self, 'userconversation'):
            return self.Type.USER

        if hasattr(self, 'groupconversation'):
            return self.Type.GROUP

    @property
    def to_entity_conversation(self):
        return self.userconversation if self.type == self.Type.USER else self.groupconversation

    @property
    def to_entity(self):
        to_entity_conversation = self.to_entity_conversation
        return to_entity_conversation.to_user if self.type == self.Type.USER else to_entity_conversation.to_group

    @property
    def online_user_ids(self):
        return self.to_entity_conversation.online_user_ids

    @property
    def last_message(self):
        messages = list(self.messages.all())
        return messages[-1] if len(messages) > 0 else None

    @property
    def call_receiver_conversation(self):
        return self.to_entity_conversation.call_receiver_conversation

    @property
    def all_calls(self):
        return self.outgoing_calls.union(self.incoming_calls.all())

    @property
    def last_call(self):
        return self.all_calls.order_by('started_at').last()

    @property
    def active_call(self):
        active_call_exists = self.last_call and not self.last_call.ended_at
        return self.last_call if active_call_exists else None
