from rest_framework import serializers

from app.mixins import ShortFieldsSerializerMixin

from users.models import User
from users.serializers import UserSerializerV1

from groups.models import Group
from groups.serializers import GroupSerializerV1

from conversations.models import Conversation


class ConversationSerializerV1(ShortFieldsSerializerMixin, serializers.ModelSerializer):
    to_user = serializers.SerializerMethodField()
    to_group = serializers.SerializerMethodField()

    class Meta:
        model = Conversation
        short_fields = ('id', 'to_user', 'to_group',)
        full_fields = (*short_fields, 'is_muted',)
        read_only_fields = short_fields

    def get_to_user(self, instance):
        if isinstance(instance.to_entity, User):
            return UserSerializerV1(
                instance.userconversation.to_user, is_short=True, context=self.context
            ).data

        return None

    def get_to_group(self, instance):
        if isinstance(instance.to_entity, Group):
            return GroupSerializerV1(
                instance.groupconversation.to_group, is_short=self._is_short, context=self.context
            ).data

        return None

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        if not self._is_short:
            action = self.context['view'].action

            if action == 'list':
                # Show last conversation message in list view.
                representation['last_message'] = instance.last_message.content if instance.last_message else None
            else:
                representation['active_call'] = instance.active_call.id if instance.active_call else None
                representation['online_user_ids'] = instance.online_user_ids

                representation['blockage'] = {
                    'local': instance.userconversation.is_blocked,
                    'remote': instance.userconversation.symmetric_conversation.userconversation.is_blocked
                } if instance.type == instance.Type.USER else None

        return representation
