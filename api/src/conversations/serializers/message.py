from rest_framework import serializers

from app.mixins import ActionFieldsSerializerMixin
from app.utils.csv import CSVSerializer
from app.utils.serializers import WritableNestedFieldsModelSerializer

from conversations.models import Message

from .attachment import AttachmentSerializerV1


class MessageSerializerV1(ActionFieldsSerializerMixin, WritableNestedFieldsModelSerializer):

    attachments = AttachmentSerializerV1(many=True, context={}, required=False)
    has_attachments = serializers.SerializerMethodField()

    class Meta:
        model = Message
        create_fields = ('id', 'sender', 'content', 'attachments', 'date',)
        list_fields = ('id', 'sender', 'content', 'has_attachments', 'date',)
        read_only_fields = ('id', 'sender', 'date',)
        writable_nested_fields = (('attachments', {}),)  # Attachments not available for updates -- empty dict.

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'attachments' in self.fields:
            self.fields['attachments'].context.update(self.context)

    def validate_content(self, value):
        if len(value) > self.Meta.model.MAX_CONTENT_LENGTH:
            raise serializers.ValidationError(f'Message cannot be longer than {self.Meta.model.MAX_CONTENT_LENGTH}.')

        return value

    def create(self, validated_data):
        current_user = self.context['request'].user
        return super().create({**validated_data, 'sender': current_user})

    def get_has_attachments(self, instance):
        return instance.has_attachments


class MessageCSVSerializerV1(CSVSerializer):

    def __init__(self):
        self.fields = ('sender', 'content', 'date',)
