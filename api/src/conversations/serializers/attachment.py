from rest_framework import serializers

from app.mixins import ActionFieldsSerializerMixin

from conversations.models import Attachment


class AttachmentSerializerV1(ActionFieldsSerializerMixin, serializers.ModelSerializer):

    url = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()

    class Meta:
        model = Attachment
        create_fields = ('file',)
        list_fields = ('url', 'size',)
        retrieve_fields = list_fields

    def get_url(self, instance):
        return instance.file.url

    def get_size(self, instance):
        return instance.file.size
