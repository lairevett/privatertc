from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from ws.utils.messages import send_message_to_sockets_sync


@receiver(post_save, sender='conversations.GroupConversation')
def notify_sockets_of_group_conversation_create_or_update(sender, instance, created, **kwargs):
    if created:
        send_message_to_sockets_sync(
            instance.to_group.member_user_ids,
            'create_group_conversation',
            {'owner_user_id': instance.conversation.of_user.id, 'group_id': instance.to_group.id}
        )
    else:
        # TODO: updated_fields user_rank
        pass


@receiver(post_delete, sender='conversations.GroupConversation')
def notify_sockets_of_group_conversation_delete(sender, instance, **kwargs):
    send_message_to_sockets_sync(
        [instance.conversation.of_user.id, *instance.to_group.member_user_ids],
        'delete_group_conversation',
        {
            'owner_user_id': instance.conversation.of_user.id,
            'owner_conversation_id': instance.conversation.id,
            'group_id': instance.to_group.id
        }
    )


@receiver(post_delete, sender='conversations.GroupConversation')
def delete_group_when_no_participants_left(sender, instance, **kwargs):
    # NOTE: model's delete() method isn't called on CASCADE delete,
    # a signal has to be used to ensure the groups are deleted when the user owning the conversation is deleted.

    # Object is no longer in database here, compare to 0.
    if len(instance.to_group.member_user_ids) == 0:
        instance.to_group.delete()
