from django.db.models import Max
from django.shortcuts import get_object_or_404
from django.http import Http404

from rest_framework import status
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from app.utils.csv import CSVStream

from calls.models import Call

from conversations.serializers import (
    ConversationSerializerV1,
    MessageSerializerV1,
    MessageCSVSerializerV1,
    AttachmentSerializerV1,
)


class ConversationViewSetV1(UpdateModelMixin, DestroyModelMixin, ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    search_fields = (
        'id',
        'userconversation__to_user__id',
        'userconversation__to_user__broadcasted_name',
        'groupconversation__to_group__id',
        'groupconversation__to_group__broadcasted_name',
    )

    def _get_messages(self):
        conversation = self.get_object()
        # Get latest messages but return list in reverse order for conversation-like effect.
        messages = conversation.messages.all().order_by('-index')
        queryset = reversed(self.paginate_queryset(messages))

        serializer = MessageSerializerV1(
            queryset, context={**self.get_serializer_context(), 'action': 'list'}, many=True
        )

        return self.get_paginated_response(serializer.data)

    def _send_message(self, request):
        conversation = self.get_object()
        serializer = MessageSerializerV1(
            data=request.data, context={**self.get_serializer_context(), 'action': 'create'}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save(conversation=conversation)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def _delete_messages(self):
        conversation = self.get_object()
        conversation.messages.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_serializer_class(self):
        if self.action == 'messages':
            return MessageSerializerV1

        return ConversationSerializerV1

    def get_queryset(self):
        current_user = self.request.user

        # Allow operations like sending a message to conversations
        # but list only the ones that have at least one message in them.
        queryset = current_user.acknowledged_conversations_of if self.action == 'list' \
            else current_user.conversations_of

        # Get distinct entries by annotate.
        return queryset.annotate(last_message_date=Max('messages__date')).order_by('-last_message_date')

    def perform_destroy(self, instance):
        if instance.type != instance.Type.GROUP:
            raise ValidationError({'detail': 'Cannot delete conversation to user, use clear chat endpoint instead.'})

        # sender_conversation == None in group call, needs filtering to find an active call
        group_conversation_with_active_call = instance.groupconversation.to_group.groupconversations_to\
            .filter(conversation__outgoing_calls__isnull=False, conversation__outgoing_calls__ended_at=None)\
            .distinct()

        if group_conversation_with_active_call.exists():
            participants = group_conversation_with_active_call.first().conversation.active_call.participants
            is_leaving_user_in_group_call = len(participants[self.request.user.id]) > 0  # defaultdict, so won't throw
            if is_leaving_user_in_group_call:
                raise ValidationError({'detail': 'Cannot leave group conversation while in call.'})

        super().perform_destroy(instance)

    @action(detail=False, methods=('GET',))
    def call_offer_info(self, request, *args, **kwargs):
        call_id = request.GET.get('id')
        if not call_id:
            raise ValidationError({'detail': 'Call id not provided.'})

        call = get_object_or_404(Call, id=call_id)
        conversation = call.get_own_conversation(request.user)
        if not conversation:
            raise Http404

        return Response({'conversation_id': conversation.id, 'participants': call.participants})

    @action(detail=True, methods=('GET',))
    def user_ids(self, request, pk=None):
        conversation = self.get_object()

        if conversation.type == conversation.Type.USER:
            user_ids = [conversation.userconversation.to_user.id]
        else:
            user_ids = conversation.groupconversation.to_group.member_user_ids
            user_ids.discard(request.user.id)

        return Response({'user_ids': user_ids})

    @action(detail=True, methods=('GET', 'POST', 'DELETE',))
    def messages(self, request, pk=None):
        if request.method == 'GET':
            return self._get_messages()

        if request.method == 'POST':
            return self._send_message(request)

        if request.method == 'DELETE':
            return self._delete_messages()

    @action(detail=True, methods=('GET',))
    def supplementary_message_data(self, request, pk=None):
        message_index = request.GET.get('message_index')
        if not message_index:
            raise ValidationError({'detail': 'Message index not provided.'})

        conversation = self.get_object()
        message = conversation.messages.filter(index=message_index)
        if not message.exists():
            raise Http404()

        message = message.first()
        return Response({'id': message.id, 'has_attachments': message.has_attachments})

    @action(detail=True, methods=('GET',))
    def attachments(self, request, pk=None):
        message_id = request.GET.get('message_id')
        if not message_id:
            raise ValidationError({'detail': 'Message id not provided.'})

        conversation = self.get_object()
        message = conversation.messages.filter(id=message_id)
        if not message.exists():
            raise ValidationError({'detail': 'Message id does not exist.'})

        message = message.first()
        if not message.attachments.exists():
            # Cache message.attachments.exists()?
            raise ValidationError({'detail': 'This message does not have any attachments.'})

        serializer = AttachmentSerializerV1(
            message.attachments.all(), context={**self.get_serializer_context(), 'action': 'list'}, many=True
        )
        return Response({'results': serializer.data})

    @action(detail=True, methods=('GET',))
    def messages_export(self, request, pk=None):
        conversation = self.get_object()
        iterator = conversation.messages.iterator()
        csv_serializer = MessageCSVSerializerV1()
        csv_stream = CSVStream(iterator, csv_serializer)
        return csv_stream.export(conversation.id)

    @action(detail=True, methods=('POST',))
    def trigger_block(self, request, pk=None):
        conversation = self.get_object()

        if conversation.type == conversation.Type.GROUP:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        conversation.userconversation.is_blocked = not conversation.userconversation.is_blocked
        conversation.userconversation.save()
        serializer = self.get_serializer(instance=conversation)
        return Response(serializer.data)
