from django.conf import settings
from django.utils import timezone
from django.core.cache import cache


class CacheRequestTimeMiddleware:

    def process_request(self, request):
        if request.user.is_authenticated:
            now = timezone.now()
            cache.set(f'{settings.USER_LAST_REQUEST_TIME_CACHE_KEY_PREFIX}_{request.user.pk}', now,
                      settings.USER_LAST_REQUEST_TIME_CACHE_TIMEOUT)
