from rest_framework import serializers

from app.mixins import ShortFieldsSerializerMixin

from users.models import User


class UserSerializerV1(ShortFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = User
        short_fields = ('id', 'broadcasted_name', 'is_online',)
        full_fields = (*short_fields, 'username', 'last_seen', 'is_active', 'is_staff', 'is_superuser',)
        read_only_fields = ('id', 'username', 'last_seen', 'is_active', 'is_online', 'is_staff', 'is_superuser',)
