from django.core.exceptions import ObjectDoesNotExist

from rest_framework import permissions
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from conversations.models import Conversation
from conversations.serializers import ConversationSerializerV1

from users.models import User
from users.serializers import UserSerializerV1


class UserViewSetV1(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializerV1
    permission_classes = (permissions.IsAuthenticated,)
    search_fields = ('id', 'broadcasted_name',)

    def get_serializer(self, *args, **kwargs):
        return super().get_serializer(*args, is_short=True, **kwargs)

    @action(detail=True, methods=('GET',))
    def channels_count(self, request, pk=None):
        user = self.get_object()
        return Response({'channels_count': user.channels_count})

    @action(detail=True, methods=('GET', 'POST',))
    def conversation(self, request, pk=None):
        user = self.get_object()

        if request.method == 'POST':
            try:
                conversation = request.user.conversations_of.get(userconversation__to_user=user)
                response_status = status.HTTP_409_CONFLICT
            except ObjectDoesNotExist:
                conversation = Conversation.objects.create(of_user=request.user, to_user=user)
                response_status = status.HTTP_201_CREATED

            serializer = ConversationSerializerV1(instance=conversation, context=self.get_serializer_context())
            return Response(serializer.data, status=response_status)

        try:
            conversation = request.user.conversations_of.get(userconversation__to_user=user)

            show_id = request.GET.get('show_id')
            if show_id is not None:
                return Response({'conversation_id': conversation.id})

            serializer = self.get_serializer(conversation)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
