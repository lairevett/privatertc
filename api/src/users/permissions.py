from rest_framework import permissions


class IsAnonymousUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_anonymous

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
