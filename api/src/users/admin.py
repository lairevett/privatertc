from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'get_is_online', 'is_active', 'is_staff', 'is_superuser',)
    list_filter = ('is_staff', 'is_superuser',)
    exclude = ('password', 'last_login',)

    def get_is_online(self, obj):
        return obj.is_online
    get_is_online.short_description = 'Is online'
    get_is_online.boolean = True
