from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.cache import cache
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import PermissionsMixin

from app.mixins import BroadcastedNameModelMixin, UUIDAsPrimaryKeyModelMixin

from calls.models import Call


class UserManager(BaseUserManager):

    def create_user(self, username, password):
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(BroadcastedNameModelMixin, UUIDAsPrimaryKeyModelMixin, AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []
    username_validator = UnicodeUsernameValidator()

    objects = UserManager()
    username = models.CharField(max_length=30, unique=True, validators=[username_validator])
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    @property
    def channels_count(self):
        channels_count = cache.get(f'{settings.USER_CHANNELS_COUNT_CACHE_KEY_PREFIX}_{self.pk}', 0)
        assert channels_count >= 0
        return channels_count

    @property
    def is_online(self):
        return self.channels_count > 0

    @property
    def last_seen(self):
        return cache.get(f'{settings.USER_LAST_SEEN_CACHE_KEY_PREFIX}_{self.pk}', None)

    @property
    def all_calls(self):
        conversations = self.conversations_of.all()
        outgoing_calls = self.get_outgoing_calls(conversations)
        incoming_calls = self.get_incoming_calls(conversations)
        return outgoing_calls.union(incoming_calls)

    @property
    def acknowledged_conversations_of(self):
        # Conversation is acknowledged (displayed in list view) if:
        # 1. It's a user conversation with at least one message.
        # or
        # 2. It's a group conversation.

        return self.conversations_of \
            .annotate(messages_count=models.Count('messages')) \
            .filter(models.Q(groupconversation__isnull=False) | models.Q(messages_count__gt=0))

    @property
    def conversations_to_groups_of(self):
        return self.conversations_of.filter(groupconversation__isnull=False)

    @property
    def group_ids_of(self):
        return self.conversations_to_groups_of.values_list('groupconversation__to_group', flat=True)

    def _set_last_seen(self, value=None):
        value = timezone.now() if not value else value
        cache.set(f'{settings.USER_LAST_SEEN_CACHE_KEY_PREFIX}_{self.pk}', value,
                  settings.USER_LAST_SEEN_CACHE_TIMEOUT)

    def _set_channels_count(self, value):
        assert value >= 0

        if value == 0 and self.last_seen:
            self._set_last_seen()

        cache.set(f'{settings.USER_CHANNELS_COUNT_CACHE_KEY_PREFIX}_{self.pk}', value,
                  settings.USER_CHANNELS_COUNT_CACHE_TIMEOUT)

    def _set_interacted_user_ids(self, value):
        cache.set(f'{settings.USER_INTERACTED_USER_IDS_CACHE_KEY_PREFIX}_{self.pk}', value,
                  settings.USER_INTERACTED_USER_IDS_CACHE_TIMEOUT)

    def start_recording_last_seen(self):
        if not self.last_seen:
            self._set_last_seen()

    def increment_channels_count(self):
        current_channels_count = self.channels_count + 1
        self._set_channels_count(current_channels_count)
        return current_channels_count

    def decrement_channels_count(self):
        current_channels_count = self.channels_count - 1
        self._set_channels_count(current_channels_count)
        return current_channels_count

    def delete_channels_count(self):
        cache.delete(f'{settings.USER_CHANNELS_COUNT_CACHE_KEY_PREFIX}_{self.pk}')

    def get_interacted_user_ids(self):
        return cache.get(f'{settings.USER_INTERACTED_USER_IDS_CACHE_KEY_PREFIX}_{self.pk}', set())

    def add_interacted_user_id(self, id_):
        interacted_user_ids = self.get_interacted_user_ids()
        interacted_user_ids.add(id_)
        self._set_interacted_user_ids(interacted_user_ids)

    def clear_interacted_user_ids(self):
        cache.delete(f'{settings.USER_INTERACTED_USER_IDS_CACHE_KEY_PREFIX}_{self.pk}')

    def get_outgoing_calls(self, conversations=None):
        if not conversations:
            conversations = self.conversations_of.all()

        return Call.objects.get_ended() \
            .filter(sender_conversation__in=conversations) \
            .annotate(is_outgoing=models.Value(True, output_field=models.BooleanField()))

    def get_incoming_calls(self, conversations=None):
        if not conversations:
            conversations = self.conversations_of.all()

        return Call.objects.get_ended() \
            .filter(receiver_conversation__in=conversations) \
            .annotate(is_outgoing=models.Value(False, output_field=models.BooleanField()))
