import re
from itertools import zip_longest

from rest_framework.parsers import MultiPartParser, ParseError


class NestedMultiPartParser(MultiPartParser):

    forbidden_key_characters_regex = re.compile('\[\[|\]\]|{|}')  # pylint: disable=W1401
    missing_parent_key_regex = re.compile('^\W+|^\w+\]')  # pylint: disable=W1401

    def _validate_full_key(self, full_key):
        if self.forbidden_key_characters_regex.search(full_key):
            raise ParseError(f'Forbidden key character in "{full_key}".')

        if self.missing_parent_key_regex.search(full_key):
            raise ParseError(f'Missing parent key name in "{full_key}".')

        if full_key.count('[') != full_key.count(']'):
            raise ParseError(f'Not all brackets were matched in "{full_key}".')

    @staticmethod
    def get_first_bracket_pair(key_part):
        try:
            opening_bracket_index = key_part.index('[')
            closing_bracket_index = key_part.index(']')
            return opening_bracket_index, closing_bracket_index
        except ValueError:
            return None, None

    @staticmethod
    def get_root_key(full_key, opening_bracket_index):
        return full_key[:opening_bracket_index]

    @classmethod
    def is_root_item_array(cls, full_key, opening_bracket_index, closing_bracket_index):
        key = cls.get_nested_key(full_key, opening_bracket_index, closing_bracket_index)
        return cls.is_nested_item_array_element(key)

    @staticmethod
    def get_nested_key(key_part, opening_bracket_index, closing_bracket_index):
        return key_part[opening_bracket_index + 1:closing_bracket_index]

    @staticmethod
    def is_nested_item_array_element(key):
        return len(key) == 0 or key.isdigit()

    @staticmethod
    def get_rest_of_key_slice(key_part, closing_bracket_index):
        return key_part[closing_bracket_index + 1:]

    @classmethod
    def parse_key(cls, key_part, keys=None, instances=None):
        opening_bracket_index, closing_bracket_index = cls.get_first_bracket_pair(key_part)
        if opening_bracket_index is None:
            instances[-1] = None  # Alert _get_collapsed_dict() that this is the deepest key.
            # root_key_parsed == False is taken care of in parse() loop.
            return zip_longest(keys, instances)

        assert (keys is None and instances is None) or (keys is not None and instances is not None)
        is_root_item = keys is None
        if is_root_item:
            keys = []
            instances = []
            key = cls.get_root_key(key_part, opening_bracket_index)
            is_array = cls.is_root_item_array(key_part, opening_bracket_index, closing_bracket_index)
        else:
            key = cls.get_nested_key(key_part, opening_bracket_index, closing_bracket_index)
            is_array = cls.is_nested_item_array_element(key)

        keys.append(key)
        instances.append(list if is_array else dict)

        rest_of_key = cls.get_rest_of_key_slice(key_part, closing_bracket_index)
        return cls.parse_key(rest_of_key, keys, instances)

    @classmethod
    def get_collapsed_dict(cls, keys, values):
        collapsed_dict = dict()
        pointer = collapsed_dict  # References next nested dicts/lists in the loop.

        for (key, instance) in keys:
            if cls.is_nested_item_array_element(key):
                next_pointer = instance()
                pointer.append(next_pointer)  # pylint: disable=E1101
                pointer = next_pointer
            elif instance is None:
                if isinstance(pointer, list):
                    for value in values:
                        pointer.append({key: value})  # pylint: disable=E1101
                else:
                    pointer = values[0] if len(values) == 1 else values
            else:
                pointer = pointer.setdefault(key, instance())

        return collapsed_dict

    def parse(self, stream, media_type=None, parser_context=None):
        Self = self.__class__
        parsed_stream = super().parse(stream, media_type, parser_context)
        data = {**parsed_stream.files, **parsed_stream.data}
        unwrapped_data = {}

        # NOTE: values[0] reason:
        # -- taken from django.utils.datastructures.MultiValueDict code
        # This class exists to solve the irritating problem raised by cgi.parse_qs,
        # which returns a list for every key, even though most Web forms submit
        # single name-value pairs.
        # --

        for full_key, values in data.items():
            self._validate_full_key(full_key)

            if not '[' in full_key:
                unwrapped_data[full_key] = values[0]
                continue

            keys = tuple(Self.parse_key(full_key))
            collapsed_dict = Self.get_collapsed_dict(keys, values)
            # TODO: deep update the dict so nested arrays-in-arrays-... aren't lost.
            # TODO: memoize the parse_key() results.
            unwrapped_data = {**unwrapped_data, **collapsed_dict}

        return unwrapped_data
