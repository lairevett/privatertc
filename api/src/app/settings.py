import os
from django_redis import get_redis_connection

from . import secrets


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '^pdeub*b9fs@#-*7q0s(y@$778i(2-rkl#v4x*faku-0q@2d#y'
DEBUG = True

ALLOWED_HOSTS = [f'api.{secrets.DOMAIN}', secrets.DOMAIN]
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = [f'{secrets.FRONT_PROTOCOL}://{secrets.DOMAIN}:{secrets.FRONT_PORT}']
CSRF_TRUSTED_ORIGINS = [secrets.DOMAIN]

STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../media/') if DEBUG else 'TODO dev-prod settings'  # /api/media
MEDIA_URL = '/media/'

ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
ASGI_APPLICATION = 'app.ws.application'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'solo',
    'channels',
    'rest_framework',
    'rest_framework.authtoken',
    'app',
    'calls.apps.CallsConfig',
    'conversations.apps.ConversationsConfig',
    'groups.apps.GroupsConfig',
    'users',
]

#ACCOUNT_USER_MODEL_EMAIL_FIELD = None
#ACCOUNT_EMAIL_REQUIRED = False
#ACCOUNT_USERNAME_REQUIRED = True
#ACCOUNT_AUTHENTICATION_METHOD = 'username'
COOKIES_DOMAIN = f'.{secrets.DOMAIN}'
COOKIES_SECURE = secrets.FRONT_PROTOCOL == 'https'

CSRF_COOKIE_NAME = '_ct'
CSRF_COOKIE_SECURE = COOKIES_SECURE
CSRF_COOKIE_DOMAIN = COOKIES_DOMAIN

SESSION_COOKIE_NAME = '_sid'
SESSION_COOKIE_SECURE = COOKIES_SECURE
SESSION_COOKIE_DOMAIN = COOKIES_DOMAIN

AUTH_USER_MODEL = 'users.User'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/1',
        'KEY_PREFIX': 'privatertc'
    }
}

ROOMS_CACHE_KEY_PREFIX = 'rooms'
ROOMS_CACHE_TIMEOUT = 60 * 60 * 24

USER_CHANNELS_COUNT_CACHE_KEY_PREFIX = 'user_channels_count'
USER_CHANNELS_COUNT_CACHE_TIMEOUT = None
USER_INTERACTED_USER_IDS_CACHE_KEY_PREFIX = 'user_interacted_user_ids'
USER_INTERACTED_USER_IDS_CACHE_TIMEOUT = None
USER_LAST_SEEN_CACHE_KEY_PREFIX = 'user_last_seen'
USER_LAST_SEEN_CACHE_TIMEOUT = 60 * 60 * 24

CALL_PARTICIPANTS_CACHE_KEY_PREFIX = 'call_participants'
CALL_PARTICIPANTS_CACHE_TIMEOUT = 60 * 60 * 24

SOLO_CACHE = 'default'
SOLO_CACHE_TIMEOUT = 60 * 5  # 5 minutes.
SOLO_CACHE_PREFIX = 'solo'

if os.environ.get('DJANGO_SETTINGS_MODULE'):
    # Clear cache on start, otherwise channels_count isn't counted properly.
    # Only if DJANGO_SETTINGS_MODULE is provided, so the flushall won't run with every pylint execution
    get_redis_connection('default').flushall()

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            'hosts': [('127.0.0.1', 6379)],
        },
    },
}

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 16,
    'DEFAULT_FILTER_BACKENDS': ['rest_framework.filters.SearchFilter'],
    'DEFAULT_PARSER_CLASSES': ['rest_framework.parsers.JSONParser', 'app.parsers.NestedMultiPartParser'],
}

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
