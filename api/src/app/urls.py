from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from .routers.v1 import v1
from .views import app


urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_framework.urls')),
    path('auth/', include('auth.urls')),
    path('v1/', include((v1.urls))),
    path('v1/app/', app),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
