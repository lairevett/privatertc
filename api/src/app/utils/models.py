from uuid import uuid4

from django.db import models


class UUIDField(models.Field):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 32
        kwargs['editable'] = False
        kwargs['default'] = kwargs.get('default', self.__class__.generate_uuid4)
        super().__init__(*args, **kwargs)

    @staticmethod
    def generate_uuid4():
        return uuid4().hex

    def get_internal_type(self):
        return 'CharField'
