import csv

from django.http import StreamingHttpResponse


class Buffer():

    def write(self, value):
        return value


class CSVStream():

    def __init__(self, iterator, csv_serializer):
        self._buffer = Buffer()
        self._iterator = iterator
        self._csv_serializer = csv_serializer

    def export(self, filename):
        writer = csv.DictWriter(
            self._buffer,
            fieldnames=self._csv_serializer.fields,
            delimiter=',',
            quoting=csv.QUOTE_ALL
        )

        response = StreamingHttpResponse(
            (
                writer.writeheader(),
                *[writer.writerow(self._csv_serializer.serialize_row(instance)) for instance in self._iterator],
            ),
            content_type='text/csv'
        )

        response['Content-Disposition'] = f'attachment; filename={filename}.csv'
        return response


class CSVSerializer():

    def __init__(self):
        self.fields = tuple()

    def serialize_row(self, instance):
        return {field: getattr(instance, field) for field in self.fields}
