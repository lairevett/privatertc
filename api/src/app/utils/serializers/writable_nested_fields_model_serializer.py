from rest_framework.serializers import ModelSerializer


class WritableNestedFieldsModelSerializer(ModelSerializer):
    """Usage example:

    class Meta:
        writable_nested_fields = (('nested_field_name', {'nested_field__some_fk__id': 'json_request_key_being_mapped'}))

    PATCH /resource/:id/
    {
        'some_other_not_nested_field': 'value',
        'nested_field_name': {
            'json_request_key_being_mapped': '',
            'nested_field_name_key1': 'new_value',
            ...
        }
    }
    """

    class Meta:
        writable_nested_fields = tuple()

    def _get_field_serializer(self, field_name):
        # Returns get_fields() as @cached_property.
        return self.fields[field_name].child

    def _get_existing_or_new_nested_instance(self, root_instance, real_field_name, identification):
        if identification is None:
            # Create.
            nested_field_manager = getattr(root_instance, real_field_name)
            root_instance_field_name_in_nested_field = nested_field_manager.field.name
            extra_kwargs = {root_instance_field_name_in_nested_field: root_instance}
            nested_instance = nested_field_manager.model(**extra_kwargs)
        else:
            # Update.
            nested_instance = getattr(root_instance, real_field_name).get(**identification)

        return nested_instance

    def _validate_nested_entry(self, field_serializer, nested_entry):
        serializer = field_serializer.__class__(data=nested_entry['field_serializer_data'])
        serializer.is_valid(raise_exception=True)

    def _save_nested_entry(self, nested_entry, nested_instance):
        for (key, value) in nested_entry['field_serializer_data'].items():
            setattr(nested_instance, key, value)

        nested_instance.save()

    def _validate_and_save_single(self, root_instance, field_serializer, real_field_name, nested_entry):
        self._validate_nested_entry(field_serializer, nested_entry)

        nested_instance = self._get_existing_or_new_nested_instance(
            root_instance, real_field_name, nested_entry['identification']
        )

        self._save_nested_entry(nested_entry, nested_instance)

    def _validate_and_save_many(self, root_instance, field_serializer, real_field_name, nested_entries):
        for nested_entry in nested_entries:
            self._validate_and_save_single(root_instance, field_serializer, real_field_name, nested_entry)

    def _get_validate_and_save_method(self, field_name):
        field = self.fields[field_name]
        is_many = hasattr(field, 'many') and field.many
        return self._validate_and_save_many if is_many else self._validate_and_save_single

    def _gather_nested_fields_data_for_saving(self, validated_data):
        # The fields need to be popped from validated_data before saving the root instance.
        nested_fields_data = []
        validated_data_copy = validated_data.copy()

        for (field_name, _) in self.Meta.writable_nested_fields:
            try:
                validate_and_save_method = self._get_validate_and_save_method(field_name)
                field_serializer = self._get_field_serializer(field_name)
                real_field_name = self.get_real_field_name(field_name, field_serializer)
                nested_entries = validated_data_copy.pop(real_field_name)

                nested_fields_data.append(
                    (validate_and_save_method, field_serializer, real_field_name, nested_entries,)
                )
            except KeyError:
                pass

        return nested_fields_data, validated_data_copy

    def _validate_and_save_nested_fields(self, root_instance, nested_fields_data):
        for entry in nested_fields_data:
            validate_and_save_method, field_serializer, real_field_name, nested_entries = entry
            validate_and_save_method(root_instance, field_serializer, real_field_name, nested_entries)

    def get_real_field_name(self, field_name, field_serializer=None):
        if field_serializer is None:
            field_serializer = self._get_field_serializer(field_name)

        has_source = hasattr(field_serializer, 'source') and len(field_serializer.source) > 0
        return field_serializer.source if has_source else field_name

    def to_internal_value(self, data):
        internal_value = super().to_internal_value(data)
        # Allow manual action overwrite from context.
        current_action = self.context.get('action', self.context['view'].action)
        is_create = current_action == 'create'

        for (field_name, identification_kwarg_mapping) in self.Meta.writable_nested_fields:
            field_serializer = self._get_field_serializer(field_name)
            real_field_name = self.get_real_field_name(field_name, field_serializer)
            internal_value[real_field_name] = list()

            for nested_field in field_serializer.fields:
                is_field_flattened = hasattr(field_serializer.Meta, 'flattened_fields') \
                    and nested_field in field_serializer.Meta.flattened_fields

                if is_field_flattened:
                    continue

                for entry in data.get(field_name, []):
                    internal_value[real_field_name].append({
                        'identification': None if is_create else {
                            kwarg: entry.pop(request_field_name)
                            for (kwarg, request_field_name)
                            in identification_kwarg_mapping.items()
                        },
                        'field_serializer_data': entry,
                    })

        return internal_value

    def create(self, validated_data):
        nested_fields_data, validated_data_copy = self._gather_nested_fields_data_for_saving(validated_data)
        created_instance = super().create(validated_data_copy)
        self._validate_and_save_nested_fields(created_instance, nested_fields_data)
        return created_instance

    def update(self, instance, validated_data):
        nested_fields_data, validated_data_copy = self._gather_nested_fields_data_for_saving(validated_data)
        updated_instance = super().update(instance, validated_data_copy)
        self._validate_and_save_nested_fields(updated_instance, nested_fields_data)
        return updated_instance
