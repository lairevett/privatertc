from rest_framework.routers import DefaultRouter

from calls.views import CallViewSetV1
from conversations.views import ConversationViewSetV1
from groups.views import GroupViewSetV1
from users.views import UserViewSetV1


v1 = DefaultRouter()
v1.register(r'calls', CallViewSetV1, basename='call')
v1.register(r'conversations', ConversationViewSetV1, basename='conversation')
v1.register(r'groups', GroupViewSetV1, basename='group')
v1.register(r'users', UserViewSetV1, basename='user')
