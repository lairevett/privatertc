from functools import reduce
from collections import OrderedDict


class ActionFieldsSerializerMixin():

    DEFAULT_ACTIONS = ('create', 'list', 'retrieve', 'update', 'destroy',)

    def __init__(self, *args, **kwargs):
        self._meta_fields_initialized = False

        context = kwargs.get('context')
        if context:
            self._init_meta_fields(context)

        super().__init__(*args, **kwargs)

    def _init_meta_fields(self, context):
        overwritten_action = context.get('action')

        if overwritten_action:
            self.Meta.fields = self._get_action_specific_field_names(overwritten_action)
            self._meta_fields_initialized = True
        else:
            view = context.get('view')

            if view:
                self.Meta.fields = self._get_all_field_names(view) if view.action == 'metadata' \
                    else self._get_action_specific_field_names(view.action)
                self._meta_fields_initialized = True

    def _get_action_specific_field_names(self, action):
        return getattr(self.Meta, f'{action}_fields', tuple())

    def _get_all_field_names(self, view):
        all_actions = set(self.DEFAULT_ACTIONS) | set(view.get_extra_actions())
        field_names = [self._get_action_specific_field_names(action) for action in all_actions]
        flat_field_names = set(reduce(tuple.__add__, field_names))

        if 'POST' not in view.allowed_methods:
            create_fields = self._get_action_specific_field_names('create')
            flat_field_names -= set(create_fields)

        if 'PUT' not in view.allowed_methods:
            update_fields = self._get_action_specific_field_names('update')
            flat_field_names -= set(update_fields)

        return tuple(flat_field_names)

    def get_field_names(self, declared_fields, info):
        if not self._meta_fields_initialized:
            self._init_meta_fields(self.context)

        filtered_declared_fields = OrderedDict(
            (k, v) for k, v in declared_fields.items() if k in self.Meta.fields
        )

        return super().get_field_names(filtered_declared_fields, info)
