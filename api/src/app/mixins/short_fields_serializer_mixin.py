from collections import OrderedDict


class ShortFieldsSerializerMixin():

    def __init__(self, *args, is_short=False, **kwargs):
        self._is_short = is_short

        if is_short:
            assert isinstance(self.Meta.short_fields, tuple)
            self.Meta.fields = self.Meta.short_fields
        else:
            self.Meta.fields = self.Meta.full_fields

        super().__init__(*args, **kwargs)

    def get_field_names(self, declared_fields, info):
        if not self._is_short:
            return super().get_field_names(declared_fields, info)

        filtered_declared_fields = OrderedDict(
            (k, v) for k, v in declared_fields.items() if k in self.Meta.short_fields
        )

        return super().get_field_names(filtered_declared_fields, info)
