class FlattenedFieldsSerializerMixin():

    def to_representation(self, instance):
        flattened_fields = self.Meta.flattened_fields

        representation = super().to_representation(instance)
        for field in flattened_fields:
            assert isinstance(representation[field], dict)
            representation.update(**representation.pop(field))

        return representation
