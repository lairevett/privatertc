from django.db import models


class BroadcastedNameModelMixin(models.Model):

    class Meta:
        abstract = True

    broadcasted_name = models.CharField(max_length=255, blank=True)

    @property
    def is_broadcasting(self):
        return bool(str(self.broadcasted_name).strip())

    def __str__(self):
        representation = super().__str__()

        if self.is_broadcasting:
            representation += f' ({self.broadcasted_name})'

        return representation
