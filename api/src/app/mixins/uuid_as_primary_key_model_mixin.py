from django.db import models

from app.utils.models import UUIDField


class UUIDAsPrimaryKeyModelMixin(models.Model):

    class Meta:
        abstract = True

    id = UUIDField(primary_key=True)

    def __str__(self):
        return f'{self.__class__.__name__} {self.id}'
