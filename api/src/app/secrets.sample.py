DOMAIN = 'domain.com' # Dev domain, e.g. public one pointing to the machine
                      # from the Internet or one from /etc/hosts
FRONT_PROTOCOL = 'http'
FRONT_PORT = '3000'
