from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from .models import App


@api_view(('GET', 'POST',))
def app(request):
    app_instance = App.get_solo()

    if request.method == 'POST':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        changed = False

        name = request.data.get('name')
        if name:
            app_instance.name = name
            changed = True

        trigger_maintenance_mode = request.data.get('trigger_maintenance_mode')
        if trigger_maintenance_mode:
            app_instance.maintenance_mode = not app_instance.maintenance_mode
            changed = True

        if changed:
            app_instance.save()

    return Response({
        'name': app_instance.name,
        'maintenance_mode': app_instance.maintenance_mode,
    })
