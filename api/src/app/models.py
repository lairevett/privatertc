from django.db import models

from solo.models import SingletonModel


class App(SingletonModel):
    name = models.CharField(max_length=24, default='PrivateRTC', blank=False, null=False)
    maintenance_mode = models.BooleanField(default=False)
