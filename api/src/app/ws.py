from django.urls import path

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack

from ws.urls import websocket_urlpatterns as ws_urls


application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter([
            path('ws/', URLRouter(ws_urls)),
        ]),
    ),
})
