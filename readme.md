# PrivateRTC

WebRTC instant messaging system written in React and Django.

## Mobile

![mobile preview](previews/preview_mobile_merged_2.png)

## Desktop

### Conversation list master-detail

![desktop conversation list preview](previews/conversations_desktop.png)

![desktop conversation details preview1](previews/conversation_details_desktop.png)

### Conversation call (incoming, answer with audio, video and hang up)

![desktop call preview0](previews/conversation_call_desktop.png)

### Conversation call (in with 4 other participants, camera stream from one of them)

![desktop call preview1](previews/conversation_call2_desktop.png)
